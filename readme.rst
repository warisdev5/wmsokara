######################################
District & Session Courts Faisalabad
######################################

Nazarat Agency of Senior Civil Judge (Civil Division) Faisalabad.
`https://nazaratagency.dsjfaisalabad.gov.pk/`

*******************
Release Information
*******************

This repo contains in-development code for future releases. To download the
latest stable release please visit the `https://gitlab.com/warisdev5/nazaratagency.git`_ page.

**************************
Changelog and New Features
**************************


*******************
Server Requirements
*******************

PHP version 7.2 or newer is recommended.
