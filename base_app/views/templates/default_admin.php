<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
    <head>
    	<meta charset="UTF-8">		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <title><?= $page_title ? "$page_title - $title" : $title ?></title>
    	<meta name="description" content="<?= $description; ?>">
		<meta name="keywords" content="<?= $key_words; ?>">
    	<meta name="author" content="<?= $author; ?>">

        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/images/favicon.ico'); ?>">	
    	
    	<link href="<?= base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />    	
        <link href="<?= base_url('assets/css/core.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/components.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/icons.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/pages.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/responsive.css'); ?>" rel="stylesheet" type="text/css" />
        
        <link href="<?= base_url('assets/plugins/custombox/css/custombox.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/plugins/animate/animate.min.css'); ?>" rel="stylesheet" type="text/css" />

        <!-- Custom Pages Load CSS -->
        <?php foreach($css_files as $css) : ?>
            <link href="<?= base_url().$css?>" rel="stylesheet">
        <?php endforeach; ?>

        <!-- custom style -->
        <link href="<?= base_url('assets/css/admin_style.css'); ?>" rel="stylesheet" type="text/css" />
        <link media="print" href="<?= base_url('assets/css/print.css');?>" rel="stylesheet">

		<!-- jQuery  -->
		<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>     
			
		<!-- Custom Pages Load JS -->
        <?php foreach ($header_js_files as $js) : ?>
            <script src="<?= base_url().$js;?>" type="text/javascript"></script>
        <?php endforeach; ?>
		
		<!-- Custom Pages Load JS Script -->
        <?php foreach ($js_script as $script) : ?>
            <script type="text/javascript"><?= $script; ?></script>
        <?php endforeach; ?>

        <script>
            let baseURL = "<?= base_url() ?>";
            let currentURL = "<?= current_url() ?>";
            let is_Admin = "<?= $this->ion_auth->is_admin() ?>";
            const active_district_id = "<?= $active_district_id ?>";
            let dataTable, formatState, dropdownOptions, selectedOption, searchField, permission_check, message_type, message;            

            formatState = (state) => {

                if (!state.id) {
                    return state.text;
                }

                var text = state.text.split(',')

                if(text[1] == undefined || text[1] == '')
                {
                    var $state = $(`<span>${text[0]}</span>`);
                }else{
                    var $state = $(`<span>${text[0]}</span><span class='urdu'> (${text[1]})</span>`);
                }

                return $state;
            }

            selectedOption = (selector, table, selectorURL) => {
                $.ajax({
                    url: selectorURL,
                    type: 'post',
                    dataType: 'json',
                    data: { table: table, field_name : '', field_value: '' },
                    success: function (data) {
                        // console.log(data);
                        let newOption = new Option(data.text, data.id, true, true);
                        // Append it to the select
                        $(selector).empty().append(newOption).trigger('change');


                    }
                });
            }

            dropdownOptions = (selector, tableName, fieldName, fieldValue, selectorURL, id) => {
                $(selector).select2({
                    placeholder: '--PleaseSelect--',
                    allowClear: true,
                    ajax: {
                        url: selectorURL + id,
                        type: 'post',
                        dataType: 'json',
                        delay: 500,
                        data: function (params) {
                            return {
                                searchTerm  : params.term,
                                table       : tableName,
                                field_name  : fieldName,
                                field_value : fieldValue                                
                            };
                        },
                        processResults: function (resp) {
                            return {
                                results: resp
                            };
                        },
                        cache: false,
                    },
                    templateResult: formatState,
                    templateSelection: formatState
                });
            }

            searchField = (searchInput, table, fieldName, searchList) => {
                $(searchInput).keyup(function(){
                    let inputValue = $(searchInput).val();                
                
                    if(inputValue == '') {
                        $(searchList).html('');
                    }else{
                        if(inputValue.length > 2) {
                            $.ajax({
                                type: "POST",
                                url: currentURL + '/fetchSearch',
                                data: {
                                    table : table,
                                    search : inputValue,
                                    field_name : fieldName
                                },
                                success: function(resp) {
                                    $(searchList).html(resp).show();
                                }
                            });
                        }
                    }
                });
            }

            permission_check = (date, minutes) => {
                let saved_date = new Date(date);
                
                // Time difference between your local time and UTC is: 5 hour(s) (300 minutes)
                saved_date.setMinutes(saved_date.getMinutes() + (300 + minutes) );

                // console.log("saved_date: ", saved_date);
                
                let today = new Date();

                if( today.getTime() >= saved_date.getTime() )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

        </script>

		<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?= base_url('assets/js/modernizr.min.js'); ?>"></script>
    </head>
    
    <body class="fixed-left-void">
        
        <!-- Begin page -->
        <div id="wrapper" class="forced enlarged">

            <!-- Top Bar Start -->
            <div class="topbar hidden-print">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center" >
                        <a href="<?= base_url('admin'); ?>" class="logo">
                            <i class="icon-c-logo"> <img src="<?= base_url('assets/images/admin/admin_logo_small.jpg'); ?>" height="60"/> </i>
                            <span><img src="<?= base_url('assets/images/admin/admin_logo.jpg'); ?>" height="60"/></span>
                        </a>
                    </div>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left nav-small-btn" >
                                    <i class="ion-navicon"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                            <ul class="nav navbar-nav hidden-xs">
                                <li><a href="javascript:void(0);"><span style="color:#eee;">Nazarat Agency</span></a></li>
                            </ul>

                            <ul class="nav navbar-nav navbar-right pull-right">
                                <li class="hidden-xs">
                                    <a href="<?= base_url();?>">Home</a>
                                </li>
                                <li class="hidden-xs">
                                    <a href="#" class="right-bar-toggle waves-effect waves-light">Welcome to <?=($user->first_name ?? 'Malik') .' '.($user->last_name ?? 'Waris'); ?></a>
                                </li>
                                <li>
                                	<a href="#logoutModal" class="right-bar-toggle waves-effect waves-light" data-animation="door" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a" data-toggle="modal" data-target="#logoutModal"><i class="ti-power-off m-r-5 text-danger"></i> Logout</a>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>

            </div>
            <!-- Top Bar End -->
            
            <!-- Modal Logout -->
            <div id="logoutModal" class="modal logout-modal fade" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <p class="text-center" style="font-size: 25px; line-height: 50px; margin-top: -25px; color: #fff"><span style="font-weight: bold;">Logout</span> Confirmation</p>
                        <div class="col-sm-12">
                            <p style="color: #2b2929; margin-top: 5%" class="text-center">Are you sure you want to Logout from the system?</p>
                            <form>
                                <div class="row">
                                    <div class="center-block" style="margin-top: 5%">
                                        <button class="btn btn-inverse btn-rounded waves-effect waves-light" onclick="Custombox.close();" data-dismiss="modal">No</button>
                                        <a href="<?= base_url('admin/users/logout'); ?>" class="btn btn-primary btn-rounded waves-effect waves-light">Yes</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu hidden-print" >
                <div class="sidebar-inner slimscrollleft">

                    <div class="user-details">
                        <div class="pull-left">
                            <img src="<?= base_url('assets/images/users/avatar-sm.png'); ?>" alt="user picture" class="thumb-md img-circle">
                        </div>
                        <div class="user-info">
                            <div class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><?=$user->username ?? 'Malik Waris';?> <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href='<?=base_url("admin/users/edit_user/$user->id")?>'><i class="md md-face-unlock"></i> Edit Profile<span class="ripple-wrapper"></span></a></li>
                                    <li><a href='<?=base_url('admin/users/change_password')?>'><i class="md md-vpn-key"></i> Change</a></li>
                                </ul>
                            </div>
                            <?php if ( $this->ion_auth->is_admin() ): ?>
                            	<p class="text-muted m-0">Administrator</p>
                            <?php endif; ?>
                        </div>
                    </div>

                
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>
                        	<li class="text-muted menu-title">Navigation</li>

                            <li class=""><a href="<?= base_url('admin'); ?>" class="waves-effect"><i class="fa fa-dashboard"></i><span>Dashboard</span> </a></li>
                            
                            <li><a href="<?= base_url('admin/warrants'); ?>" class="waves-effect"><i class="fa fa-file-text-o"></i> <span>Warrants</span> </a></li>
                            <li><a href="<?= base_url('admin/reports'); ?>" class="waves-effect"><i class="fa fa-bar-chart-o"></i> <span>Reports</span> </a></li>
                            <!-- admin access -->
                            <?php if ( $this->ion_auth->in_group('admin') ) : ?>
                                <li><a href="<?= base_url('admin/staff'); ?>" class="waves-effect"><i class="ti-id-badge"></i> <span>Staff</span> </a></li>                            
                                <li><a href="<?= base_url('admin/courts'); ?>" class="waves-effect"><i class="fa fa-institution"></i> <span>Courts</span> </a></li>
                                <li><a href="<?= base_url('admin/zones'); ?>" class="waves-effect"><i class="fa fa-map-o"></i> <span>Zones</span> </a></li>

                                <li><a href="<?= base_url('admin/warrant_type'); ?>" class="waves-effect"><i class="fa fa-files-o"></i> <span>Warrant Type</span> </a></li>
                                <li><a href="<?= base_url('admin/designations'); ?>" class="waves-effect"><i class="fa fa-openid"></i> <span>Designations</span> </a></li>
                                <li><a href="<?= base_url('admin/cities'); ?>" class="waves-effect"><i class="fa fa-map-marker"></i> <span>Cities</span> </a></li>
                                <li><a href="<?= base_url('admin/users'); ?>" class="waves-effect"><i class="fa fa-users"></i> <span>Users</span> </a></li>
                            <?php endif; ?>
                            
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End --> 
            
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container " >

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-xs-6 col-sm-6">
                                <h4 class="page-title"><?= $page_title ? $page_title : $title; ?></h4>
                            </div>
                            <div class="col-xs-6 col-sm-6">                            
                                <ol class="breadcrumb text-right hidden-print hidden-phone">
									<li>
										<a href="<?= base_url('admin')?>">Dashboard</a>
                                    </li>
									<?php if ($this->router->fetch_class()!== 'dashboard' ) :?>
									<li>
										<a href="<?= base_url( 'admin/'.$this->router->fetch_class() );?>"><?= ucfirst($this->router->fetch_class()); ?></a>
									</li>
									<?php endif; ?>
									<!-- <li class="active">
										<?= ( $page_title ? $page_title : $title ); ?>
									</li> -->
								</ol>                                
                            </div>                            
                        </div>

                        <?php if($message = $this->session->flashdata('message')) : ?>
                            <div class="col-sm-4 col-sm-offset-4 hidden-print alert-msg m-t-10">
                                <div class="alert alert-top-20 alert-<?= $this->session->flashdata('message_type');?> alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>Message</strong> <?= $message?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        <?php endif; ?>
                        
                        <div class="animate-bottom"> 
                        	<?= $main_content; ?>
                        </div>

                    </div> <!-- container -->
                               
                </div> <!-- content -->
                
                <footer class="footer">
                    <div class="col-sm-7 text-xs-center mb-15">&copy; <?=date('Y')?> - Powered by Mr. Ahsan Yaqoob Saqib Senior Civil Judge (Civil Division) Okara.</div>
                    <div class="col-sm-5 text-xs-center text-right"><span class="">Developed by <i class="md md-developer-mode"></i> <a href="http://warisportfolio.blogspot.com/" class="text-custom" style="color:#fff;">Muhammad Waris (JC) District Courts Faisalabad</a></span></div>
                </footer>
            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->
        
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/detect.js'); ?>"></script>
        <script src="<?= base_url('assets/js/fastclick.js'); ?>"></script>
        
        <script src="<?= base_url('assets/js/jquery.slimscroll.js'); ?>"></script>
        <script src="<?= base_url('assets/js/jquery.blockUI.js'); ?>"></script>
        <script src="<?= base_url('assets/js/waves.js'); ?>"></script>
        <script src="<?= base_url('assets/js/wow.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/jquery.nicescroll.js'); ?>"></script>
        <script src="<?= base_url('assets/js/jquery.scrollTo.min.js'); ?>"></script>
        
        <script src="<?= base_url('assets/plugins/peity/jquery.peity.min.js'); ?>"></script>

        <!-- THIS PAGE PLUGINS -->
        <?php foreach ($js_files as $js) : ?>
            <script src="<?= base_url().$js;?>" type="text/javascript"></script>
        <?php endforeach; ?>
        <!-- END PAGE PLUGINS -->

        <!-- Modal-Effect -->
        <script src="<?= base_url('assets/plugins/custombox/js/custombox.min.js'); ?>"></script>
        <script src="<?= base_url('assets/plugins/custombox/js/legacy.min.js'); ?>"></script>
        
        <!-- form validation -->
        <script src="<?= base_url('assets/plugins/parsleyjs/parsley.min.js'); ?>"></script>
        
        <script src="<?= base_url('assets/js/jquery.core.js'); ?>"></script>
        <script src="<?= base_url('assets/js/jquery.app.js'); ?>"></script>

        <script>
            $('.modal').on('shown.bs.modal', function() {
                $(this).find('[autofocus]').focus();
            });

            $('.modal').on('hidden.bs.modal', function(){
                $('#id').val(0);            
                $('#addForm')[0].reset();

                $('button[type="submit"]').attr('disabled',false);
                $('button[type="submit"]').text('Save');
            });

            $(".form-control").change(function(){
                if( $('button[type="submit"]').is(':disabled') ) {
                    $('button[type="submit"]').attr('disabled',false);
                    $('button[type="submit"]').text('Save');
                }
            });

            // on first focus (bubbles up to document), open the menu
            $(document).on('focus', '.select2-selection.select2-selection--single', function (e) {
                $(this).closest(".select2-container").siblings('select:enabled').select2('open');
            });
            
            $(document).tooltip({ selector: '[data-rel="tooltip"]', trigger: 'hover' });
        </script>

	</body>
</html>