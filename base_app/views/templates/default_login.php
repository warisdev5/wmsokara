<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?= ($page_title ? "$page_title - $title" : $title) ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/images/favicon.ico'); ?>">
        
		<meta name="description" content="<?= $description; ?>">
		<meta name="keywords" content="<?= $key_words; ?>">
        <meta name="author" content="<?= $author; ?>">
        
    	<link href="<?= base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/core.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/components.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/icons.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/pages.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/responsive.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/login.css'); ?>" rel="stylesheet" type="text/css" />

        <!-- jQuery  -->
		<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
        
        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?= base_url('assets/js/modernizr.min.js'); ?>"></script>

    </head>
    <body class="ws-login-page">
        
        <div class="wrapper-page">
            <div class="card-box" >
                <div class="panel-heading" > 
                    <div class="logo text-center">
                        <a href="<?= base_url(); ?>">
                            <h2>Senior Civil Judge</h2>
                            <h4>Okara</h4>                        
                        </a>
                    </div>
                </div> 

                <div class="panel-body">
                    <?php if($message = $this->session->flashdata('message')) : ?>
                        <div class="hidden-print alert-msg m-t-10">
                            <div class="alert alert-top-20 alert-<?php echo $this->session->flashdata('message_type');?> alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>Message</strong> <?php echo $message?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="col-xs-12">
                    <?php echo $main_content; ?>
                    </div>
                </div>   
            </div>                              
        </div>

        <div class="row">
            <div class="col-sm-12 text-center">
                <?php if($this->router->fetch_class() == 'login') : ?>
                <p><a href="<?php echo base_url('admin/users/forgot_password'); ?>" class="text-primary m-l-5"><b>Forgot password?</b></a></p>
                <?php endif; ?>
                <!-- <p>&copy; <?php echo date('Y'); ?> Senior Civil Judge (Civil Division) Faisalabad</p> -->
                <h3>Powered by Mr. Ahsan Yaqoob Saqib Senior Civil Judge (Civil Division) Okara.</h3>
                <p>Developed by <i class="md md-developer-mode"></i> <a href="http://warisportfolio.blogspot.com/" style="">Muhammad Waris</a> (JC) District Courts Faisalabad.</p>
            </div>
        </div>
        
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/detect.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/fastclick.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.slimscroll.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.blockUI.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/waves.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/wow.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.nicescroll.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.scrollTo.min.js'); ?>"></script>


        <!-- notify -->
        <script src="<?php echo base_url('assets/plugins/notifyjs/js/notify.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/notifications/notify-metro.js'); ?>"></script>
        <!-- form validation -->
        <script src="<?= base_url('assets/plugins/parsleyjs/parsley.min.js'); ?>"></script>

        <script src="<?php echo base_url('assets/js/jquery.core.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.app.js'); ?>"></script>

        <script>
        $(document).ready(function(){
            let message_type = "<?= $this->session->flashdata('message_type'); ?>";
            let message = "<?= $this->session->flashdata('message'); ?>";

            console.log("message_type:", message_type);
            console.log("message:", message);

            // if(message) {
            //     console.log("Call Message!");
            //     $.Notification.autoHideNotify(message_type, 'top center', message);
            // }
        })
        </script>
	</body>
</html>
