<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= ($page_title ? $page_title : $title) ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/images/favicon.ico'); ?>">
    <meta name="description" content="<?= $description; ?>">
    <meta name="keywords" content="<?= $key_words; ?>">
    <meta name="author" content="<?= $author; ?>">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="<?= base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />

    <!-- Template -->
    <link href="<?= base_url('assets/css/style.css'); ?>" rel="stylesheet">

    <!-- Custom Pages Load CSS -->
    <?php foreach($css_files as $css) {?>
    <link href="<?= base_url().$css?>" rel="stylesheet">
    <?php }?>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
    <![endif]-->
  </head>
  <body data-spy="scroll" data-target="#navbar-menu">
    <!-- Navbar -->
    <div class="navbar navbar-custom sticky navbar-fixed-top" role="navigation" id="sticky-nav">
      <div class="container">

          <!-- Navbar-header -->
          <div class="navbar-header">

              <!-- Responsive menu button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- LOGO -->
            <a class="navbar-brand logo" href="<?=base_url()?>">
            <span class="text-custom">Senior Civil Judge</span> Faisalabad
            </a>

          </div>
          <!-- end navbar-header -->

          <!-- menu -->
          <div class="navbar-collapse collapse" id="navbar-menu">

              <!-- Navbar right -->
            <ul class="nav navbar-nav navbar-right">
                <li class="active">
                    <a href="<?= base_url(); ?>" class="nav-link">Home</a>
                </li>
                <?php if($this->ion_auth->logged_in()) : ?>
                <li><a href="<?= base_url('admin')?>" class="nav-link" title="User Access Dashboard">Dashboard</a></li>
                <li><a href="javascript:void(0);" class="nav-link">Welcome to <?=($user->first_name ?? 'Malik') .' '.($user->last_name ?? 'Waris'); ?></a></li>
                <li><a href="<?= base_url('admin/users/logout')?>" class="nav-link" title="Click Logout">Logout</a></li>
                <?php else: ?>
                <li><a href="<?= base_url('login')?>" class="nav-link" title="User login here">Login</a></li>
                <?php endif; ?>
                <li>
                    <a href="" class="btn btn-white-bordered navbar-btn">Try for Free</a>
                </li>
            </ul>

          </div>
          <!--/Menu -->
      </div>
      <!-- end container -->
    </div>
    <!-- End navbar-custom -->

    <!-- HOME -->
    <section class="home bg-img-1" id="home">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <div class="home-fullscreen">
                            <div class="full-screen">
                                <div class="home-wrapper home-wrapper-alt">
                                    <h2 class="font-light text-white">Ubold is a fully featured premium admin template</h2>
                                    <h4 class="text-white">Ubold is a fully featured premium admin template built on top of awesome Bootstrap 3.3.7, modern web technology HTML5, CSS3 and jQuery. It has many ready to use hand crafted components. </h4>
                                    <a href="http://themeforest.net/item/ubold-responsive-web-app-kit/13489470?ref=coderthemes" target="_blank" class="btn btn-custom">Buy Template</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END HOME -->

    <!-- container -->
    <div class="container">
        <div class="page-header">
            <h1><?= ($page_title ? $page_title : $title) ?></h1>
        </div>
    
        <?= $main_content; ?>

    </div><!-- /container -->

    <!-- FOOTER -->
    <footer class="footer">
            <div class="container">
                <div class="row">
                  <div class="col-sm-8"><p class="text-muted">&copy; <?=date('Y')?> - Powered by Mr. Ahsan Yaqoob Saqib Senior Civil Judge (Civil Division) Faisalabad.</p></div>
                  <div class="col-sm-4 text-right"><p class="text-muted">Developed by <i class="md md-developer-mode"></i> <a href="http://warisportfolio.blogspot.com/">Muhammad Waris</a></p></div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </footer>
        <!-- End Footer -->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/query.easing.1.3.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/jquery.sticky.js'); ?>"></script>
    <!--load js files -->
    <?php foreach ($js_files as $js): ?>
    <script src="<?= base_url().$js ?>" type="text/javascript"></script>
    <?php endforeach; ?>

     <!-- form validation -->
     <script src="<?= base_url('assets/plugins/parsleyjs/parsley.min.js'); ?>"></script>

  </body>
</html>