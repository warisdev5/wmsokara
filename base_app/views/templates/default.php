<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= ($page_title ? "$page_title - $title" : $title) ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/images/favicon.ico'); ?>">
    
    <meta name="description" content="<?= $description; ?>">
    <meta name="keywords" content="<?= $key_words; ?>">
    <meta name="author" content="<?= $author; ?>">

    <!-- Bootstrap -->
    <link href="<?= base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets/css/icons.css'); ?>" rel="stylesheet" type="text/css" />

    <!-- Template -->
    <link href="<?= base_url('assets/css/style.css'); ?>" rel="stylesheet">

    <!-- Custom Pages Load CSS -->
    <?php foreach($css_files as $css) :?>
    <link href="<?= base_url().$css?>" rel="stylesheet">
    <?php endforeach; ?>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <header class="header hidden-print">
    
      <div class="logo-header display-flex-center">
        
        <div class="col-sm-3 col-md-3 p-0 visible-sm visible-md visible-lg">
            <img src="<?= base_url('assets/images/judicial-complex.png')?>" alt="district judiciary faisalabad" width="280" height="150">
        </div>
        
        <div class="col-xs-12 col-sm-6 col-md-6 logo text-center p-0">
            <a href="<?= base_url(); ?>"><h2>Senior Civil Judge<br><span>Okara</span></h2></a>
        </div>
        
        <div class="col-sm-3 col-md-3 p-0 text-right visible-sm visible-md visible-lg">
            <img src="<?= base_url('assets/images/lhc-building.png')?>" alt="lahore hight court" width="280" height="150">
        </div>
        
        <div class="clearfix"></div>
      </div><!-- fd-logo -->
      
      
      <!-- Navigation -->
			<nav class="navbar navbar-inverse" role="navigation">
				<div class="container-fluid container">					  
				<!-- header -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#multi-level-dropdown">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						</button>
					 	<a class="navbar-brand active" href="<?= base_url(); ?>">Home</a>
					</div>
				  
					<!-- menus -->
					<div class="collapse navbar-collapse col-md-12" id="multi-level-dropdown">
						<ul class="nav navbar-nav">              
              <?php if($this->ion_auth->logged_in()) : ?>
                  <li><a href="<?= base_url('admin')?>" title="User Access Dashboard">Dashboard</a></li>
                  <li><a href="javascript:void(0);">Welcome to <?=($user->first_name ?? 'Malik') .' '.($user->last_name ?? 'Waris'); ?></a></li>
                  <li><a href="<?= base_url('admin/users/logout')?>" title="Click Logout">Logout</a></li>
              <?php else: ?>
              <li><a href="<?= base_url('login')?>" title="User login here">Login</a></li>
              <?php endif; ?>
			
						</ul>
					</div>
				 
				 </div>
			</nav>
			<!-- /Navigation -->
    </header>

    <!-- container -->
    <div class="container">
        <div class="page-title">
            <h1><?= ($page_title ? $page_title : $title) ?></h1>
        </div>
    
        <?= $main_content; ?>

    </div><!-- /container -->

    <!-- Footer -->
    <footer class="footer">
      <div class="container">
        <!-- <p class="text-muted">Place sticky footer content here.</p> -->
        <div class="col-sm-7"><p class="text-muted">&copy; <?=date('Y')?> - Powered by Mr. Ahsan Yaqoob Saqib Senior Civil Judge (Civil Division) Okara.</p></div>
        <div class="col-sm-5 text-right"><p class="text-muted">Developed by <i class="md md-developer-mode"></i> <a href="http://warisportfolio.blogspot.com/">Muhammad Waris</a> (JC) District Courts Faisalabad.</p></div>
      </div>
    </footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/detect.js'); ?>"></script>
    <script src="<?= base_url('assets/js/fastclick.js'); ?>"></script>
    <!--load js files -->
    <?php foreach ($js_files as $js): ?>
    <script src="<?= base_url().$js ?>" type="text/javascript"></script>
    <?php endforeach; ?>

     <!-- form validation -->
     <script src="<?= base_url('assets/plugins/parsleyjs/parsley.min.js'); ?>"></script>

  </body>
</html>