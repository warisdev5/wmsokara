<?php defined('BASEPATH') or die(); ?>
<div class="wrapper-page">
    <div class="ex-page-content text-center">
        <div id="container">
            <h1><?php echo $heading; ?></h1>
            <?php echo $message; ?>
        </div>     
    </div>
</div>