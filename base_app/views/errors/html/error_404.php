<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
  $page_title = $heading;
  include VIEWPATH.'errors'.DIRECTORY_SEPARATOR.'header.php';
?>
	<div id="container">
		<h1><?php echo $heading; ?></h1>
		<?php echo $message; ?>
	</div>

<?php include VIEWPATH.'errors'.DIRECTORY_SEPARATOR.'footer.php'; ?>
