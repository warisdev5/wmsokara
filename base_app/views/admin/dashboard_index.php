<?php defined('BASEPATH') or die(); ?>
<div class="row">

    <div class="col-sm-6 col-md-3 col-lg-3">
        <div class="widget-bg-color-icon card-box fadeInDown animated">
            <div class="bg-icon bg-icon-info pull-left">
                <i class="fa fa-balance-scale text-info"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter"><?=$total_warrants;?></b></h3>
                <p class="text-muted">Total Warrants</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-sm-6 col-md-3 col-lg-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-success pull-left">
                <i class="fa fa-gavel text-success"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter"><?=$total_success_warrants;?></b></h3>
                <p class="text-muted">Success</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-sm-6 col-md-3 col-lg-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-purple pull-left">
                <i class="md md-equalizer text-purple"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter"><?=sprintf("%.2f", $total_success_warrants/$total_warrants*100)?></b>%</h3>
                <p class="text-muted">Progress</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-sm-6 col-md-3 col-lg-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-pink pull-left">
                <i class="md md-remove-red-eye text-pink"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter"><?=$total_pending_warrants;?></b></h3>
                <p class="text-muted">Pending</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <!--  Warrants chart -->
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="portlet">
            <!-- /primary heading -->
            <div class="portlet-heading">
                <h3 class="portlet-title text-dark"> Warrants Chart </h3>
                <div class="clearfix"></div>
            </div>
            <div id="portlet4" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <div class="text-center">
                        <ul class="list-inline chart-detail-list">
                            <li>
                                <h5><i class="fa fa-circle m-r-5" style="color: #36404a;"></i>Total Warrants</h5>
                            </li>
                            <li>
                                <h5><i class="fa fa-circle m-r-5" style="color: #5fbeaa;"></i>Success Report</h5>
                            </li>
                            <li>
                                <h5><i class="fa fa-circle m-r-5" style="color: #5d9cec;"></i>Others Report</h5>
                            </li>
                            <li>
                                <h5><i class="fa fa-circle m-r-5" style="color: #ff3f4e;"></i>Total Pending Warrants</h5>
                            </li>
                        </ul>
                    </div>
                    <div id="warrants-chart" style="height: 300px;"></div>
                </div>
            </div>
        </div>
        <!-- /Portlet -->
    </div>

</div>

<script type="text/javascript">
    jQuery(document).ready(function($) {

        $('.counter').counterUp({
            delay: 100,
            time: 1200
        });

        // $(".knob").knob();

        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        Morris.Line({
            element: 'warrants-chart',
            data: [<?php echo $chart_data; ?>],
            xkey: 'month',
            ykeys: ['total', 'success', 'others', 'pending'],
            labels: ['Total Warrants', 'Success Reports', 'Others Reports', 'Pending Warrants'],
            xLabelFormat: function (mo) { return months[mo.getMonth()]; },
            fillOpacity: ['0.1'],
            pointFillColors: ['#ffffff'],
            pointStrokeColors: ['#999999'],
            behaveLikeLine: true,
            gridLineColor: '#ddd', // '#eef0f2',
            hideHover: 'auto',
            resize: true, //defaulted to true
            lineColors: ['#36404a', '#5fbeaa', '#5d9cec', '#ff3f4e']
        });

    });
</script>