<?php defined('BASEPATH') or die('Restricted access'); ?>

<div class="row">
	<div class="col-md-8 col-md-offset-2">

		<div class="card-box">

            <!-- <table class="table table-bordered table-hover table-left"> -->
            <table class="staffDataTable table table-bordered table-left">
                <thead>
                    <tr>
                        <th colspan="4"><h3 class="text-center">Profile of <?=$name;?>, <span class="text-muted"><?=$designation?></span></h3></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class="col-sm-2">Name:</th>
                        <td colspan="2"><?=$name;?><span class="urdu"><?=(isset($name_ur) ? ' &nbsp;('.$name_ur.') ' : '');?></span></td>
                        <td class="col-sm-4 text-center" rowspan="5" class="text-center"><img class="img-thumbnail" src="<?php echo base_url('uploads/profile/'). ($profile_photo ? $profile_photo : ($gender == 'Male' ? 'male.jpg' : 'female.jpg')) ?>" width="140" alt="official <?=$name;?>" /></td>                            
                    </tr>
                    <tr>
                        <th>Father Name:</th>
                        <td colspan="2"><?=$father;?></td>
                    </tr>
                    <tr>
                        <th>CNIC #:</th>
                        <td colspan="2"><?=$cnic_no;?></td>
                    </tr>
                    <tr>
                        <th>Contact #:</th>
                        <td colspan="2"><?=$contact_no;?></td>
                    </tr>
                    <tr>
                        <th>Date of Birth:</th>
                        <td colspan="3"><?=$date_of_birth;?></td>
                    </tr>
                    <tr>
                        <th>Domicile:</th>
                        <td><?=$domicile;?></td>
                        <th class="col-sm-2">Area/Zone:</th>
                        <td><?=$zone;?></td>
                    </tr>
                    <tr>
                        <th>Qualification:</th>
                        <td><?=$qualification;?></td>
                        <th>Disability if any:</th>
                        <td><?=$disability;?></td>
                    </tr>
                    <tr>
                        <th>Gender:</th>
                        <td><?=$gender;?></td>
                        <th>Blood Group:</th>
                        <td><?=$bloodgroup;?></td>
                    </tr>
                    <tr>
                        <th>Religion:</th>
                        <td><?=$religion;?></td>
                        <th>Service Status:</th>
                        <td><?=$service_status;?></td>
                    </tr>
                    <tr>
                        <th>Officile Bike No.:</th>
                        <td><?=$official_bike_no;?></td>
                        <th>Driving License:</th>
                        <td><?=$driving_license_type;?></td>
                    </tr>                  
                    <tr>
                        <th>Postal Address:</th>
                        <td colspan="3"><?=$postal_address;?></td>
                    </tr>
                    <tr>
                        <th>Permanent Address:</th>
                        <td colspan="3"><?=$permanent_address;?></td>
                    </tr>
                    <tr>
                        <th>Remarks:</th>
                        <td colspan="3"><?=$remarks;?></td>
                    </tr>

                </tbody>
            </table>

            <table class="staffDataTable table table-bordered text-center">
                <tr>
                    <th colspan="5">Service Related Information</th>
                </tr>
                <tr>
                    <th>Date of Appointment</th>
                    <td class="view-date-of-appointment"><?=$date_of_appointment;?></td>
                    <th colspan="3">Promotion Related Information (if any)</th>
                </tr>
                <tr>
                    <th>Personnel No.</th>
                    <td class="view-personnel-number"><?=$personnel_number;?></td>
                    <th>Sr.#</th>
                    <th>Date of Promotion</th>
                    <th>Promoted as</th>
                </tr>
                <tr>
                    <th rowspan="">Appointed as</th>
                    <td rowspan="" class="view-appointed-as"><?=$appointed_as?></td>
                    <th>1</th>
                    <td class="view-date-of-promotion-0"></td>
                    <td class="view-promoted-designation-0"></td>
                </tr>
                <tr>
                    <th rowspan="">Current Designation</th>
                    <td rowspan="" class="view-designation"><?=$designation .' ('.$payscale.')';?></td>
                    <th>2</th>
                    <td class="view-date-of-promotion-1"></td>
                    <td class="view-promoted-designation-1"></td>
                </tr>
            </table>

            <!-- <table class="staffDataTable table table-bordered text-center">
                <thead>
                    <tr>
                        <th colspan="4">Posting History</th>
                    </tr>
                    <tr>
                        <th>Posted As</th>
                        <th>Station</th>
                        <th>Posted Dated</th>
                        <th>Transfer Date</th>  
                    </tr>
                </thead>
                <tbody id="postingRecord">
                    <tr><td colspan="4" class="text-center">No Record Found!</td></tr>
                </tbody>                
            </table> -->

            <table class="staffDataTable table table-bordered text-center">
                <thead>
                    <tr>
                        <th colspan="5">Leave Record</h4></th>
                    </tr>
                    <tr>
                        <th>Leave</th>
                        <th>Days</th>
                        <th>Date From</th>
                        <th>Till Date</th>
                        <th>Remarks</th>
                    </tr>
                </thead>

                <tbody id="leaveRecord">
                    <?php foreach($leaves as $leave) : ?>
                        <tr>
                            <td class="col-sm-2"><?=$leave->leavetype?></td>
                            <td class="col-sm-1 text-center"><?=abs(round((strtotime($leave->enddate) - strtotime($leave->startdate)) / 86400)) + 1?></td>
                            <td class="col-sm-2 text-center"><?=$leave->startdate?></td>
                            <td class="col-sm-2 text-center"><?=$leave->enddate?></td>
                            <td class="col-sm-5"><?=$leave->remarks?></td>
                        </tr>
                    <?php endforeach;?>                   
                </tbody>
                
            </table>

            <div class="text-right button-list m-t-20 hidden-print">
                <a class="btn btn-primary btn-rounded" onClick="window.print()" data-toggle="tooltip" title="Print this page! (Ctrl+P)"><span class="btn-label"><i class="fa fa-print"></i></span>Print</a>
            </div>

        </div>

        



    </div>
</div>