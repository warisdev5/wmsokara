<?php defined('BASEPATH') or die('Restricted access'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="card-box table-responsive">

            <table id="dataTables" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>                      
                        <th>Zone Name</th>
                        <th>Description</th>
                        <th class="hidden-print">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>
    </div>
</div>

<!-- add modal -->
<div id="addModal" class="modal fade" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog"> 
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 id="modal_title" class="modal-title">Add Designation</h4> 
            </div>

            <?php echo form_open(current_url().'/add', array( 'class' => 'form-horizontal', 'autocomplete' => 'on', 'id' => 'addForm', 'data-parsley-validate' => '')) ?>

            <div class="modal-body">

                <div class="row">

                    <div class="form-group">                        
                        <label for="name" class="col-xs-4 control-label">Zone Name</label>
                        <div class="col-xs-8">
                            <input type="text" name="name" id="name" required autofocus value="" placeholder="ex. City-1" class="form-control" data-parsley-trigger="change" data-parsley-length="[3, 50]" data-parsley-errors-container="#name_errors" list="name-list">
                            <datalist id="name-list"></datalist>
                            <div class="parsley-custom-errors" id="name_errors"></div>  
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name_ur" class="col-xs-4 control-label">Name (Urdu)</label>
                        <div class="col-xs-8">
                            <input type="text" onfocus="setEditor(this)" name="name_ur" id="name_ur" value="" required class="form-control" placeholder="سٹی۔2" data-parsley-trigger="change" data-parsley-length="[3, 50]" data-parsley-errors-container="#name_ur_errors">
                            <script type=text/javascript>makeUrduEditor('name_ur', 12);</script>
                            <div class="parsley-custom-errors" id="name_ur_errors"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="description" class="col-xs-4 control-label">Description</label>
                        <div class="col-xs-8">    
                            <textarea name="description" id="description" class="form-control" cols="10" rows="3" placeholder="ex. description of zone" data-parsley-trigger="change" data-parsley-length="[3, 255]" data-parsley-errors-container="#description_errors"></textarea>
                            <div class="parsley-custom-errors" id="description_errors"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sorting" class="col-xs-4 control-label">Sorting #:</label>
                        <div class="col-xs-4">
                            <input type="number" name="sorting" id="sorting" value="10" required class="form-control" placeholder="Sorting #" data-parsley-trigger="change" data-parsley-type="integer" data-parsley-range="[1, 100]" data-parsley-errors-container="#sorting_errors">
                        </div>
                        <div class="col-xs-6 col-xs-offset-6 parsley-custom-errors" id="sorting_errors"></div>
                    </div>

                    <input type="hidden" name="id" id="id" value="0">
                
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                <button type="submit" id="submitBtn" name="submit" class="btn btn-primary waves-effect waves-light px-20">Save</button> 
            </div>

            <?php echo form_close(); ?> 
        </div> 
    </div>
</div><!-- /.modal -->

<script>
    $(document).ready(function(){

        let table = "<?=$table?>";        

        dataTable = $('#dataTables').DataTable({
            "dom":"<'row print-hidden'<'col-sm-3'l><'col-sm-4'B><'col-sm-3'f><'col-sm-2 add-btn text-right text-md-center'>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            "buttons": [              
                {
                    extend: 'excelHtml5',
                    text: 'Excel',
                    titleAttr: 'Export Excel',
                    exportOptions: {
                        columns: [':visible:not(.not-export-col):not(.hidden, .hidden-print)'],
                    },
                    className: "tb-button btn btn-sm btn-btn-success"
                },
                {
                    extend: "pdfHtml5",
                    text: 'PDF',
                    titleAttr: 'Create PDF',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',                    
                    className: "tb-button btn btn-sm btn-primary",
                }
            ],
            "paging": true,
            "autoWidth": true,
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "order": [],
            "searchDelay":2000,
            "ajax": {
                url: currentURL + '/fetchAllData',
                type: 'POST',                
            },
            "columnDefs": [                            
                { width: '8%', targets: [ -1 ] },                
                { width: '20%', targets: [ 0 ] },                
                { className: "text-center hidden-print", targets: [ -1 ] },
            ],
            "initComplete": function() {
                $(".dataTables_filter input")
                .unbind()
                .bind("keypress keyup", function(e) { 
                    if(e.keyCode == 13) {
                        dataTable.search(this.value).draw();
                    }
                    if(this.value == "") {
                        dataTable.search("").draw();
                    }
                    return;
                });

                $('.tb-button').attr('data-rel', 'tooltip').attr('data-container', 'body');

                $("div.add-btn").html("<button class='btn btn-sm btn-primary btn-rounded waves-effect waves-light' data-toggle='modal' data-target='#addModal' data-rel='tooltip' data-container='body' title='Add New Designation'><span class='btn-label'><i class='fa fa-plus'></i></span> Add</button>");
            },
            "language": {
                "emptyTable": "No Record Found!",                
            },
            responsive: true,            
            lengthMenu: [
                [10, 25, 50, 100, 200, -1],
                [10, 25, 50, 100, 200, "All"]
            ]
        });

        $('#submitBtn').click(function(evt){
            // evt.preventDefault();
            var submitForm = $('#addForm');

            if( submitForm.parsley().validate() )
            {
                $.ajax({
                    url: submitForm.attr('action'),
                    type: submitForm.attr('method'),
                    data: submitForm.serialize(),
                    dataType: 'json',
                    beforeSend:function(){
                        $('#submitBtn').attr('disabled','disabled');
                        $('#submitBtn').text('Saving...');
                    },
                    success: function(resp){

                        if(resp.message_type == 'success') {

                            $.Notification.autoHideNotify(resp.message_type, 'top center',
                            resp.message);

                            $('#name').val('');
                            $('#name_ur').val('');
                            $('#description').val('');
                            
                            $('#id').val(0);
                            
                            submitForm.find('[autofocus]').focus();

                            $('#submitBtn').attr('disabled',false);
                            $('#submitBtn').text('Save');

                            if(resp.id > 0) {
                                $('.modal').modal('hide');                     
                            }

                            dataTable.ajax.reload(null, false);

                        } else {
                            $.Notification.autoHideNotify(resp.message_type, 'top center',
                            resp.message);
                        }
                    },
                    error: function(){
                        $.Notification.autoHideNotify('error', 'top center',
                        'Error: This case could not save!');
            
                        $('.modal').modal('hide');
                    }

                });

            }
        });

        $(document).on('click', '#editBtn', function() {
            $('#modal_title').text('Edit Zone Name');
            $('#id').val( $(this).attr('edit_id') );
            $('#name').val( $(this).attr('name') );
            $('#name_ur').val( $(this).attr('name_ur') );
            $('#description').val( $(this).attr('description') );
            $('#sorting').val( $(this).attr('sorting') );
        });

        /* $('.modal').on('shown.bs.modal', function() {
            $(this).find('[autofocus]').focus();
        }); */

        /* $('.modal').on('hidden.bs.modal', function(){
            $('#addForm')[0].reset();
            $('#id').val(0);
        }); */

        /* $(".form-control").change(function(){
            if( $("#submitBtn").is(":disabled") ) {
                $('#submitBtn').attr('disabled',false);
                $('#submitBtn').text('Save');
            }
        }); */

        // $(document).tooltip({ selector: '[data-rel="tooltip"]', trigger: 'hover' });

        searchField('#name', table, 'name', '#name-list');
    });
</script>
