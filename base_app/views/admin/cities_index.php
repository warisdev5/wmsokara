<?php defined('BASEPATH') or die('Restricted access'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">

            <div class="table-responsive">
                <table id="dataTables" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>                            
                            <th>District >Tehsil</th>                            
                            <th class="hidden-print">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<!-- add modal -->
<div id="addModal" class="modal fade" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog"> 
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 id="modal_title" class="modal-title">Add City Name</h4> 
            </div>

            <?php echo form_open(current_url().'/add', array( 'class' => 'form-horizontal', 'id' => 'addForm', 'data-parsley-validate' => '')) ?>

            <div class="modal-body">

                <div class="row">

                    <div class="form-group">                        
                        <label for="name" class="col-xs-4 col-sm-4 control-label">Name</label>
                        <div class="col-xs-8 col-sm-6">
                            <input type="text" name="name" id="name" required autofocus value="" placeholder="ex. Jaranwala" class="form-control" data-parsley-trigger="change" data-parsley-length="[3, 50]" data-parsley-errors-container="#name_errors" list="name-list">
                            <datalist id="name-list"></datalist>
                            <div class="parsley-custom-errors" id="name_errors"></div>  
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name_ur" class="col-xs-4 col-sm-4 control-label">Name (Urdu)</label>
                        <div class="col-xs-8 col-sm-6">
                            <input type="text" onfocus="setEditor(this)" name="name_ur" id="name_ur" value="" required class="form-control" placeholder="ex:جڑانوالہ" data-parsley-trigger="change" data-parsley-length="[3, 50]" data-parsley-errors-container="#name_ur_errors">
                            <script type=text/javascript>makeUrduEditor('name_ur', 12);</script>                            
                            <div class="parsley-custom-errors" id="name_ur_errors"></div>
                        </div>
                    </div>

                    <div class="form-group">                        
                        <label for="dist_id" class="col-xs-4 col-sm-4 control-label">District</label>
                        <div class="col-xs-8 col-sm-6">
                            <select name="dist_id" id="dist_id" class="form-control select2" data-parsley-trigger="change" data-parsley-errors-container="#dist_id_errors"></select>
                            <div class="parsley-custom-errors" id="dist_id_errors"></div>                    
                        </div>
                    </div>

                    <div class="form-group" id="active_cms_field">
                        <label for="active_cms" class="col-xs-4 col-sm-4 control-label"></label>
                        <div class="col-xs-8 col-sm-6">
                            <div class="checkbox checkbox-primary">
                                <input id="active_cms" type="checkbox" name="active_cms" value="1" data-parsley-trigger="change">
                                <label for="active_cms">CMS want to active on this district?</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">    
                        <label for="sorting" class="col-xs-4 col-sm-4 control-label">Sorting #:</label>     
                        <div class="col-xs-6">                            
                            <input type="number" name="sorting" id="sorting" value="10" required class="form-control" placeholder="Sorting #" data-parsley-trigger="change" data-parsley-type="integer" data-parsley-range="[1, 100]" data-parsley-errors-container="#sorting_errors">
                        </div>
                        <div class="col-xs-8 col-sm-6 col-xs-offset-6 parsley-custom-errors" id="sorting_errors"></div>
                    </div>

                    <input type="hidden" name="id" id="id" value="0">
                
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                <button type="submit" id="submitBtn" name="submit" class="btn btn-primary waves-effect waves-light px-20">Save</button> 
            </div>

            <?php echo form_close(); ?> 
        </div> 
    </div>
</div><!-- /.modal -->

<script>
    $(document).ready( function () {

        let table = "<?=$table?>";

        dropdownOptions('#dist_id', 'cities', 'dist_id', 'NULL', currentURL+'/getOptions/', 0);

        dataTable = $('#dataTables').DataTable({                
            "dom":"<'row print-hidden'<'col-sm-3'l><'col-sm-4'B><'col-sm-3'f><'col-sm-2 add-btn text-right text-md-center'>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            "buttons": [                      
                {
                    extend: 'excelHtml5',
                    text: 'Excel',
                    titleAttr: 'Export Excel',
                    exportOptions: {
                        columns: [':visible:not(.not-export-col):not(.hidden, .hidden-print)'],
                    },
                    className: "tb-button btn btn-sm btn-btn-success"
                },
                {
                    extend: "pdfHtml5",
                    text: 'PDF',
                    titleAttr: 'Create PDF',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',                    
                    className: "tb-button btn btn-sm btn-primary",
                }
            ],
            "paging": true,
            "autoWidth": true,
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "order": [],
            "searchDelay":2000,
            "ajax": {
                url: currentURL + '/fetchAllData',                
                type: 'POST',
            },
            "columnDefs": [
                { width: '8%', targets: [-1] },                   
                { className: "text-center hidden-print", targets: [-1] },                
            ],
            "initComplete": function() {
                $(".dataTables_filter input")
                .unbind()
                .bind("keypress keyup", function(e) { 
                    if(e.keyCode == 13) {
                        dataTable.search(this.value).draw();
                    }
                    if(this.value == "") {
                        dataTable.search("").draw();
                    }
                    return;
                });

                $('.tb-button').attr('data-rel', 'tooltip').attr('data-container', 'body');

                $("div.add-btn").html("<button class='btn btn-sm btn-primary btn-rounded waves-effect waves-light' data-toggle='modal' data-target='#addModal' data-rel='tooltip' data-container='body' title='Add New City'><span class='btn-label'><i class='fa fa-plus'></i></span> Add</button>");
            },
            "language": {
                "emptyTable": "No Record Found!",                
            },
            responsive: true,            
            lengthMenu: [
                [5, 10, 25, 50, 100, 200, -1],
                [5, 10, 25, 50, 100, 200, "All"]
            ]
        });

        $('#submitBtn').click(function(evt){
            // evt.preventDefault();
            var submitForm = $('#addForm');

            if( submitForm.parsley().validate() )
            {
                $.ajax({
                    url: submitForm.attr('action'),
                    type: submitForm.attr('method'),
                    data: submitForm.serialize(),
                    dataType: 'json',
                    beforeSend:function(){
                        $('#submitBtn').attr('disabled','disabled');
                        $('#submitBtn').text('Saving...');
                    },
                    success: function(resp){

                        if(resp.message_type == 'success') {

                            $.Notification.autoHideNotify(resp.message_type, 'top center',
                            resp.message);

                            $('#name').val('');
                            $('#name_ur').val('');
                            $('#active_cms').prop('checked',false);
                            $('#dist_id').val(null).trigger('change');
                            $('#id').val(0);
                            
                            submitForm.find('[autofocus]').focus();

                            $('#submitBtn').attr('disabled',false);
                            $('#submitBtn').text('Save');

                            if(resp.id > 0) {
                                $('.modal').modal('hide');                     
                            }

                            dataTable.ajax.reload(null, false);

                        } else {
                            $.Notification.autoHideNotify(resp.message_type, 'top center',
                            resp.message);
                        }
                    },
                    error: function(){
                        $.Notification.autoHideNotify('error', 'top center',
                        'Error: This record could not be saved!');
            
                        $('.modal').modal('hide');
                    }

                });

            }
        });

        $(document).on('click', '#editBtn', function() {

            $('#modal_title').text('Edit Record');
            $('#id').val( $(this).attr('edit_id') );
            $('#name').val( $(this).attr('name') );
            $('#name_ur').val( $(this).attr('name_ur') );
            
            $('#sorting').val( $(this).attr('sorting') );

            if($(this).attr('active_cms') == 1){
                $('#active_cms').prop('checked',true);
            }else{
                $('#active_cms').prop('checked',false);
            }

            if($(this).attr('dist_id')) {
                selectedOption('#dist_id', 'cities', currentURL+'/getOptions/'+$(this).attr('dist_id'));
            }

        });

        /* $('.modal').on('shown.bs.modal', function() {
            $(this).find('[autofocus]').focus();
        }); */

        $('.modal').on('hidden.bs.modal', function(){            
            $('#dist_id').val(null).trigger('change');
            $('#active_cms').prop('checked',false);
        });

        // on first focus (bubbles up to document), open the menu
        /* $(document).on('focus', '.select2-selection.select2-selection--single', function (e) {
            $(this).closest(".select2-container").siblings('select:enabled').select2('open');
        }); */

        // $(document).tooltip({ selector: '[data-rel="tooltip"]', trigger: 'hover' });

        $("#dist_id").change(function(){
            if(!$(this).val())
            {
                $('#active_cms_field').removeClass('hidden');
                $('#active_cms_field').addClass('show');
            } else {
                $('#active_cms_field').removeClass('show');
                $('#active_cms_field').addClass('hidden');
            }
		});

        /* $(".form-control").change(function(){
            if( $("#submitBtn").is(":disabled") ) {
                $('#submitBtn').attr('disabled',false);
                $('#submitBtn').text('Save');
            }
        }); */

        searchField('#name', table, 'name', '#name-list');

    } );
</script>