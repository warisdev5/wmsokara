<?php defined('BASEPATH') or die('Restricted access'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">

            <div class="search-form hidden-print px-15 m-b-20">
                <form action="#" id="searchForm" class="form-horizontal" data-parsley-validate>
                   
                    <div class="form-group">
                        
                        <label for="zone_id" class="col-sm-1 control-label">Halqa</label>
                        <div class="col-sm-2">
                            <select name="zone_id" id="zone_id" class="form-control select2" data-parsley-trigger="change" data-parsley-errors-container="#zone_id_errors"></select>
                            <div class="parsley-custom-errors" id="zone_id_errors"></div>                    
                        </div>

                        <label for="staff_id" class="col-sm-1 control-label">Bailiff</label>
                        <div class="col-sm-2">
                            <select name="staff_id" id="staff_id" class="form-control select2" data-parsley-trigger="change" data-parsley-errors-container="#staff_id_errors"></select>
                            <div class="parsley-custom-errors" id="staff_id_errors"></div>                    
                        </div>

                        <label for="startDate" class="col-sm-1 control-label">Date</label>
                        <div class="col-sm-3">
                            <div class="input-daterange input-group" id="date-range">
                                <input type="text" class="form-control" id="startDate" name="startDate" value="<?php echo date('d-m-Y', strtotime('30 days ago', time())); ?>" required autocomplete="off" datepicker" placeholder="dd-mm-yyyy" data-parsley-trigger="focusout" data-parsley-date data-mask="99-99-9999">                                
                                <span class="input-group-addon bg-primary b-0 text-white">to</span>
                                <input type="text" class="form-control" id="endDate" name="endDate" value="<?php echo date('d-m-Y'); ?>" required autocomplete="off" placeholder="dd-mm-yyyy" data-parsley-trigger="focusout" data-parsley-date data-mask="99-99-9999">
                                <span class="input-group-addon bg-primary b-0 text-white"><i class="icon-calender"></i></span>
                            </div>
                        </div>

                        <div class="col-sm-2 m-t-5 text-center">
                            <button type="submit" id="submit" class="btn btn-sm btn-primary btn-rounded"><span class="btn-label"><i class="fa fa-search"></i></span> Search</button>
                        </div>

                    </div>
                    
                </form>
            </div>

            <div class="clearfix"></div>

            <div class="table-responsive reportTable">
                <table id="dataTables" class="table table-striped table-bordered">
                    <thead>
                        <tr>                            
                            <th>Bailiff</th>
                            <th>Progress<br>Percentage</th>
                            <th>Total<br>Warrants</th>
                            <th>Executed</th>
                            <th>Not Executed</th>
                            <th>Abscondence</th>
                            <th>Accommodation<br>changed</th>
                            <th>Incorrect/<br>Incomplete<br>Address</th>
                            <th>Died</th>
                            <th>Other</th>
                            <th>Cancel</th>                            
                            <th>Pending</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>
    </div>    
</div>

<script>
    $(document).ready(function() {

        dropdownOptions('#zone_id', 'zones', '', '', currentURL+'/getOptions/', 0);
        dropdownOptions('#staff_id', 'staff', 'desgn_id', 39, currentURL+'/getOptions/', 0);

        $('#zone_id').on('change', function() {            
            let zone_id = $("#zone_id option:selected").val();
            if(zone_id) {
                dropdownOptions('#staff_id', 'staff', 'zone_id', zone_id, currentURL+'/getOptions/', 0);
            }else{
                dropdownOptions('#staff_id', 'staff', 'desgn_id', 39, currentURL+'/getOptions/', 0);
            }            
        })

        $('#date-range').datepicker({
            format: 'dd-mm-yyyy',
            toggleActive: true,
            autoclose: true,
            todayHighlight: true,
            daysOfWeekDisabled: '0',
            daysOfWeekHighlighted: '0,5',
            clearBtn: true,
        });

        $('#searchForm').on('submit', function(e) {
            e.preventDefault();

            var $searchForm = $(this);
            if ($searchForm.parsley().validate()) {

                dataTable.ajax.reload(null, true);
            }
        });

        dataTable = $('#dataTables').DataTable({
            "dom":"<'row print-hidden'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            "buttons": [
                {
                    extend: 'colvis',
                    text: 'Columns',
                    titleAttr: 'Show/Hide Columns',                
                    collectionLayout: 'two-column',
                    columns: [':visible:not(.not-export-col)'],
                    className: "tb-button btn btn-sm btn-primary",                    
                },                   
                {
                    extend: 'excelHtml5',
                    text: 'Excel',
                    titleAttr: 'Export Excel',
                    exportOptions: {
                        columns: [':visible:not(.not-export-col):not(.hidden, .hidden-print)'],
                    },
                    className: "tb-button btn btn-sm btn-btn-success"
                },
                {
                    extend: "pdfHtml5",
                    text: 'PDF',
                    titleAttr: 'Create PDF',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',                    
                    className: "tb-button btn btn-sm btn-primary",
                }
            ],
            "paging": true,
            "autoWidth": false,
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "order": [],
            "searchDelay":2000,
            "ajax": {
                url: currentURL + '/fetchAllData',
                type: 'POST',
                data: {
                    startDate : function() { return $('#startDate').val() },
                    endDate : function() { return $('#endDate').val() },
                    zone_id : function() { return $('#zone_id').val() },
                    staff_id : function() { return $('#staff_id').val() }                    
                },
            },
            "columnDefs": [
                { visible: false, targets: [ 8, 9, 10 ] },              
                { className: "text-center", targets: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ] },
            ],
            "initComplete": function() {
                $(".dataTables_filter input")
                .unbind()
                .bind("keypress keyup", function(e) { 
                    if(e.keyCode == 13) {
                        dataTable.search(this.value).draw();
                    }
                    if(this.value == "") {
                        dataTable.search("").draw();
                    }
                    return;
                });
                $('.tb-button').attr('data-rel', 'tooltip').attr('data-container', 'body');
            },
            "language": {
                "emptyTable": "No Record Found!",                
            },
            responsive: true,            
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ]
        });

    });
</script>