<?php defined('BASEPATH') or die('Restricted access'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <table class="table table-left table-striped table-bordered">
                <tr>
                    <th>Court Name</th>
                    <td colspan="3"><?=$warrant->court?></td>
                </tr>
                <tr>
                    <th>Warrant</th>
                    <td colspan="3"><?=$warrant->warrant_name?></td>
                </tr>
                <tr>
                    <th>Case Title</th>
                    <td><?=$warrant->case_title?></td>
                    <th>Date of Hearing</th>
                    <td><?=$warrant->date_of_hearing?></td>
                </tr>
                <tr>
                    <th>Plaintiff</th>
                    <td><?=$warrant->plaintiff?></td>
                    <th>Judgement Debtor</th>
                    <td><?=$warrant->defendant?></td>
                </tr>
                <tr>
                    <th>Judgement Debtor Address</th>
                    <td colspan="3"><?=$warrant->address?></td>                    
                </tr>
                <tr>
                    <th>Halqa</th>
                    <td><?=$warrant->zone?></td>
                    <th>District</th>
                    <td><?=$warrant->district?></td>
                </tr>
                <tr>
                    <th>Remarks</th>
                    <td colspan="3"><?=$warrant->remarks?></td>
                </tr>
                <tr>
                    <th>Bailiff</th>
                    <td><?=$warrant->bailiff?></td>
                    <th>Mobile #</th>
                    <td><?=$warrant->bailiff_mobile?></td>
                </tr>
                <tr>
                    <th>Warrant Report</th>
                    <td><?=$warrant->warrant_report?></td>
                    <th>Report Date</th>
                    <td><?=$warrant->report_date?></td>
                </tr>
                <tr>
                    <th>Add</th>
                    <td><?=$warrant->cr_user?></td>
                    <th>Add Date</th>
                    <?php  
                        $date = "20-11-2021 10:10:05";
                    ?>
                    <td><?= $warrant->cr_date ?  $_SERVER['HTTP_HOST'] == 'nazaratagency.dsjfaisalabad.gov.pk' ? date('d-m-Y H:i:s', strtotime($warrant->cr_date . ' + 5 hours')) : $warrant->cr_date : '' ?></td>
                </tr>                
                <tr>
                    <th>Update</th>
                    <td><?=$warrant->md_user?></td>
                    <th>Update Date</th>                    
                    <td><?= $warrant->md_date ?  $_SERVER['HTTP_HOST'] == 'nazaratagency.dsjfaisalabad.gov.pk' ? date('d-m-Y H:i:s', strtotime($warrant->md_date . ' + 5 hours')) : $warrant->md_date : '' ?></td>
                </tr>
            </table>

            <?php        
            if(!empty($warrantHistory)) :
            ?>
            <h3 class="text-center">Warrant Update History</h3>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Warrant</th>
                        <th>Bailiff</th>
                        <th>Halqa</th>
                        <th>Update By</th>
                        <th>Date Time</th>
                    </tr>
                </thead>

                <tbody>                
                    <?php foreach($warrantHistory as $item) : ?>
                        <tr>
                           <td><?= $item->warrant ?></td>
                           <td><?= $item->bailiff ?></td>
                           <td><?= $item->zone_area ?></td>
                           <td class='text-center'><?= $item->username ?></td>                           
                            <td class='text-center'><?= $item->cr_date ?  $_SERVER['HTTP_HOST'] == 'nazaratagency.dsjfaisalabad.gov.pk' ? date('d-m-Y H:i:s', strtotime($item->cr_date . ' + 5 hours')) : $item->cr_date : '' ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>

            </table>

            <?php endif ?>
        </div>
    </div>
</div>
