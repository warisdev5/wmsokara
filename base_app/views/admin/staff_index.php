<?php defined('BASEPATH') or die('Restricted access'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">

            <div class="search-form hidden-print px-15 m-b-20">
                <form action="javascript:void(0);" id="searchForm" class="" data-parsley-validate>              
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <div class="form-group">
                            <label for="desgn_id_search">Designation</label>
                            <select id="desgn_id_search" class="form-control select2 designationOption"></select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <div class="form-group">
                            <label for="zone_id_search">Halqa</label>
                            <select id="zone_id_search" class="form-control select2"></select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <div class="form-group">
                            <label for="bloodgroup_id_search">Blood Group</label>
                            <select id="bloodgroup_id_search" class="form-control select2 bloodgroupOption"></select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <div class="form-group m-t-30">
                            <button type="submit" id="submit" class="btn btn-sm btn-primary btn-rounded"><span class="btn-label"><i class="fa fa-search"></i></span> Search</button>
                        </div>
                    </div>                    
                </form>
            </div>

            <div class="clearfix"></div>
            
            <div class="table-responsive">
                <table id="dataTables" class="table table-striped table-bordered dt-responsive">
                    <thead>
                        <tr>
                            <th>Profile Picture</th>
                            <th>Name</th>
                            <th>Parantage</th>                            
                            <th>Mobile #</th>
                            <th>CNIC</th>                            
                            <th>Date of Birth</th>
                            <th>Qualification</th>                            
                            <th>Designation</th>
                            <th>Personnel #</th>                            
                            <th>Date of Appointment</th>
                            <th>Domicile</th>
                            <th>Gender</th>
                            <th>Blood Group</th>                            
                            <th>Disability if any</th>
                            <th>Religion</th>
                            <th>Address</th>
                            <th>Posted Area</th>
                            <th>Status</th>
                            <th>Last Leave</th>
                            <th class="hidden-print">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<!-- add modal -->
<div id="addModal" class="modal fade" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg"> 
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 id="modal_title" class="modal-title">Add Staff Performa</h4> 
            </div>

            <?php echo form_open_multipart(current_url().'/add', array( 'class' => 'form-horizontal', 'id' => 'addForm', 'data-parsley-validate' => '', 'role' => 'form')) ?>

            <div class="modal-body">

                <div class="row">

                    <div class="col-sm-8">
                        <div class="form-group">                        
                            <label for="name" class="col-sm-3 control-label">Name <small>*</small></label>
                            <div class="col-sm-9">
                                <input type="text" name="name" id="name" class="form-control" required autofocus value="" placeholder="Name of the official" data-parsley-trigger="change" data-parsley-length="[3, 50]" data-parsley-errors-container="#name_errors" list="name-list">
                                <datalist id="name-list"></datalist>
                                <div class="parsley-custom-errors" id="name_errors"></div>  
                            </div>
                        </div>
                        <div class="form-group">                        
                            <label for="name_ur" class="col-sm-3 control-label">Name <span class="urdu">(اردو)</span><small>*</small></label>
                            <div class="col-sm-9">
                                <input type="text" name="name_ur" id="name_ur" class="form-control" required value="" placeholder="نام اردو میں۔۔۔" data-parsley-trigger="change" data-parsley-errors-container="#name_ur_errors" onfocus="setEditor(this)">
                                <script type=text/javascript>makeUrduEditor('name_ur', 12);</script>
                                <div class="parsley-custom-errors" id="name_ur_errors"></div>  
                            </div>
                        </div>

                        <div class="form-group">                        
                            <label for="father" class="col-sm-3 control-label">Father's Name <small>*</small></label>
                            <div class="col-sm-9">
                                <input type="text" name="father" id="father" class="form-control" required value="" placeholder="Father's Name" data-parsley-trigger="change" data-parsley-length="[3, 50]" data-parsley-errors-container="#father_errors" list="father-list">
                                <datalist id="father-list"></datalist>
                                <div class="parsley-custom-errors" id="father_errors"></div>  
                            </div>
                        </div>

                        <div class="form-group">                        
                            <label for="cnic_no" class="col-sm-3 control-label">CNIC # <small>*</small></label>
                            <div class="col-sm-9">
                                <input type="text" name="cnic_no" id="cnic_no" class="form-control" required value="" placeholder="33123-1234567-1" data-parsley-trigger="change" data-mask="99999-9999999-9" data-parsley-errors-container="#cnic_no_errors">
                                <div class="parsley-custom-errors" id="cnic_no_errors"></div>  
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="personnel_number" class="col-sm-3 control-label">Personnel No. <small>*</small></label>
                            <div class="col-sm-9">
                                <input type="text" name="personnel_number" id="personnel_number" class="form-control" value="" placeholder="12345678" data-parsley-trigger="change" data-mask="99999999" data-parsley-errors-container="#personnel_number_errors">
                                <div class="parsley-custom-errors" id="personnel_number_errors"></div>  
                            </div>
                        </div>

                        <div class="form-group">                        
                            <label for="contact_no" class="col-sm-3 control-label">Contact No. <small>*</small></label>
                            <div class="col-sm-9">
                                <input type="text" name="contact_no" id="contact_no" class="form-control" required value="" placeholder="0333-1234567" data-parsley-trigger="change" data-mask="9999-9999999" data-parsley-errors-container="#contact_no_errors">
                                <div class="parsley-custom-errors" id="contact_no_errors"></div>  
                            </div>
                        </div>

                    </div>

                    <div class="col-sm-4 text-center">
                        <div class="pic-center">
                            <div class="form-photo-input">
                                <button type="button" id="profile_photo_close" class="close" style="display:none;">x</button>
                                <div class="photo-preview"> 
                                    <img id="profile_photo_preview">
                                </div>
                                <label for="profile_photo" class="label-photo" id="label-photo">Upload Photo</label>
                                <input type="file" name="profile_photo" id="profile_photo" class="form-control" accept="image/*" onchange="showPreview(event);">
                            </div>
                        </div>
                        <span class="font-13 bg-inverse text-warning px-5">Upload passport size photo less than 50kb</span>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group">                        
                        <label for="date_of_birth" class="col-sm-2 control-label">Date of Birth <small>*</small></label>
                        <div class="col-sm-4 mb-15">
                            <div class="input-group">
                                <input type="text" name="date_of_birth" id="date_of_birth" class="form-control datepicker-all" required value="" placeholder="dd-mm-yyyy" data-parsley-trigger="change" data-mask="99-99-9999" data-parsley-errors-container="#date_of_birth_errors">
                                <span class="input-group-addon bg-primary b-0 text-white"><i class="icon-calender"></i></span>
                            </div>
                            <div class="parsley-custom-errors" id="date_of_birth_errors"></div>  
                        </div>
                        
                        <label for="domicile_id" class="col-sm-2 control-label">Domicile <small>*</small></label>
                        <div class="col-sm-4">
                            <select name="domicile_id" id="domicile_id" class="form-control select2" required data-parsley-trigger="change" data-parsley-errors-container="#domicile_id_errors"></select>
                            <div class="parsley-custom-errors" id="domicile_id_errors"></div>                    
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="date_of_appointment" class="col-sm-2 control-label">Date of Appointment <small>*</small></label>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <input type="text" name="date_of_appointment" id="date_of_appointment" class="form-control datepicker-all" required placeholder="dd-mm-yyyy" data-parsley-trigger="change" data-mask="99-99-9999" data-parsley-errors-container="#date_of_appointment_errors">
                                <span class="input-group-addon bg-primary b-0 text-white"><i class="icon-calender"></i></span>
                            </div>
                            <div class="parsley-custom-errors" id="date_of_appointment_errors"></div>  
                        </div>

                        <label for="appointed_as_id" class="col-sm-2 control-label">Appointed As <small>*</small></label>
                        <div class="col-sm-4 mb-15">
                            <select name="appointed_as_id" id="appointed_as_id" class="form-control select2 designationOption" required data-parsley-trigger="change" data-parsley-errors-container="#appointed_as_id_errors"></select>
                            <div class="parsley-custom-errors" id="appointed_as_id_errors"></div>                    
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="desgn_id" class="col-sm-2 control-label">Designation <small>*</small></label>
                        <div class="col-sm-4 mb-15">
                            <select name="desgn_id" id="desgn_id" class="form-control select2 designationOption" required data-parsley-trigger="change" data-parsley-errors-container="#desgn_id_errors"></select>
                            <div class="parsley-custom-errors" id="desgn_id_errors"></div>                    
                        </div>

                        <label for="payscale_id" class="col-sm-2 control-label">BPS <small>*</small></label>
                        <div class="col-sm-4 mb-15">
                            <select name="payscale_id" id="payscale_id" class="form-control" required data-parsley-trigger="change" data-parsley-errors-container="#payscale_id_errors"></select>
                            <div class="parsley-custom-errors" id="payscale_id_errors"></div>                    
                        </div>
                    </div>

                    <div class="form-group">                        
                        <label for="service_status_id" class="col-sm-2 control-label">Service Status <small>*</small></label>
                        <div class="col-sm-4 mb-15">
                            <select name="service_status_id" id="service_status_id" class="form-control select2" required data-parsley-trigger="change" data-parsley-errors-container="#service_status_id_errors"></select>
                            <div class="parsley-custom-errors" id="service_status_id_errors"></div>                    
                        </div>

                        <label for="zone_id" class="col-sm-2 control-label">Zone/Area <small>*</small></label>
                        <div class="col-sm-4">
                            <select name="zone_id" id="zone_id" class="form-control select2" data-parsley-trigger="change" data-parsley-errors-container="#zone_id_errors"></select>
                            <div class="parsley-custom-errors" id="zone_id_errors"></div>                    
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="gender_id" class="col-sm-2 control-label">Gender <small>*</small></label>
                        <div class="col-sm-4 mb-15">
                            <select name="gender_id" id="gender_id" class="form-control select2" required data-parsley-trigger="change" data-parsley-errors-container="#gender_id_errors"></select>
                            <div class="parsley-custom-errors" id="gender_id_errors"></div>  
                        </div>

                        <label for="bloodgroup_id" class="col-sm-2 control-label">Blood Group <small>*</small></label>
                        <div class="col-sm-4">
                            <select name="bloodgroup_id" id="bloodgroup_id" class="form-control select2 bloodgroupOption" data-parsley-trigger="change" data-parsley-errors-container="#bloodgroup_id_errors"></select>
                            <div class="parsley-custom-errors" id="bloodgroup_id_errors"></div>  
                        </div>                        
                    </div>

                    <div class="form-group">
                        <label for="qualification" class="col-sm-2 control-label">Qualification <small>*</small></label>
                        <div class="col-sm-4 mb-15">
                            <input type="text" name="qualification" id="qualification" class="form-control" value="" placeholder="F.A" data-parsley-trigger="change" data-parsley-length="[1, 50]" data-parsley-errors-container="#qualification_errors" list="qualification-list">
                            <datalist id="qualification-list"></datalist>
                            <div class="parsley-custom-errors" id="qualification_errors"></div>  
                        </div> 

                        <label for="disability" class="col-sm-2 control-label">Disability if any</label>
                        <div class="col-sm-4">
                            <input type="text" name="disability" id="disability" class="form-control" value="N.A" placeholder="Physical"  data-parsley-trigger="change" data-parsley-length="[1, 50]" data-parsley-errors-container="#disability_errors" list="disability-list">
                            <datalist id="disability-list"></datalist>
                            <div class="parsley-custom-errors" id="disability_errors"></div>  
                        </div>                       
                    </div>

                    <div class="form-group">                        
                        <label for="religion_id" class="col-sm-2 control-label">Religion <small>*</small></label>
                        <div class="col-sm-4 mb-15">
                            <select name="religion_id" id="religion_id" class="form-control select2" required data-parsley-trigger="change" data-parsley-errors-container="#religion_id_errors"></select>
                            <div class="parsley-custom-errors" id="religion_id_errors"></div>  
                        </div>

                        <label for="official_bike_no" class="col-sm-2 control-label">Official Bike #</label>
                        <div class="col-sm-4 mb-15">
                            <input type="text" name="official_bike_no" id="official_bike_no" class="form-control" value="" placeholder="FDJ-111" data-parsley-trigger="change" data-parsley-length="[3, 10]" data-parsley-errors-container="#official_bike_no_errors" list="official_bike_no-list">
                            <datalist id="official_bike_no-list"></datalist>
                            <div class="parsley-custom-errors" id="official_bike_no_errors"></div>  
                        </div> 
                    </div>

                    <div class="form-group">
                        <label for="permanent_address" class="col-sm-2 control-label">Address <small>*</small></label>
                        <div class="col-sm-4">
                            <input type="text" name="permanent_address" id="permanent_address" class="form-control" required value="" placeholder="Civil Colony, Kotwali Road, Faisalabad" data-parsley-trigger="change" data-parsley-length="[3, 255]" data-parsley-errors-container="#permanent_address_errors" list="permanent_address-list">
                            <datalist id="permanent_address-list"></datalist>
                            <div class="parsley-custom-errors" id="permanent_address_errors"></div>  
                        </div>

                        <label for="postal_address" class="col-sm-2 control-label">Postal Address</label>
                        <div class="col-sm-4">
                            <input type="text" name="postal_address" id="postal_address" class="form-control" value="" placeholder="Civil Colony, Kotwali Road, Faisalabad" data-parsley-trigger="change" data-parsley-length="[3, 255]" data-parsley-errors-container="#postal_address_errors" list="postal_address-list">
                            <datalist id="postal_address-list"></datalist>
                            <div class="parsley-custom-errors" id="postal_address_errors"></div>  
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inquiries" class="col-sm-2 control-label">If the inquiry is ruled out.</label>
                        <div class="col-sm-10">
                            <input type="text" name="inquiries" id="inquiries" class="form-control" value="" placeholder="" data-parsley-trigger="change" data-parsley-length="[3, 255]" data-parsley-errors-container="#inquiries_errors">
                            <div class="parsley-custom-errors" id="inquiries_errors"></div>  
                        </div>                   
                    </div>

                    <div class="form-group">
                    <label for="remarks" class="col-sm-2 control-label">Remarks</label>
                        <div class="col-sm-10">
                            <textarea name="remarks" id="remarks" class="form-control" cols="30" rows="3" maxlength="65535"></textarea>
                            <div class="parsley-custom-errors" id="remarks_errors"></div>  
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <input type="hidden" name="id" id="id" class="hidden-field" value="0">
                    <input type="hidden" name="zone_id_old" id="zone_id_old" class="hidden-field" value="0">

                    <input type="hidden" name="hidden_profile_photo" id="hidden_profile_photo" class="" value="">
                
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                <button type="submit" id="submitBtn" name="submit" class="btn btn-primary waves-effect waves-light px-20">Save</button> 
            </div>

            <?php echo form_close(); ?>

        </div> 
    </div>
</div><!-- /.modal -->

<!-- add leave modal -->
<div id="addLeaveModal" class="modal fade" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog"> 
        <div class="modal-content">
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 id="leave-modal-title" class="modal-title">Add Leave</h4> 
            </div> 
            <div class="modal-body">

            <?php echo form_open(current_url().'/addleave', array( 'class' => 'form-horizontal', 'id' => 'addLeaveForm', 'data-parsley-validat' => '')) ?>


                <div class="form-group">
                    <label class="control-label col-sm-4">Date</label>
                    <div class="col-sm-8">
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" name="start_date" id="start_date" class="form-control" required data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" data-mask="99-99-9999">
                            <span class="input-group-addon bg-primary b-0 text-white">to</span>
                            <input type="text" name="end_date" id="end_date" class="form-control" required data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" data-mask="99-99-9999">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="leavetype_id" class="col-sm-4 control-label">Leave Type</label>
                    <div class="col-sm-8">
                        <select name="leavetype_id" id="leavetype_id" class="form-control select2" required data-parsley-trigger="change" data-parsley-errors-container="#leavetype_id_errors"></select>
                        <div class="parsley-custom-errors" id="leavetype_id_errors"></div>                    
                    </div>
                </div>

                <div class="form-group">
                    <label for="leave_remarks" class="control-label col-sm-4">Remarks</label>
                    <div class="col-sm-8">
                        <textarea name="remarks" id="leave_remarks" cols="10" rows="3" class="form-control"></textarea>
                    </div>
                </div>

                <div class="text-center">
                    <span class="font-13 text-muted">Add leave carefully because you can't edit leave entry after 30 minutes.</span>
                </div>

                <input type="hidden" name="leave_staff_id" id="leave_staff_id" value="">
                <input type="hidden" name="leave_id" id="leave_id" value="0">
                

            </div> 
            <div class="modal-footer"> 
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                <button type="button" id="submitLeaveBtn" class="btn btn-primary waves-effect waves-light">Save</button> 
            </div>

            <?php echo form_close(); ?>
        </div> 
    </div>
</div><!-- /.modal -->

<!-- transfer/posting modal -->
<div id="addPostingModal" class="modal fade" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-full"> 
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 id="posting_modal_title" class="modal-title">Posting Related Performa</h4>
            </div>

            <?php echo form_open('', array( 'class' => 'form-horizontal', 'id' => 'addPostingForm', 'data-parsley-validat' => '', 'role' => 'form')) ?>

            <div class="modal-body table-responsive">

                <table id="postingTable" class="table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="7" class="text-capitalize" id="posting-form-heading">Posting Related Information</th>
                        </tr>
                        <tr class="hidden-print">
                            <td colspan="7" class="portlet m-b-0 text-capitalize">
                                <div class="alert alert-warning m-b-0">
                                    <a href="#" class="close font-10" data-toggle="remove"><i class="ion-close-round"></i></a>
                                    <p><b>Notice!</b> If you're posted in any office of <b>DSJ</b> and <b>SCJ</b> please the select Name of Court <b>"Office/English Branch"</b> or </b>"Office/COC"</b> and enter the branch name in column <b>Work Nature</b> like (IT Section, Copy Branch, Record Room, COC Office etc).</p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Name of Court</th>
                            <th>Station</th>
                            <th>Posted as</th>
                            <th>Work Nature</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody id="posting">
                        <tr class="posting-row">

                            <td>
                                <select name="posting[0][court_id]" id="court_id_0" class="form-control select2 posting_court_id" required data-parsley-errors-container="#court_id_errors_0"></select>
                                <div class="parsley-custom-errors" id="court_id_errors_0"></div>
                            </td>
                            <td>
                                <select name="posting[0][station_id]" id="station_id_0" class="form-control select2 posting_station_id" required data-parsley-errors-container="#station_id_errors_0"></select>
                                <div class="parsley-custom-errors" id="station_id_errors_0"></div>
                            </td>
                            <td>
                                <select name="posting[0][posted_as_id]" id="posted_as_id_0" class="form-control select2 posted_as_id" required data-parsley-errors-container="#posted_as_id_errors_0"></select>
                                <div class="parsley-custom-errors" id="posted_as_id_errors_0"></div>
                            </td>
                            <td>
                                <input type="text" name="posting[0][work_nature]" id="work_nature_0" class="form-control work_nature" required value="" placeholder="Civil/Criminal/Office" data-parsley-trigger="change" data-parsley-length="[3, 50]" data-parsley-trigger="focusout" data-parsley-errors-container="#work_nature_errors_0">
                                <div class="parsley-custom-errors" id="work_nature_errors_0"></div>  
                            </td>
                            <td>
                                <input type="text" name="posting[0][posting_date_from]" id="posting_date_from_0" class="form-control datepicker" required value="" placeholder="dd-mm-yyyy" data-parsley-trigger="focusout" data-parsley-date data-mask="99-99-9999" data-parsley-errors-container="#posting_date_from_errors_0">
                                <div class="parsley-custom-errors" id="posting_date_from_errors_0"></div>
                            </td>
                            <td>
                                <input type="text" name="posting[0][posting_date_to]" id="posting_date_to_0" class="form-control datepicker" value="" placeholder="dd-mm-yyyy" data-parsley-trigger="focusout" data-parsley-date data-mask="99-99-9999" data-parsley-errors-container="#posting_date_to_errors_0">
                                <div class="parsley-custom-errors" id="posting_date_to_errors_0"></div> 
                            </td>
                            <td>
                                <input type="hidden" name="posting[0][staff_id]" id="posting_staff_id_0" class="hidden-field" value="0">
                                <input type="hidden" name="posting[0][id]" id="posting_id_0" class="hidden-field" value="0">
                                <button class="btn btn-success btn-sm addPosting" type="button" data-rel="tooltip" title="Add More posting Record!"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
                            </td>

                        </tr>
                    </tbody>

                </table>
            </div>
            
            <div class="modal-footer">
                <div class="col-sm-12 ">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                    <button type="submit" id="submitPostingBtn" name="submit" class="btn btn-primary waves-effect waves-light">Save</button> 
                </div>
            </div>
            
            <?php echo form_close(); ?>

        </div> 
    </div>
</div>            
<!-- end transfer/posting modal -->

<!-- print model -->
<div id="printModal" class="modal fade" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg"> 
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 id="view-staff-data_title" class="modal-title">Staff Profile</h4> 
            </div>

            <div class="modal-body table-responsive">

                <table class="staffDataTable table table-bordered">
                    <tr>
                        <th colspan="4">Personal Information</th>
                    </tr>                    
                    <tr>
                        <th class="col-sm-2">Name of the Official</th>
                        <td colspan="2" class="view-name text-capitalize"></td>
                        <td rowspan="5" class="col-sm-4">
                            <div class="text-center view-profile-photo"><img src="" alt="" widht="160" height="180"></div>
                        </td>
                    </tr>
                    <tr>
                        <th>Father's Name</th>
                        <td colspan="2" class="view-father text-capitalize"></td>
                    </tr>
                    <tr>
                        <th>CNIC No.</th>
                        <td colspan="2" class="view-cnic-no"></td>
                    </tr>                    
                    <tr>
                        <th>Contact No.</th>
                        <td colspan="2" class="view-contact-no"></td>
                    </tr>
                    <tr>
                        <th>Date of Birth</th>
                        <td colspan="2" class="view-date-of-birth"></td>
                    </tr>
                    <tr>
                        <th>Domicile</th>
                        <td class="view-domicile"></td>
                        <th class="col-sm-2">Zone Area</th>
                        <td class="view-zone"></td>
                    </tr>
                    <tr>
                        <th>Qualification</th>
                        <td class="view-qualification"></td>
                        <th class="col-sm-2">Disability if any</th>
                        <td class="view-disability"></td>
                    </tr>
                    <tr>
                        <th>Gender</th>
                        <td class="view-gender"></td>
                        <th>Blood Group</th>
                        <td class="view-bloodgroup"></td>
                    </tr>
                    <tr>
                        <th>Permanent Address</th>
                        <td colspan="3" class="view-permanent-address text-capitalize"></td>
                    </tr>
                    <tr>
                        <th>Present Address</th>
                        <td colspan="3" class="view-postal-address text-capitalize"></td>
                    </tr>
                </table>

                <table class="staffDataTable table table-bordered text-center">
                    <tr>
                        <th colspan="5">Service Related Information</th>
                    </tr>
                    <tr>
                        <th>Date of Appointment</th>
                        <td class="view-date-of-appointment"></td>
                        <th colspan="3">Promotion Related Information (if any)</th>
                    </tr>
                    <tr>
                        <th>Personnel No.</th>
                        <td class="view-personnel-number"></td>
                        <th>Sr.#</th>
                        <th>Date of Promotion</th>
                        <th>Promoted as</th>
                    </tr>
                    <tr>
                        <th rowspan="">Appointed as</th>
                        <td rowspan="" class="view-appointed-as"></td>
                        <th>1</th>
                        <td class="view-date-of-promotion-0"></td>
                        <td class="view-promoted-designation-0"></td>
                    </tr>
                    <tr>
                        <th rowspan="">Current Designation</th>
                        <td rowspan="" class="view-designation"></td>
                        <th>2</th>
                        <td class="view-date-of-promotion-1"></td>
                        <td class="view-promoted-designation-1"></td>
                    </tr>
                </table>

                <table class="staffDataTable table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="5">Leave Record</th>
                        </tr>
                        <tr>
                            <th>Leave</th>
                            <th>Days</th>
                            <th>Date From</th>
                            <th>Till Date</th>                            
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody id="view-leaves">                        
                    </tbody>
                </table>
                
            </div>
            
            <div class="modal-footer">
                <div class="col-sm-12">
                    <a href="#" class="btn btn-primary btn-rounded" id="profile-print" data-rel="tooltip" title="Print this page!" target="_blank"><span class="btn-label"><i class="fa fa-print"></i></span>Print</a>
                    <!-- <button type="button" class='btn btn-sm btn-primary btn-rounded waves-effect waves-light' id="employee-profile-print" employee_profile_print="" onClick="EmployeeProfilePrint()" data-rel='tooltip' title='Employee Performa Print!'><span class='btn-label'><i class='fa fa-print'></i></span> Print</button> -->
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        
        </div> 
    </div>
</div>            
<!-- end print model -->

<script>
    $(document).ready(function(){
        let table = "<?=$table?>";

        $('.datepicker-all').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            todayHighlight: true,
            endDate: '+0d', // future dates disabled
            daysOfWeekHighlighted: '0,5',
            clearBtn: true,
        });

        $('#date-range').datepicker({
			format: 'dd-mm-yyyy',
            autoclose: true,
        	todayHighlight: true,
            startDate: '+0d',
        	daysOfWeekDisabled: '0',
        	daysOfWeekHighlighted: '0,5',
            clearBtn: true,
        });

        dropdownOptions('.bloodgroupOption', 'opbloodgroup', '', '', currentURL+'/getOptions/', 0);
        dropdownOptions('#payscale_id', 'oppayscale', '', '', currentURL+'/getOptions/', 0);
        dropdownOptions('#service_status_id', 'opservicestatus', '', '', currentURL+'/getOptions/', 0);
        dropdownOptions('#gender_id', 'opgender', '', '', currentURL+'/getOptions/', 0);
        dropdownOptions('#religion_id', 'opreligion', '', '', currentURL+'/getOptions/', 0);
        dropdownOptions('#leavetype_id', 'opleavetypes', '', '', currentURL+'/getOptions/', 0);

        // dropdownOptions('#appointed_as_id', 'designations', 'designation_type', 0, currentURL+'/getOptions/', 0);
        dropdownOptions('.designationOption', 'designations', 'designation_type', 0, currentURL+'/getOptions/', 0);
        dropdownOptions('#domicile_id', 'cities', 'dist_id', 'NULL', currentURL+'/getOptions/', 0);
        dropdownOptions('#zone_id', 'zones', '', '', currentURL+'/getOptions/', 0);

        // search form
        // dropdownOptions('#desgn_id_search', 'designations', 'designation_type', 0, currentURL+'/getOptions/', 0);
        dropdownOptions('#zone_id_search', 'zones', '', '', currentURL+'/getOptions/', 0);
        // dropdownOptions('#bloodgroup_id_search', 'opbloodgroup', '', '', currentURL+'/getOptions/', 0);

        $('#searchForm').on('submit', function(e) {
            e.preventDefault();

            var $searchForm = $(this);
            if ($searchForm.parsley().validate()) {

                dataTable.ajax.reload(null, true);
            }
        });

        dataTable = $('#dataTables').DataTable({
            "dom":"<'row print-hidden'<'col-sm-3'l><'col-sm-4'B><'col-sm-3'f><'col-sm-2 add-btn text-right text-md-center'>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            "buttons": [
                {
                    extend: 'colvis',
                    text: 'Columns',
                    titleAttr: 'Show/Hide Columns',                
                    collectionLayout: 'two-column',
                    columns: [':visible:not(.not-export-col)'],
                    className: "tb-button btn btn-sm btn-primary",                    
                },                   
                {
                    extend: 'excelHtml5',
                    text: 'Excel',
                    titleAttr: 'Export Excel',
                    exportOptions: {
                        columns: [':visible:not(.not-export-col):not(.hidden, .hidden-print)'],
                    },
                    className: "tb-button btn btn-sm btn-btn-success"
                },
                {
                    extend: "pdfHtml5",
                    text: 'PDF',
                    titleAttr: 'Create PDF',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',                    
                    className: "tb-button btn btn-sm btn-primary",
                }
            ],
            "paging": true,
            "autoWidth": false,
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "order": [],
            "searchDelay":2000,
            "ajax": {
                url: currentURL + '/fetchAllData',
                type: 'POST',
                data: {
                    desgn_id : function() { return $('#desgn_id_search').val() },
                    zone_id: function() { return $('#zone_id_search').val() },
                    bloodgroup_id: function() { return $('#bloodgroup_id_search').val() },
                },
            },
            "columnDefs": [
                { width: '6%', targets: 0 },                
                { width: '10%', targets: 3 },
                { width: '12%', targets: 4 },
                { width: '8%', targets: [5, 8, 9] },
                { width: '6%', targets: [11, 12] },                               
                { width: '8%', targets: [14, 16, 18] },     
                { width: '10%', targets: -1 },     
                { visible: false, targets: [ 4,5,6,7,8,9,10,11,13,14,15,17 ] },
                { className: "text-center", targets: [ 0,3,4,5,6,7,8,9,10,11,12,14,16,17,18 ] },
                { className: "text-center hidden-print", targets: -1 },
            ],
            "initComplete": function() {
                $(".dataTables_filter input")
                .unbind()
                .bind("keypress keyup", function(e) { 
                    if(e.keyCode == 13) {
                        dataTable.search(this.value).draw();
                    }
                    if(this.value == "") {
                        dataTable.search("").draw();
                    }
                    return;
                });

                $('.tb-button').attr('data-rel', 'tooltip').attr('data-container', 'body');

                $("div.add-btn").html("<button class='btn btn-sm btn-primary btn-rounded waves-effect waves-light' data-toggle='modal' data-target='#addModal' data-rel='tooltip' data-container='body' title='Add New City'><span class='btn-label'><i class='fa fa-plus'></i></span> Add</button>");
            },
            "language": {
                "emptyTable": "No Record Found!",                
            },
            "responsive": true,            
            "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ]
        });

        $('#submitBtn').click(function(evt){
            // evt.preventDefault();
            let submitForm = $('#addForm');

            // FormData object 
            let data = new FormData($(submitForm)[0]);

            if( submitForm.parsley().validate() )
            {
                $.ajax({
                    url: submitForm.attr('action'),
                    type: submitForm.attr('method'),
                    enctype: submitForm.attr('enctype'),
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    beforeSend:function(){
                        $('#submitBtn').attr('disabled','disabled');
                        $('#submitBtn').text('Saving...');
                    },
                    success: function(response){

                        var resp = JSON.parse(response);

                        if(resp.message_type == 'success' || resp.message_type == 'warning') {

                            $.Notification.autoHideNotify(resp.message_type, 'top center',
                            resp.message);

                            // submitForm[0].reset();
                            $('#id').val(0);

                            $('#name').val('');
                            $('#name_ur').val('');
                            $('#father').val('');
                            $('#cnic_no').val('');
                            $('#personnel_number').val('');
                            $('#contact_no').val('');
                            $('#date_of_birth').val('');
                            $('#date_of_appointment').val('');
                            $('#qualification').val('');
                            $('#official_bike_no').val('');
                            $('#driving_license_type').val('');
                            $('#permanent_address').val('');
                            $('#postal_address').val('');
                            $('#inquiries').val('');
                            $('#remarks').val('');

                            // $('#desgn_id').empty().trigger('change');
                            // $('#domicile_id').empty().trigger('change');
                            // $('#zone_id').empty().trigger('change');

                            $('#bloodgroup_id').empty().trigger('change');
                            
                            submitForm.find('[autofocus]').focus();

                            $('#submitBtn').attr('disabled',false);
                            $('#submitBtn').text('Save');

                            if(resp.id > 0) {
                                $('.modal').modal('hide');                     
                            }

                            // photo preview clear
                            $('.photo-preview').html('<img id="profile_photo_preview">');
                            document.getElementById("profile_photo_close").style.display = "none";
                            document.getElementById("label-photo").style.color = "black";

                            // photo preview clear
                            // $('.photo-preview').html('<img id="profile_photo_preview">');
                            // $("#profile_photo").val(null);
                            // $("#hidden_profile_photo").val('');
                            // $("#profile_photo_close").css("display", "none");

                            dataTable.ajax.reload(null, false);

                        } else {
                            $.Notification.autoHideNotify(resp.message_type, 'top center',
                            resp.message);
                        }
                    },
                    error: function(){
                        $.Notification.autoHideNotify('error', 'top center',
                        'Error: This case could not save!');
            
                        $('.modal').modal('hide');
                    }

                });

            }
        });

        $(document).on('click', '#editBtn', function() {

            $('#modal_title').text('Edit Staff Performa of ' + $(this).attr('name'));
            $('#id').val( $(this).attr('staff_id') );            
            $('#name').val( $(this).attr('name') );
            $('#name_ur').val( $(this).attr('name_ur') );
            $('#father').val( $(this).attr('father') );
            $('#cnic_no').val( $(this).attr('cnic_no') );
            $("#date_of_birth").datepicker("update", $(this).attr('date_of_birth') );
            $("#date_of_appointment").datepicker("update", $(this).attr('date_of_appointment') );

            // $('#pay_scale').val( $(this).attr('pay_scale') );
            // $('#status').val( $(this).attr('status') );
            $('#personnel_number').val( $(this).attr('personnel_number') );
            
            $('#qualification').val( $(this).attr('qualification') );
            // $('#gender').val( $(this).attr('gender') );
            // $('#blood_group').val( $(this).attr('blood_group') );
            // $('#religion').val( $(this).attr('religion') );
            $('#disability').val( $(this).attr('disability') );
            $('#contact_no').val( $(this).attr('contact_no') );
            $('#permanent_address').val( $(this).attr('permanent_address') );
            $('#postal_address').val( $(this).attr('postal_address') );
            $('#inquiries').val( $(this).attr('inquiries') );
            $('#remarks').val( $(this).attr('remarks') );

            $('#official_bike_no').val( $(this).attr('official_bike_no') );
            $('#driving_license_type').val( $(this).attr('driving_license_type') );
            
            $('#sorting').val( $(this).attr('sorting') );

            if($(this).attr('appointed_as_id')) {
                selectedOption('#appointed_as_id', 'designations', currentURL+'/getOptions/'+$(this).attr('appointed_as_id'));
            }
            if($(this).attr('desgn_id')) {
                selectedOption('#desgn_id', 'designations', currentURL+'/getOptions/'+$(this).attr('desgn_id'));
            }
            if($(this).attr('payscale_id')) {
                selectedOption('#payscale_id', 'oppayscale', currentURL+'/getOptions/'+$(this).attr('payscale_id'));
            }
            if($(this).attr('service_status_id')) {
                selectedOption('#service_status_id', 'opservicestatus', currentURL+'/getOptions/'+$(this).attr('service_status_id'));
            }
            if($(this).attr('domicile_id')) {
                selectedOption('#domicile_id', 'cities', currentURL+'/getOptions/'+$(this).attr('domicile_id'));
            }
            if($(this).attr('gender_id')) {
                selectedOption('#gender_id', 'opgender', currentURL+'/getOptions/'+$(this).attr('gender_id'));
            }
            if($(this).attr('bloodgroup_id')) {
                selectedOption('#bloodgroup_id', 'opbloodgroup', currentURL+'/getOptions/'+$(this).attr('bloodgroup_id'));
            }
            if($(this).attr('religion_id')) {
                selectedOption('#religion_id', 'opreligion', currentURL+'/getOptions/'+$(this).attr('religion_id'));
            }
            if($(this).attr('zone_id')) {
                selectedOption('#zone_id', 'zones', currentURL+'/getOptions/'+$(this).attr('zone_id'));
            }
            $('#zone_id_old').val( $(this).attr('zone_id') );

            let profile_photo    = $(this).attr('profile_photo');
            $('#hidden_profile_photo').val(profile_photo);
            if(profile_photo){
                let src = baseURL + '/uploads/profile/' + profile_photo;
                let preview = document.getElementById("profile_photo_preview");
                    preview.src = src;
                    preview.style.display = "block";

                $("#profile_photo_close").css("display", "block");
            }else{
                $('.photo-preview').html('<img id="profile_photo_preview">');
            }

        });

        $(document).on('click', '#addLeaveBtn', function() {
            $('#leave-modal-title').text('Add Leave of ' + $(this).attr('name'));
            // let leave_staff_id = $(this).attr('staff_id');
            $('#leave_staff_id').val( $(this).attr('staff_id') );
        });

        $(document).on('click', '#editLeaveBtn', function(et) {
            et.preventDefault();

            if( permission_check($(this).attr('cr_date'), 60) == false )
            {
                $.Notification.autoHideNotify('warning', 'top center',
                'You can\'t permission to edit after 1 hour!');

                return false;
            }else{
                $("#addLeaveModal").modal('show');

                $('#leave-modal-title').text('Edit Leave of ' + $(this).attr('name'));
                $("#start_date").datepicker("update", $(this).attr('start_date') );
                $("#end_date").datepicker("update", $(this).attr('end_date') );
                if($(this).attr('leavetype_id')) {
                    selectedOption('#leavetype_id', 'opleavetypes', currentURL+'/getOptions/'+$(this).attr('leavetype_id'));
                }
                $('#leave_remarks').text( $(this).attr('remarks') );
                $('#leave_staff_id').val( $(this).attr('staff_id') );
                $('#leave_id').val( $(this).attr('leave_id') );
            }
        });

        $('#submitLeaveBtn').click(function(et){
            // et.preventDefault();

            let submitForm = $('#addLeaveForm');
            if( submitForm.parsley().validate() )
            {
                $.ajax({
                    url: submitForm.attr('action'),
                    type: submitForm.attr('method'),
                    data: submitForm.serialize(),
                    dataType: 'json',
                    beforeSend:function(){
                        $(this).attr('disabled', 'disabled');
                        $(this).text('Saving...');
                    },
                    success: function(response){

                        // console.log(response);

                        if(response.message_type == 'success') {

                            $.Notification.autoHideNotify(response.message_type, 'top center',
                            response.message);

                            submitForm[0].reset();

                            $(this).attr('disabled',false);
                            $(this).text('Save');

                            $('.modal').modal('hide');

                            dataTable.ajax.reload(null, false);

                        } else {
                            $.Notification.autoHideNotify(response.message_type, 'top center',
                            response.message);
                        }
                    },
                    error: function(){
                        $.Notification.autoHideNotify('error', 'top center', 'Error in the database!');
            
                        $('.modal').modal('hide');
                    }
                });
            }
        });

        $(document).on('click', '#printBtn', function() {
            let staff_id = $(this).attr('staff_id');
            
            // $("#employee-profile-print").attr("employee_profile_print",staff_id);
            $("#profile-print").attr("href", currentURL + "/print/" + staff_id);

            $('.view-profile-photo').html("<img src='"+$(this).attr('profile_photo')+"' alt='"+$(this).attr('name')+"' width='160', height='180'>");

            $('.view-name').text( $(this).attr('name') );
            $('.view-father').text( $(this).attr('father') );
            $('.view-cnic-no').text( $(this).attr('cnic_no') );
            
            $('.view-contact-no').text( $(this).attr('contact_no') );
            $('.view-date-of-birth').text( $(this).attr('date_of_birth') );
            
            $('.view-domicile').text( $(this).attr('domicile') );
            $('.view-zone').text( $(this).attr('zone') );

            $('.view-disability').text( $(this).attr('disability') );
            $('.view-qualification').text( $(this).attr('qualification') );
            $('.view-gender').text( $(this).attr('gender') );
            $('.view-bloodgroup').text( $(this).attr('bloodgroup') );
            $('.view-permanent-address').text( $(this).attr('permanent_address') );
            
            $('.view-postal-address').text( $(this).attr('postal_address') );

            $('.view-personnel-number').text( $(this).attr('personnel_number') );
            $('.view-date-of-appointment').text( $(this).attr('date_of_appointment') );
            $('.view-designation').text( $(this).attr('designation') + ' (' + $(this).attr('payscale') + ')' );
            $('.view-appointed-as').text( $(this).attr('appointed_as') );

            /* leavesRecord */
            if(staff_id){
                $.ajax({
                    url: currentURL + '/fetch_leave_record/' + staff_id,
                    type: 'post',
                    dataType: 'json',
                    
                    success: function (data) {

                        // console.log("Leaves Data length: ", data.length);
                        // console.log("Leaves Data: ", data);

                        let objTo = $('#view-leaves').html("");

                        if(data.length > 0 ) {
                            for(let i = 0; i < data.length; i++) {
                               
                                rowLeavesData = '<tr><td class="text-capitalize"> '+data[i].leavetype+' </td><td class="text-center"> '+countDays(data[i].start_date, data[i].end_date)+' </td><td class="text-center"> '+data[i].startdate+' </td><td class="text-center"> '+data[i].enddate+' </td><td class="text-center"> '+data[i].remarks+'</tr>';

                                objTo.append(rowLeavesData);
                            }
                        } else {
                            objTo.html('<tr><td class="text-center" colspan="5"> No Record Found! </td></tr>');
                        }
                    }                
                });
            }

        });

        $(document).on('click', '#profile_photo_close', function() {

            // photo preview clear
            $('.photo-preview').html('<img id="profile_photo_preview">');
            document.getElementById("profile_photo_close").style.display = "none";
            document.getElementById("label-photo").style.color = "black";

            $("#profile_photo").val(null);
            $("#hidden_profile_photo").val('');


            if( $("#submitBtn").is(":disabled") ) {
                $('#submitBtn').attr('disabled',false);
                $('#submitBtn').text('Save');
            }
        });

        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });

        function countDays(firstDate, secondDate) {

            const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
            const start_date = new Date(firstDate);
            const end_date = new Date(secondDate);

            return  Math.round(Math.abs((start_date - end_date) / oneDay)) + 1;
        }

        /* $('.modal').on('shown.bs.modal', function() {
            $('#addForm').find('[autofocus]').focus();
        }); */

        $('.modal').on('hidden.bs.modal', function(){        
            $('#appointed_as_id').empty().trigger('change');
            $('#desgn_id').empty().trigger('change');
            $('#domicile_id').empty().trigger('change');
            $('#zone_id').empty().trigger('change');

            $('#service_status_id').empty().trigger('change');
            $('#gender_id').empty().trigger('change');
            $('#bloodgroup_id').empty().trigger('change');
            $('#payscale_id').empty().trigger('change');
            $('#religion_id').empty().trigger('change');            
            
            $('#leave_id').val(0);
            $('#leave_staff_id').val(0);
            $('#leavetype_id').empty().trigger('change');

            $('#addLeaveForm')[0].reset();            
            $('#addLeaveForm').parsley().reset();

            $('#leave-modal-title').text('Add Leave');

            // photo preview clear
            $('.photo-preview').html('<img id="profile_photo_preview">');
            $("#profile_photo").val(null);
            $("#hidden_profile_photo").val('');
            $("#profile_photo_close").css("display", "none");

            $('#addForm').parsley().destroy();
        });

        $(".form-control").change(function(){        
            if(this.value.length > 3) {
                $(this).parsley().validate();
            }
        });

        // on first focus (bubbles up to document), open the menu
        /* $(document).on('focus', '.select2-selection.select2-selection--single', function (e) {
            $(this).closest(".select2-container").siblings('select:enabled').select2('open');
        }); */

        // $(document).tooltip({ selector: '[data-rel="tooltip"]', trigger: 'hover' });

        searchField('#name', table, 'name', '#name-list');
        searchField('#father', table, 'father', '#father-list');
        searchField('#qualification', table, 'qualification', '#qualification-list');
        searchField('#disability', table, 'disability', '#disability-list');
        searchField('#official_bike_no', table, 'official_bike_no', '#official_bike_no-list');
        searchField('#permanent_address', table, 'permanent_address', '#permanent_address-list');
        searchField('#postal_address', table, 'postal_address', '#postal_address-list');
    });

    // profile photo preview
    function showPreview(event){
        if(event.target.files.length > 0){
            var src = URL.createObjectURL(event.target.files[0]);
            var preview = document.getElementById("profile_photo_preview");
            preview.src = src;
            preview.style.display = "block";

            document.getElementById("profile_photo_close").style.display = "block";
            document.getElementById("label-photo").style.color = "transparent";
        }
    }
</script>
