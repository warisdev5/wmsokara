<?php defined('BASEPATH') or die('Restricted access'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">

            <!-- <div class="search-form hidden-print px-15 m-b-20"> -->
            <div class="form custom-form-box m-b-20 hidden-print">
                <h4 class="search-heading">Search Warrant</h4>
                <form action="javascript:void(0);" id="searchForm" class="" data-parsley-validate>                   
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="court_id-search">Court Name</label>
                            <select id="court_id-search" class="form-control select2 courtsOptions"></select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="warrant_type_id-search">Warrant</label>
                            <select id="warrant_type_id-search" class="form-control select2 warrantsOptions"></select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="staff_id-search">Bailiff</label>
                            <select id="staff_id-search" class="form-control select2"></select>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="date_of_hearing-search">Date of Hearing</label>                        
                            <input type="text" id="date_of_hearing-search" value="" class="form-control datepicker" placeholder="dd-mm-yyyy" data-parsley-trigger="focusout" data-parsley-date data-mask="99-99-9999">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="case_title-search">Plaintiff / Defendat</label>
                            <input type="text" id="case_title-search"value="" placeholder="Type Plaintiff/Defendat Name" class="form-control" maxlength="99" autocomplete="off" list="case_title-search-list">
                            <datalist id="case_title-search-list"></datalist>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="defendant-search">Judgement Debtor</label>
                            <input type="text" id="defendant-search"value="" placeholder="Type Judgement Debtor" class="form-control" maxlength="99" autocomplete="off" list="defendant-search-list">
                            <datalist id="defendant-search-list"></datalist>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group m-t-30">
                            <button type="submit" id="submit" class="btn btn-sm btn-primary btn-rounded"><span class="btn-label"><i class="fa fa-search"></i></span> Search</button>
                        </div>
                    </div>
                    
                </form>
            </div>

            <div class="clearfix"></div>

            <div class="table-responsive">
                <table id="dataTables" class="table table-striped table-bordered dt-responsive">
                    <thead>
                        <tr>
                            <th>Computer#</th>
                            <th>Bailiff</th>
                            <th>Warrant</th>
                            <th>Court Name</th>
                            <th>Case Title</th>
                            <th>Judgement Debtor/<br>Address</th>
                            <th>Date of Hearing</th>
                            <th>Report/<br>Report Date</th>                                              
                            <th>Zone</th>
                            <th>Remarks</th>
                            <th class="hidden-print">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<!-- add warrant modal -->
<div id="addModal" class="modal fade" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog"> 
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 id="modal-title-warrant" class="modal-title">Add Warrant</h4> 
            </div>

            <div class="modal-body">

                <?= form_open(current_url().'/add', array( 'class' => 'form-horizontal-0', 'id' => 'addForm', 'data-parsley-validat' => '', 'role' => 'form')) ?>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="warrant_type_id">Warrant</label>
                                <select name="warrant_type_id" id="warrant_type_id" class="form-control dropdownOptions warrantsOptions" required data-parsley-trigger="change" data-parsley-errors-container="#warrant_type_id_errors"></select>
                                <div class="parsley-custom-errors" id="warrant_type_id_errors"></div> 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="court_id">Court Name</label>
                                <select name="court_id" id="court_id" class="form-control dropdownOptions courtsOptions" required data-parsley-trigger="change" data-parsley-errors-container="#court_id_errors"></select>
                                <div class="parsley-custom-errors" id="court_id_errors"></div> 
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="date_of_hearing">Date of Hearing</label>
                                <div class="input-group">
                                    <input type="text" name="date_of_hearing" id="date_of_hearing" class="form-control datepicker-f" required value="" placeholder="dd-mm-yyyy" data-parsley-trigger="change" data-mask="99-99-9999" data-parsley-errors-container="#date_of_hearing_errors">
                                    <span class="input-group-addon bg-primary b-0 text-white"><i class="icon-calender"></i></span>
                                </div>
                                <div class="parsley-custom-errors" id="date_of_hearing_errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="dist_id">District</label>
                                <select name="dist_id" id="dist_id" class="form-control dropdownOptions" required data-parsley-trigger="change" data-parsley-errors-container="#dist_id_errors"></select>
                                <div class="parsley-custom-errors" id="dist_id_errors"></div> 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="zone_id">Zone/Area</label>
                                <select name="zone_id" id="zone_id" class="form-control dropdownOptions" required data-parsley-trigger="change" data-parsley-errors-container="#zone_id_errors"></select>
                                <div class="parsley-custom-errors" id="zone_id_errors"></div> 
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="case_title">Case Title</label>
                                <input type="text" name="case_title" id="case_title" class="form-control" required value="" placeholder="abc vs xyz..." data-parsley-trigger="change" data-parsley-length="[3, 255]" data-parsley-errors-container="#case_title_errors" list="case_title-list">
                                <datalist id="case_title-list"></datalist>
                                <div class="parsley-custom-errors" id="case_title_errors"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="plaintiff">Plaintiff</label>
                                <input type="text" name="plaintiff" id="plaintiff" class="form-control" value="" placeholder="abc..." data-parsley-trigger="change" data-parsley-length="[3, 99]" data-parsley-errors-container="#plaintiff_errors" list="plaintiff-list">
                                <datalist id="plaintiff-list"></datalist>
                                <div class="parsley-custom-errors" id="plaintiff_errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="defendant">Defendant (for Warrant)</label>
                                <input type="text" name="defendant" id="defendant" class="form-control" required value="" placeholder="xyz..." data-parsley-trigger="change" data-parsley-length="[3, 99]" data-parsley-errors-container="#defendant_errors" list="defendant-list">
                                <datalist id="defendant-list"></datalist>
                                <div class="parsley-custom-errors" id="defendant_errors"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="address">Defendant Address</label>
                                <input type="text" name="address" id="address" class="form-control" required value="" placeholder="xyz..." data-parsley-trigger="change" data-parsley-length="[3, 255]" data-parsley-errors-container="#address_errors" list="address-list">
                                <datalist id="address-list"></datalist>
                                <div class="parsley-custom-errors" id="address_errors"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="bailiff_edit">
                        <div class="col-sm-12">
                            <div class="checkbox checkbox-primary">
                                <input id="is_bailiff_on_leave" type="checkbox" name="is_bailiff_on_leave" value="1">
                                <label for="is_bailiff_on_leave">If bailiff is on leave then assigned to other bailiff?</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">      
                                <label for="remarks">Remarks:</label>
                                <textarea name="remarks" id="remarks" class="form-control" maxlength="999"></textarea>
                            </div>
                        </div>
                    </div>
                    
                    <input type="hidden" name="id" id="id" value="0">
                    <input type="hidden" name="staff_id" id="staff_id" value="0">

                <?= form_close(); ?>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                <button type="submit" id="submitBtn" name="submit" class="btn btn-primary waves-effect waves-light px-20">Save</button> 
            </div>
             
        </div> 
    </div>
</div><!-- /.modal add warrant -->

<!-- report modal -->
<div id="reportModal" class="modal fade" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog"> 
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 id="modal-title-report" class="modal-title">Add Report</h4> 
            </div>

            <div class="modal-body">

                <?php echo form_open(current_url().'/add_report', array( 'class' => '', 'id' => 'addReportForm', 'data-parsley-validat' => '')) ?>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="warrant_report_id">Warrant</label>
                                <select name="warrant_report_id" id="warrant_report_id" class="form-control dropdownOptions" required data-parsley-trigger="change" data-parsley-errors-container="#warrant_report_id_errors"></select>
                                <div class="parsley-custom-errors" id="warrant_report_id_errors"></div> 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="report_date">Report Date</label>
                                <div class="input-group">
                                    <input type="text" id="report_date" class="form-control" disabled value="<?= date('d-m-Y')?>" placeholder="dd-mm-yyyy" data-parsley-trigger="change" data-mask="99-99-9999" data-parsley-errors-container="#report_date_errors">
                                    <span class="input-group-addon bg-primary b-0 text-white"><i class="icon-calender"></i></span>
                                </div>
                                <div class="parsley-custom-errors" id="report_date_errors"></div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">      
                                <label for="report_remarks">Remarks:</label>
                                <textarea name="report_remarks" id="report_remarks" class="form-control" maxlength="999"></textarea>
                            </div>
                        </div>
                    </div>
                    
                    <input type="hidden" name="report_id" id="report_id" value="0">

                <?php echo form_close(); ?>

            </div>

            <div class="modal-footer"> 
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                <button type="button" id="submitReportBtn" class="btn btn-primary waves-effect waves-light">Save</button> 
            </div>
             
        </div> 
    </div>
</div><!-- /.modal add warrant -->

<script>
    $(document).ready(function() {
        let table = "<?=$table?>";
        
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            todayHighlight: true,
            // startDate: '+0d', // past dates disabled
            daysOfWeekDisabled: '0',
            daysOfWeekHighlighted: '0,5',
            clearBtn: true,
        });
        $('.datepicker-f').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            todayHighlight: true,
            startDate: '+0d', // past dates disabled
            daysOfWeekDisabled: '0',
            daysOfWeekHighlighted: '0,5',
            clearBtn: true,
        });

        dropdownOptions('.warrantsOptions', 'warrant_type', 'parent_id', 'NULL', currentURL+'/getOptions/', 0);
        dropdownOptions('#dist_id', 'cities', 'dist_id', 'NULL', currentURL+'/getOptions/', 0);
        dropdownOptions('#staff_id-search', 'staff', 'desgn_id', 39, currentURL+'/getOptions/', 0);
        dropdownOptions('#zone_id', 'zones', '', '', currentURL+'/getOptions/', 0);
        dropdownOptions('.courtsOptions', 'courts', '', '', currentURL+'/courtsOptions/', 0);
        // dropdownOptions('#court_id-search', 'courts', '', '', currentURL+'/courtsOptions/', 0);

        $('#searchForm').on('submit', function(e) {
            e.preventDefault();

            var $searchForm = $(this);
            if ($searchForm.parsley().validate()) {

                dataTable.ajax.reload(null, true);
            }
        });

        dataTable = $('#dataTables').DataTable({            
            "dom":"<'row print-hidden'<'col-sm-3'l><'col-sm-4'B><'col-sm-3'f><'col-sm-2 add-btn text-right text-md-center'>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",            
            "buttons": [
                {
                    extend: 'colvis',
                    text: 'Columns',
                    titleAttr: 'Show/Hide Columns',                
                    collectionLayout: 'two-column',
                    columns: [':visible:not(.not-export-col)'],
                    className: "tb-button btn btn-sm btn-primary",                    
                },                   
                {
                    extend: 'excelHtml5',
                    text: 'Excel',
                    titleAttr: 'Export Excel',
                    exportOptions: {
                        columns: [':visible:not(.not-export-col):not(.hidden, .hidden-print)'],
                    },
                    className: "tb-button btn btn-sm btn-btn-success"
                },
                {
                    extend: "pdfHtml5",
                    text: 'PDF',
                    titleAttr: 'Create PDF',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',                    
                    className: "tb-button btn btn-sm btn-primary",
                }
            ],
            "paging": true,
            "autoWidth": false,
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "order": [],
            "searchDelay":2000,
            "ajax": {
                url: currentURL + '/fetchAllData',
                type: 'POST',
                data: {
                    court_id: function() { return $('#court_id-search').val() },
                    warrant_type_id: function() { return $('#warrant_type_id-search').val() },
                    staff_id: function() { return $('#staff_id-search').val() },
                    date_of_hearing: function() { return $('#date_of_hearing-search').val() },
                    case_title: function() { return $('#case_title-search').val() },
                    defendant: function() { return $('#defendant-search').val() },
                },
            },
            "columnDefs": [
                { width: '14%', targets: 5 },
                { width: '10%', targets: [ 6, 7 ] },
                { visible: false, targets: [ 8, 9 ] },
                { className: "text-center", targets: [ 0, 6, 7, 8, 9 ] },
                { className: "text-center hidden-print", targets: [ -1 ] },
            ],
            "initComplete": function() {
                $(".dataTables_filter input")
                .unbind()
                .bind("keypress keyup", function(e) { 
                    if(e.keyCode == 13) {
                        dataTable.search(this.value).draw();
                    }
                    if(this.value == "") {
                        dataTable.search("").draw();
                    }
                    return;
                });

                $("div.add-btn").html("<button class='btn btn-sm btn-primary btn-rounded waves-effect waves-light' data-toggle='modal' data-target='#addModal' data-rel='tooltip' data-container='body' title='Add New Warrant'><span class='btn-label'><i class='fa fa-plus'></i></span> Add</button>");
            },
            "language": {
                "emptyTable": "No Record Found!",                
            },
            "responsive": true,            
            "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ]
        });

        $('#submitBtn').click(function(evt){
            // evt.preventDefault();
            let submitForm = $('#addForm');

            if( submitForm.parsley().validate() )
            {
                $.ajax({
                    url: submitForm.attr('action'),
                    type: submitForm.attr('method'),
                    data: submitForm.serialize(),
                    dataType: 'json',
                    beforeSend:function(){
                        $('#submitBtn').attr('disabled','disabled');
                        $('#submitBtn').text('Saving...');
                    },
                    success: function(resp){

                        $.Notification.autoHideNotify(resp.message_type, 'top center', resp.message);

                        if(resp.message_type == 'success') {

                            $('.modal').modal('hide');  

                            $('#id').val(0);
                            $('#staff_id').val(0);

                            dataTable.ajax.reload(null, false);
                        }
                        
                    },
                    error: function(){
                        $.Notification.autoHideNotify('error', 'top center',
                        'Error in the database while saving record!');
            
                        // $('.modal').modal('hide');
                    }

                });

            }
        });

        $(document).on('click', '#editBtn', function() {

            $('#modal-title-warrant').text('Edit Warrant No. CN-' + $(this).attr('edit_id') )

            if( permission_check($(this).attr('cr_date'), 14400) == false )
            {
                $.Notification.autoHideNotify('warning', 'top center',
                'You can\'t permission to edit after 10 days!');

                return false;

            } else if($(this).attr('report_date').length > 0) {
                $.Notification.autoHideNotify('warning', 'top center',
                'You can\'t permission to edit after report submitted!');
                return false;

            }else{
                $("#addModal").modal('show');

                if($(this).attr('warrant_type_id')) {
                    selectedOption('#warrant_type_id', 'warrant_type', currentURL+'/getOptions/'+$(this).attr('warrant_type_id'));
                }
                if($(this).attr('court_id')) {
                    selectedOption('#court_id', 'courts', currentURL+'/courtsOptions/'+$(this).attr('court_id'));
                }
                if($(this).attr('dist_id')) {
                    selectedOption('#dist_id', 'cities', currentURL+'/getOptions/'+$(this).attr('dist_id'));
                }
                let zone_id = $(this).attr('zone_id') ? $(this).attr('zone_id') : 10 ;                
                if(zone_id) {
                    selectedOption('#zone_id', 'zones', currentURL+'/getOptions/'+zone_id);
                }

                $('#id').val( $(this).attr('edit_id') );
                $('#staff_id').val( $(this).attr('staff_id') );
                $("#date_of_hearing").datepicker("update", $(this).attr('date_of_hearing') );
                $('#case_title').val( $(this).attr('case_title') );
                $('#plaintiff').val( $(this).attr('plaintiff') );
                $('#defendant').val( $(this).attr('defendant') );
                $('#address').val( $(this).attr('address') );
                $('#remarks').val( $(this).attr('remarks') );
            }

            
        });

        $(document).on('click', '#addReportBtn', function() {

            // $('#warrant_report_id').val(null).trigger('change');

            dropdownOptions('#warrant_report_id', 'warrant_type', 'parent_id', $(this).attr('warrant_type_id'), currentURL+'/getOptions/', 0);

            $('#report_id').val( $(this).attr('edit_id') );
            $('#report_remarks').val( $(this).attr('report_remarks') );

            $('#modal-title-report').text('Report of Warrant No. CN-' + $(this).attr('edit_id') )

        });

        $(document).on('click', '#editReportBtn', function() {

            var md_date = $(this).attr('md_date');

            if( permission_check(md_date, 3600) == false )
            {
                $.Notification.autoHideNotify('error', 'top center',
                'You can\'t permission to edit after 2 days!');

                return false;
            }else{
                $("#reportModal").modal('show');

                if($(this).attr('warrant_report_id')) {
                    selectedOption('#warrant_report_id', 'warrant_type', currentURL+'/getOptions/'+$(this).attr('warrant_report_id'));
                }

                dropdownOptions('#warrant_report_id', 'warrant_type', 'parent_id', $(this).attr('warrant_type_id'), currentURL+'/getOptions/', 0);

                // $("#report_date").datepicker("update", $(this).attr('report_date') );
                $("#report_date").val( $(this).attr('report_date') );

                $('#report_id').val( $(this).attr('edit_id') );
                $('#report_remarks').val( $(this).attr('report_remarks') );

                $('#modal-title-report').text('Report of Warrant No. CN-' + $(this).attr('edit_id') );
            }

        });

        $('#submitReportBtn').click(function(et){
            // evt.preventDefault();
            let submitForm = $('#addReportForm');

            if( submitForm.parsley().validate() )
            {
                $.ajax({
                    url: submitForm.attr('action'),
                    type: submitForm.attr('method'),
                    data: submitForm.serialize(),
                    dataType: 'json',
                    beforeSend:function(){
                        $('#submitBtn').attr('disabled','disabled');
                        $('#submitBtn').text('Saving...');
                    },
                    success: function(resp){

                        $.Notification.autoHideNotify(resp.message_type, 'top center', resp.message);

                        if(resp.message_type == 'success') {

                            $('.modal').modal('hide'); 

                            $('#report_id').val(0);
                            
                            dataTable.ajax.reload(null, false);
                        }
                    },
                    error: function(){
                        $.Notification.autoHideNotify('error', 'top center',
                        'Error: This case could not save!');
            
                        $('.modal').modal('hide');
                    }

                });

            }
        });

        $('.modal').on('shown.bs.modal', function() {
            
            $('#addReportForm').find('[autofocus]').focus();
            
            if($('#id').val() > 0)
            {                
                $('#bailiff_edit').removeClass('hidden');
                $('#bailiff_edit').addClass('show');
            }
            else
            {
                $('#bailiff_edit').removeClass('show');
                $('#bailiff_edit').addClass('hidden');
            }
        });

        $('.modal').on('hidden.bs.modal', function(){
            
            $('#staff_id').val(0);

            $('#report_id').val(0);

            $('.dropdownOptions').val('').trigger('change');

            $('#warrant_report_id').empty().trigger('change');
        
            $('#addForm').parsley().reset();

            $('#addReportForm')[0].reset();  
            $('#addReportForm').parsley().reset();

            $('#modal-title-warrant').text('Add Warrant');
            $('#modal-title-report').text('Add Report of Warrant');
        });

        $(".form-control").change(function(){
            if(this.value.length > 3) {
                $(this).parsley().validate();
            }
        });

        searchField('#case_title-search', table, 'case_title', '#case_title-search-list');
        searchField('#defendant-search', table, 'defendant', '#defendant-search-list');

        searchField('#case_title', table, 'case_title', '#case_title-list');
        searchField('#plaintiff', table, 'plaintiff', '#plaintiff-list');
        searchField('#defendant', table, 'defendant', '#defendant-list');
        searchField('#address', table, 'address', '#address-list');

    });
</script>