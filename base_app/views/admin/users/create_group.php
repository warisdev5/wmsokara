<div class="col-md-4 col-md-offset-4">
	<div class="panel panel-border panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title"><?php echo lang('create_group_heading'); ?></h3>
		</div>
					
		<div class="panel-body">

			<p><?php echo lang('create_group_subheading');?></p>

			<div id="infoMessage"><?php echo $message;?></div>

			<?php echo form_open(current_url());?>
			
			      <div class="form-group">
			            <?php echo lang('create_group_name_label', 'group_name');?> <br />
			            <?php echo form_input($group_name);?>
			      </div>
			
			      <div class="form-group">
			            <?php echo lang('create_group_desc_label', 'description');?> <br />
			            <?php echo form_input($description);?>
			      </div>
			
			      <div class="form-group"><?php echo form_submit('submit', lang('create_group_submit_btn'), 'class="btn btn-primary btn-rounded"');?></div>
			
			<?php echo form_close();?>
			
		</div>
	</div>
</div>