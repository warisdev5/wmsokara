<?php defined('BASEPATH') or die('Restricted access');?>

<div class="row">
	<div class="col-sm-12">
		<div class="card-box table-responsive">

			<table id="dataTables" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="text-left"><?php echo lang('index_fname_th');?></th>
						<th class="text-left"><?php echo lang('index_lname_th');?></th>
						<th class="text-left"><?php echo lang('index_uname_th');?></th>
						<th class="text-left"><?php echo lang('index_email_th');?></th>
						<th class="text-left"><?php echo lang('index_phone_th');?></th>
						<th class="text-left"><?php echo lang('index_groups_th');?></th>
						<th class="text-left"><?php echo lang('index_status_th');?></th>
						<th class="text-left"><?php echo lang('index_action_th');?></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($users as $user):?>
					<tr>
						<td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
						<td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
						<td><?php echo htmlspecialchars($user->username,ENT_QUOTES,'UTF-8');?></td>
						<td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
						<td><?php echo htmlspecialchars($user->phone,ENT_QUOTES,'UTF-8');?></td>
						<td>
							<?php foreach ($user->groups as $group):?>
								<?php echo anchor("admin/users/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8'), [ 'class' => 'btn waves-effect waves-light', 'data-rel' => 'tooltip', 'data-container' => 'body', 'title' => 'Edit Group!' ]) ;?><br />
							<?php endforeach?>
						</td>
						<td><?php echo ($user->active) ? anchor("admin/users/deactivate/".$user->id, lang('index_active_link'), [ 'class' => 'btn waves-effect waves-light', 'data-rel' => 'tooltip', 'data-container' => 'body', 'title' => 'User Inactive!' ]) : anchor("admin/users/activate/". $user->id, lang('index_inactive_link'), [ 'class' => 'btn waves-effect waves-light', 'data-rel' => 'tooltip', 'data-container' => 'body', 'title' => 'User Active!' ]);?></td>
						<td><?php echo anchor("admin/users/edit_user/".$user->id, 'Edit', ['class' => 'btn waves-effect waves-light', 'data-rel' => 'tooltip', 'data-container' => 'body', 'title' => 'Edit Record!']) ;?></td>
					</tr>
				<?php endforeach;?>
				</tbody>
			</table>
			
		</div><!-- end card-box -->
		
	</div>
</div>

<script>

    $(document).ready(function() {
		dataTable = $('#dataTables').DataTable({
            "dom":"<'row print-hidden'<'col-sm-3'l><'col-sm-4'B><'col-sm-3'f><'col-sm-2 add-btn text-right text-md-center'>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            "buttons": [              
                {
                    extend: 'excelHtml5',
                    text: 'Excel',
                    titleAttr: 'Export Excel',
                    exportOptions: {
                        columns: [':visible:not(.not-export-col):not(.hidden, .hidden-print)'],
                    },
                    className: "tb-button btn btn-sm btn-btn-success"
                },
                {
                    extend: "pdfHtml5",
                    text: 'PDF',
                    titleAttr: 'Create PDF',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',                    
                    className: "tb-button btn btn-sm btn-primary",
                }
            ],
            "columnDefs": [            
                { className: "text-center", targets: [ 2, 3, 4, 5, 6, 7 ] },
			],
            "initComplete": function() {
                $(".dataTables_filter input")
                .unbind()
                .bind("keypress keyup", function(e) { 
                    if(e.keyCode == 13) {
                        dataTable.search(this.value).draw();
                    }
                    if(this.value == "") {
                        dataTable.search("").draw();
                    }
                    return;
                });

                $('.tb-button').attr('data-rel', 'tooltip').attr('data-container', 'body');

                $("div.add-btn").html("<a href='"+currentURL+"/create_user' class='btn btn-sm btn-primary waves-effect waves-light' data-rel='tooltip' data-container='body' title='Create New User'><span class='btn-label'><i class='fa fa-plus'></i></span> Create User</a>");
            },
            "language": {
                "emptyTable": "No Record Found!",
            },
            responsive: true,
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ]
        });
    });
</script>