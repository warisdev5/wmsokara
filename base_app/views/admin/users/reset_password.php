<div class="col-md-4 col-md-offset-4">
	<div class="panel panel-border panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title"><?php echo lang('reset_password_heading'); ?></h3>
		</div>
					
		<div class="panel-body">

			<div id="infoMessage"><?php echo $message;?></div>

			<?php echo form_open('admin/users/reset_password/' . $code);?>
  			
				<div class="form-group">
					<label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label> <br />
					<?php echo form_input($new_password);?>
				</div>

				<div class="form-group">
					<?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?> <br />
					<?php echo form_input($new_password_confirm);?>
				</div>

				<?php echo form_input($user_id);?>
				<?php echo form_hidden($csrf); ?>

				<div class="form-group"><?php echo form_submit('submit', lang('reset_password_submit_btn'), 'class="btn btn-primary btn-rounded"');?></div>
			
			<?php echo form_close();?>
			
		</div>
	</div>
</div>