<div class="col-md-4 col-md-offset-4">
	<div class="panel panel-border panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title"><?php echo lang('forgot_password_heading'); ?></h3>
                  <p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
		</div>
					
		<div class="panel-body">

			<div id="infoMessage"><?php echo $message;?></div>

			<?php echo form_open(current_url()); ?>
			
			      <div class="form-group">
                              <label for="identity"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label> <br />
      	                  <?php echo form_input($identity);?>
			      </div>
			
			
			      <div class="form-group"><?php echo form_submit('submit', lang('forgot_password_heading'), 'class="btn btn-primary btn-rounded"');?></div>
			
			<?php echo form_close();?>
			
		</div>
	</div>
</div>
