<div class="col-md-4 col-md-offset-4">
	<div class="panel panel-border panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title"><?php echo lang('change_password_heading'); ?></h3>
		</div>
					
		<div class="panel-body">

			<p><?php echo lang('change_password_subheading');?></p>

			<?php echo form_open(current_url(), array( 'data-parsley-validate' => ''));?>
			
			      <div class="form-group">
			            <?php echo lang('change_password_old_password_label', 'old');?> <br />
			            <?php echo form_input($old_password);?>
			      </div>
			
			      <div class="form-group">
                              <label for="new"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label> <br />
                              <?php echo form_input($new_password);?>
			      </div>

			      <div class="form-group">
                              <?php echo lang('change_password_new_password_confirm_label', 'new_confirm');?> <br />
                              <?php echo form_input($new_password_confirm);?>
			      </div>

                        <?php echo form_input($user_id);?>

			      <div class="form-group"><?php echo form_submit('submit', lang('change_password_submit_btn'), 'class="btn btn-primary btn-rounded"');?></div>
			
			<?php echo form_close();?>
			
		</div>
	</div>
</div>