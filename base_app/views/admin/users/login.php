<?= form_open( base_url('login'), array( 'class' => 'form-horizontal','data-parsley-validate' => '') ); ?>

  <div class="form-group">
    <div class="col-xs-12">
      <i class="ti-user pull-left"></i>
      <?= form_input($identity);?>
    </div>
  </div>

  <div class="form-group">
    <div class="col-xs-12">
      <i class="ti-lock pull-left"></i>
      <?= form_input($password);?>
      <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
    </div>
  </div>

  <div class="form-group ">
    <div class="col-xs-12">
      <div class="checkbox checkbox-primary">
        <?= form_checkbox('remember', '1', FALSE, array( 'id' => 'remember') );?>
        <label for="remember">
          Remember me
        </label>
      </div>                
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-12">
    <button class="btn btn-primary btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
    </div>
  </div>

<?= form_close(); ?>
<script>
  $(document).ready(function(){
    $(".toggle-password").click(function() {
      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));      
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });

  });
</script>