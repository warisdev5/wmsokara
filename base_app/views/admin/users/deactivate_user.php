<div class="col-md-4 col-md-offset-4">
	<div class="panel panel-border panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title"><?php echo lang('deactivate_heading'); ?></h3>
		</div>
					
		<div class="panel-body">

			<p class="m-b-20"><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>

			<?php echo form_open("admin/users/deactivate/".$user->id);?>
			
			  <div class="form-group">
			  
				  <div class="radio radio-custom radio-inline">
					<input type="radio" name="confirm" id="confirm-yes" value="yes" checked="checked">
		            <?php echo lang('deactivate_confirm_y_label', 'confirm-yes');?>
				  </div>
				  <div class="radio radio-info radio-inline">
					<input type="radio" name="confirm" id="confirm-no" value="no">
		            <?php echo lang('deactivate_confirm_n_label', 'confirm-no');?>
				  </div>
				  
			  </div>
			
			  <?php echo form_hidden($csrf); ?>
			  <?php echo form_hidden(['id' => $user->id]); ?>
			
			  <div class="form-group"><?php echo form_submit('submit', lang('deactivate_submit_btn'), 'class="btn btn-primary btn-rounded"');?></div>
			
			<?php echo form_close();?>
		</div>
	</div>
</div>
