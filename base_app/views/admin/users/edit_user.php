<?php defined('BASEPATH') or die('Restricted access');?>

<div class="col-sm-6 col-sm-offset-3">
    <div class="card-box m-t-20">

        <div id="infoMessage"><?php // echo $message;?></div>
        <?php echo form_open(uri_string());?>

        <div class="row">

            <div class="col-sm-12">
                <div class="form-group">
                    <?php echo lang('edit_user_fname_label', 'first_name');?> <br />
                    <?php echo form_input($first_name);?>
                    <?php echo form_error('first_name', '<div class="error">', '</div>'); ?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <?php echo lang('edit_user_lname_label', 'last_name');?> <br />
                    <?php echo form_input($last_name);?>
                    <?php echo form_error('last_name', '<div class="error">', '</div>'); ?>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <?php echo lang('edit_user_company_label', 'company');?> <br />
                    <?php echo form_input($company);?>
                    <?php echo form_error('company', '<div class="error">', '</div>'); ?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <?php echo lang('edit_user_phone_label', 'phone');?> <br />
                    <?php echo form_input($phone);?>
                    <?php echo form_error('phone', '<div class="error">', '</div>'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <?php echo lang('edit_user_password_label', 'password');?> <br />
                    <?php echo form_input($password);?>
                    <?php echo form_error('password', '<div class="error">', '</div>'); ?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?><br />
                    <?php echo form_input($password_confirm);?>
                    <?php echo form_error('password_confirm', '<div class="error">', '</div>'); ?>
                </div>
            </div>
        </div>

        <?php if ( $this->ion_auth->in_group( array('superadmin','admin')) ): ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <h3><?php echo lang('edit_user_groups_heading');?></h3>

                    <?php foreach ($groups as $group):?>
                        <div class="checkbox checkbox-primary">
                            <?php
                                $gID=$group['id'];
                                $checked = null;
                                $item = null;
                                foreach($currentGroups as $grp) {
                                    if ($gID == $grp->id) {
                                        $checked= ' checked="checked"';
                                    break;
                                    }
                                }
                            ?>
                            <input type="checkbox" id="" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?> data-parsley-multiple="group1">
                                        
                            <label class="" for=""><?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?></label>
                        
                        </div>

                    <?php endforeach; ?>

                </div>

                
            </div>
            
        </div>

        <?php echo form_error('group_id', '<div class="error">', '</div>'); ?>

        <?php endif ?>

        <?php echo form_hidden('id', $user->id);?>
        <?php echo form_hidden($csrf); ?>

        <p><?php echo form_submit('submit', lang('edit_user_submit_btn'), 'class="btn btn-primary btn-rounded waves-light"');?>
        </p>

        <?php echo form_close();?>
    </div>
</div>
<script>
$(document).ready(function() {
    $('form').parsley();

    function userGroup() {
        var sel = document.getElementById("group_id");
        var group = sel.options[sel.selectedIndex].text;

        alert(group);

        if (group == 'members') {
            $('#user-court').addClass('hidden');
            $('#user-court').removeClass('show');
        } else {
            $('#user-court').addClass('show');
            $('#user-court').removeClass('hidden');
        }

    }

    $('#group_id').on('change', function() {
        userGroup();
    });
});
</script>