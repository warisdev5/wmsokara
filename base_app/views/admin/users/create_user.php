<!-- style="margin-top: 50px" -->
<div class="col-sm-6 col-sm-offset-3">
	<div class="card-box m-t-20">

		<div class="panel-heading">
			<h3 class="panel-title"><?php echo lang('create_user_heading'); ?></h3>
		</div>
					
		<div class="panel-body">
		
			<div id="infoMessage"><?php // echo $message;?></div>
		
			<?php echo form_open("admin/users/create_user");?>
			
			<div class="row">

				<div class="col-sm-6">
					<div class="form-group">
						<?php echo lang('create_user_fname_label', 'first_name');?> <br />
						<?php echo form_input($first_name);?>
						<?php echo form_error('first_name', '<div class="error">', '</div>'); ?>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
						<?php echo lang('create_user_lname_label', 'last_name');?> <br />
						<?php echo form_input($last_name);?>
						<?php echo form_error('last_name', '<div class="error">', '</div>'); ?>
					</div>
				</div>

				<?php
					if($identity_column!=='email') {

						echo '<div class="col-sm-6">';
						echo '<div class="form-group">';

						echo lang('create_user_identity_label', 'identity');
						echo '<br />';
						echo form_input($identity);
						echo form_error('identity', '<div class="error">', '</div>');
						echo '</div>';
						echo '</div>';
					}
				?>

				<div class="col-sm-6">
					<div class="form-group">
						<?php echo lang('create_user_email_label', 'email');?> <br />
						<?php echo form_input($email);?>
						<?php echo form_error('email', '<div class="error">', '</div>'); ?>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<?php echo lang('create_user_phone_label', 'phone');?> <br />
						<?php echo form_input($phone);?>
						<?php echo form_error('phone', '<div class="error">', '</div>'); ?>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
						<?php echo lang('create_user_company_label', 'company');?> <br />
						<?php echo form_input($company);?>
						<?php echo form_error('company', '<div class="error">', '</div>'); ?>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
						<?php echo lang('create_user_password_label', 'password');?> <br />
						<?php echo form_input($password);?>
						<?php echo form_error('password', '<div class="error">', '</div>'); ?>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
						<?php echo form_input($password_confirm);?>
						<?php echo form_error('password_confirm', '<div class="error">', '</div>'); ?>
					</div>
				</div>
				
			</div>

			<div class="form-group pull-right"><?php echo form_submit('submit', lang('create_user_submit_btn'), 'class="btn btn-primary btn-rounded"');?></div>
			
			<?php echo form_close();?>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('form').parsley();
	});
</script>