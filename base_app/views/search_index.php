<div class="row">
    <div class="col-md-12">
        <div class="search-form hidden-print px-20 m-b-20">
            <form action="#" id="searchForm" class="form-horizontal-1" data-parsley-validate>
                <!-- <div class="col-md-8 col-md-offset-2"> -->

                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label for="court_id">Court Name</label>
                            <select name="court_id" id="court_id" class="form-control select2" data-parsley-errors-container="#court_id_errors"></select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-2 col-md-2">
                        <div class="form-group">
                            <label>Date of Hearing</label>                        
                            <input type="text" name="date_of_hearing" id="date_of_hearing" value="" class="form-control datepicker" placeholder="dd-mm-yyyy" data-parsley-trigger="focusout" data-parsley-date data-mask="99-99-9999" data-parsley-errors-container="#date_of_hearing_errors">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <div class="form-group">
                            <label for="case_title">Plaintiff / defendant</label>
                            <input type="text" name="case_title" id="case_title"value="" placeholder="Type Plaintiff/Defendat Name" class="form-control" maxlength="99" autocomplete="off" list="case-title-list">
                            <datalist id="case-title-list"></datalist>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-2 col-md-2">
                        <div class="form-group">
                            <label for="defendant">Judgement Debtor</label>
                            <input type="text" name="defendant" id="defendant"value="" placeholder="Type Judgement Debtor" class="form-control" maxlength="99" autocomplete="off" list="defendant-list">
                            <datalist id="defendant-list"></datalist>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-2 col-md-2">
                        <div class="form-group mt-25">
                            <button type="submit" id="submit" class="btn btn-sm btn-primary btn-rounded"><span class="btn-label"><i class="fa fa-search"></i></span> Search</button>
                        </div>
                    </div>

                <!-- </div> -->
            </form>
        </div>

        <div class="clearfix"></div>

        <div class="table-responsive">
            <table id="dataTables" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Warrant No./<br>Nature of Warrant</th>                        
                        <th>Court's Name</th>
                        <th>Case Title</th>
                        <th>Date of Hearing</th>
                        <th>Judgement Debtor/<br>Address</th>
                        <th>Bailiff</th>                       
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

    </div>
</div>

<script>
    $(document).ready(function(){
        let baseURL = "<?= base_url() ?>";
        let currentURL = "<?= current_url() ?>";

        let dataTable, dropdownOptions, selectedOption;

        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            todayHighlight: true,
            daysOfWeekDisabled: '0', // Sunday disabled
            daysOfWeekHighlighted: '0,5', // Sunday and Friday highlighted
            clearBtn: true,
        });

        function formatState (state) {

            if (!state.id) {
                return state.text;
            }

            var text = state.text.split(',')

            if(text[1] == undefined)
            {
                var $state = $( `<span>${text[0]}</span>` );
            }else{
                var $state = $( `<span>${text[0]}</span><span class='urdu'> (${text[1]})</span>` );
            }

            return $state;
        };

        selectedOption = function(selector, table, court_type, selectorURL){
            $.ajax({
                url: selectorURL,
                type: 'post',
                dataType: 'json',
                data: { table: table, court_type : court_type },
                success: function (data) {
                    var newOption = new Option(data[0].text, data[0].id, false, true);
                    // Append it to the select
                        // $(selector).html(newOption).trigger('change');
                    $(selector).append(newOption).trigger('change');
                }
            });
        }

        dropdownOptions = function (selector, table, court_type, selectorURL) {
            $(selector).select2({
                placeholder: '--PleaseSelect--',
                allowClear: true,
                ajax: {
                    url: selectorURL,
                    type: 'post',
                    dataType: 'json',
                    delay: 500,
                    data: function (params) {
                        return {
                            searchTerm : params.term,
                            table : table,
                            court_type : court_type,
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: false,
                },
                templateResult: formatState,
                templateSelection: formatState
            });
        }

        dropdownOptions('#court_id', 'courts', '', baseURL+'search/courtsOptions/'+0);

        $('#searchForm').on('submit', function(e) {
            e.preventDefault();

            var $searchForm = $(this);
            if ($searchForm.parsley().validate()) {

                dataTable.ajax.reload(null, true);
            }
        });

        dataTable = $('#dataTables').DataTable({
            "dom":"<'row hidden-print'<'col-md-6 text-md-center'l><'col-md-6 text-md-center'f>><'row'<'col-md-12'tr>><'row'<'col-md-12 hidden-print'p>>",
            "paging": true,
            "searching": false,
            "bLengthChange": false,
            "bInfo": false,
            "autoWidth": false,
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "order": [],
            "searchDelay":2000,
            "ajax": {
                url: currentURL + 'search/fetchAllData',
                type: 'POST',
                data: {
                    court_id: function() { return $('#court_id').val() },
                    date_of_hearing: function() { return $('#date_of_hearing').val() },
                    case_title: function() { return $('#case_title').val() },
                    defendant: function() { return $('#defendant').val() },
                },
            },
            // "fixedColumns":   {
            //     leftColumns: 2
            // },
            "columnDefs": [
                { width: '10%', targets: [ 3 ] },
                // { width: '20%', targets: [ 1, 2 ] },
                // { width: '10%', targets: [ 3 ] },
                // { className: "text-capitalize text-center", targets: [ 1, 2 ] },
                { className: "text-center", targets: [ 0, 3 ] },
            ],
            "initComplete": function() {
                $(".dataTables_filter input")
                .unbind()
                .bind("keypress keyup", function(e) { 
                    if(e.keyCode == 13) {
                        dataTable.search(this.value).draw();
                    }
                    if(this.value == "") {
                        dataTable.search("").draw();
                    }
                    return;
                });

                $("div.add-btn").html("<button class='btn btn-sm btn-primary waves-effect waves-light' data-toggle='modal' data-target='#addModal' data-rel='tooltip' data-container='body' title='Add New Case!'><span class='btn-label'><i class='fa fa-plus'></i></span> Case</button>");
            },
            "language": {
                "emptyTable": "No Record Found!",
                "processing": "<img src='"+baseURL+"assets/images/preloader.gif' />"
            },
            "responsive": true,
            "lengthMenu": [
                [10, 25, 50, 100, 200, -1],
                [10, 25, 50, 100, 200, "All"]
            ]
        });

        search_data("case_title", "#case-title-list");
        search_data("defendant", "#defendant-list");

        function search_data(input_search, input_display) {

            $('#'+input_search).keyup(function() {
                
                var value = $('#'+input_search).val();

                if (value == "") {
                    $(input_display).html("");
                } else {
                    if(value.length > 2)
                    {
                        $.ajax({
                            type: "POST",
                            url: currentURL + 'search/fetchSearch',
                            data: {
                                table : 'warrants',
                                search : value,
                                field_name : input_search
                            },
                            success: function(resp) {
                                $(input_display).html(resp).show();
                            }
                        });
                    }
                }
            });
        }
    });
</script>