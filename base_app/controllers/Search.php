<?php defined('BASEPATH') or die('Restricted acess');

class Search extends WS_Controller {

    public function __construct()
    {
        parent::__construct();

        // load models
        $this->load->model('search_model');
        $this->model = $this->search_model;
        $this->table = $this->tables['warrants'];

        $fields = $this->db->list_fields($this->table);
        $this->fields = array_values( array_diff( $fields, ['id', 'sorting'] ) );

        $this->where = [];

        $this->sorting = "a.id desc";    
    }

    public function index()
    {
        // datatable
        $this->template->add_css('assets/plugins/datatables/jquery.dataTables.min.css');
        $this->template->add_css('assets/plugins/datatables/responsive.bootstrap.min.css');
        $this->template->add_css('assets/plugins/datatables/dataTables.colVis.css');
        $this->template->add_css('assets/plugins/datatables/dataTables.bootstrap.min.css');

        $this->template->add_js('assets/plugins/datatables/jquery.dataTables.min.js');
        $this->template->add_js('assets/plugins/datatables/dataTables.bootstrap.js');
        $this->template->add_js('assets/plugins/datatables/dataTables.responsive.min.js');
        $this->template->add_js('assets/plugins/datatables/responsive.bootstrap.min.js');
        $this->template->add_js('assets/plugins/datatables/dataTables.colVis.js');
        
        // datepicker
        $this->template->add_css('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');
        $this->template->add_js('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');

        // input-mask
        $this->template->add_js('assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js');

        // select2
        $this->template->add_css('assets/plugins/select2/css/select2.min.css');
        $this->template->add_js('assets/plugins/select2/js/select2.min.js');
	
		$this->template->set_page_title('Search Warrant');
		$this->template->loadview('templates/default','search_index', $this->data);
    }

    public function fetchAllData()
    {
        $draw   = intval($this->input->post("draw"));
		$start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post('search');

        $date_of_hearing = $this->input->post('date_of_hearing');
        $court_id        = $this->input->post('court_id');
        $case_title      = $this->input->post('case_title');
        $defendant      = $this->input->post('defendant');

        if($court_id !== 'null')
        {
            $this->where['a.court_id'] = (int)$court_id;
        }

        if(!empty($date_of_hearing))
        {
            $this->where['a.date_of_hearing'] = $this->dateFormat('Y-m-d', $date_of_hearing);
        }

        if(empty($case_title) && empty($defendant) && $court_id == 'null' && empty($date_of_hearing))
        {
            $this->where['a.case_title'] = $case_title;
        }

        // var_dump(empty($case_title) && $court_id == 'null' && empty($date_of_hearing) );die();

        // var_dump($this->where);die();

        // $query = $this->model->fetchAllData($this->table, $start, $length, $search);
        $query = $this->model->fetchAllData($this->table, $this->where, $this->sorting, $this->fields, $start, $length, $search);

        $data = [];

        $i = $start;

        foreach ($query->result() as $r)
        {
            $data[] = array(
                "CN-$r->id<br>$r->warrant",
                $r->courtname,
                $r->case_title,
                $r->date_of_hearing,
                "$r->defendant<br>$r->address",
                $r->bailiff
            );

        }

        $result = array(
            "draw" => $draw,
            "recordsTotal" => $this->model->countAll($this->table, []),
            "recordsFiltered" => $this->model->countFiltered($this->table, $this->where, $this->sorting, $this->fields, $start, $length, $search),
            "data" => $data
        );
    
        echo json_encode($result);
        exit();
    }

}