<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Error_pages extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->template->set_title('Page not found');
		$this->template->loadview('templates/error_page','errors/page-404');
	}

/* 	public function error()
	{
		$this->template->set_title('Error Page');
		$this->template->loadview('templates/error_page','errors/error');	
	} */
}
