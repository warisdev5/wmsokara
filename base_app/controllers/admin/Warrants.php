<?php defined('BASEPATH') or die('Restricted acess');

class Warrants extends WS_Controller {

    public function __construct()
    {
        parent::__construct();

        // load models
        $this->load->model('warrants_model');
        $this->model = $this->warrants_model;
        $this->table = $this->tables['warrants'];
        $this->data['table'] = $this->table;

        $fields = $this->db->list_fields($this->table);
        $this->fields = array_values( array_diff( $fields, ['id', 'sorting'] ) );        

        $this->sorting = "a.id desc";
    }
    public function index()
    {
        // updated DataTables
		$this->template->add_css('assets/plugins/uDataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/uDataTables/datatables.min.js');
        
        // datepicker
        $this->template->add_css('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');
        $this->template->add_js('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');
        // notify
        $this->template->add_js('assets/plugins/notifyjs/js/notify.js');
        $this->template->add_js('assets/plugins/notifications/notify-metro.js');
        // input-mask
        $this->template->add_js('assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js');
        // select2
        $this->template->add_css('assets/plugins/select2/css/select2.min.css');
        $this->template->add_js('assets/plugins/select2/js/select2.min.js');

        $this->template->set_title('List of Warrant');
		$this->template->loadview('templates/default_admin','admin/warrants_index', $this->data);
    }
    public function fetchAllData()
    {
        $draw   = intval($this->input->post("draw"));
		$start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post('search');

        $court_id        = $this->input->post('court_id');
        $warrant_type_id = $this->input->post('warrant_type_id');
        $staff_id        = $this->input->post('staff_id');

        $date_of_hearing = $this->input->post('date_of_hearing');
        // $case_title      = $this->input->post('case_title');
        // $defendant      = $this->input->post('defendant');

        if($court_id !== 'null')
        {
            $this->where['a.court_id'] = (int)$court_id;
        }
        if($warrant_type_id !== 'null')
        {
            $this->where['a.warrant_type_id'] = (int)$warrant_type_id;
        }
        if($staff_id !== 'null')
        {
            $this->where['a.staff_id'] = (int)$staff_id;
        }

        if(!empty($date_of_hearing))
        {
            $this->where['a.date_of_hearing'] = $this->dateFormat('Y-m-d', $date_of_hearing);
        }        

        $query = $this->model->fetchAllData($this->table, $this->where, $this->sorting, $this->fields, $start, $length, $search);

        $data = [];

        foreach ($query->result() as $r)
        {
            $id = $this->encrypt_decrypt($r->id, 'encrypt');

            $button = "<div class='input-group-btn'>";

            $button .= "<a href='#' id='editBtn' edit_id='".$r->id."' warrant_type_id='".$r->warrant_type_id."' staff_id='".$r->staff_id."' zone_id='".$r->zone_id."' court_id='".$r->court_id."' dist_id='".$r->dist_id."' date_of_hearing='".$r->dateOfHearing."' case_title='".$r->case_title."' plaintiff='".$r->plaintiff."' defendant='".$r->defendant."' address='".$r->address."' remarks='".$r->remarks."' report_date='".$r->reportDate."' cr_date='".$r->cr_date."' created_date='".$r->created_date."' data-rel='tooltip' title='Edit Warrant!' class='btn btn-sm btn-custom btn-primary waves-effect waves-light'><i class='fa fa-edit'></i></a>";
            
            $button .= "</div>";

            $reportBtn = $r->warrant_report_id ? "<a href='#' id='editReportBtn' edit_id='".$r->id."' warrant_type_id='".$r->warrant_type_id."' warrant_report_id='".$r->warrant_report_id."' report_date='".$r->report_date."' md_date='".$r->md_date."' report_remarks='".$r->remarks."' data-rel='tooltip' title='Edit Warrant Report!'>$r->warrant_report<br><small>$r->reportDate</small></a>" : "<a href='#' id='addReportBtn' edit_id='".$r->id."' warrant_type_id='".$r->warrant_type_id."' report_remarks='".$r->remarks."' data-toggle='modal' data-target='#reportModal' data-rel='tooltip' title='Add Warrant Report!' class='btn btn-warning p-y3-x6'>Pending</a>";

            $data[] = array(
                "<a href='".base_url('admin/warrants/detail/'.$id)."' target='_blank'>CN-$r->id</a>",
                $r->baillif ? $r->baillif : $r->district,
                $r->warrant_name,
                $r->court,
                $r->case_title,                
                $r->defendant."<br>".$r->address,
                $r->date_of_hearing ? (empty($r->report_date) && $r->date_of_hearing < date('Y-m-d') ? "<span class='btn-warning p-y3-x6'>$r->dateOfHearing</span>" : $r->dateOfHearing) : "<span class='urdu text-primary'>$r->warrant_name_urdu</span>",
                $reportBtn,
                $r->zone,
                $r->remarks,
                $button
            );
        }

        $result = array(
            "draw" => $draw,
            "recordsTotal" => $this->model->countAll($this->table, []),
            "recordsFiltered" => $this->model->countFiltered($this->table, $this->where, $this->sorting, $this->fields, $start, $length, $search),
            "data" => $data
        );
    
        echo json_encode($result);
        exit();
        
    }
    public function detail($id=NULL)
    {
        $warrent_id = $this->encrypt_decrypt($id, 'decrypt');

        $this->data['warrant'] = $this->model->fetch_warrant($warrent_id);
        $this->data['warrantHistory'] = $this->model->fetchUpdateHistory($warrent_id);
        
        if(!$warrent_id)
        {
            $this->session->set_flashdata('message','Record not found!');
            $this->session->set_flashdata('message_type','danger');
            redirect( base_url('admin/warrants') );
        }

		$this->template->set_title('Warrant Detail');
		$this->template->loadview('templates/default_admin','admin/warrant_detail', $this->data);
    }
    
    public function add()
    {
        $today = date('Y-m-d');

        $data = $this->input->post();

        $id = $data['id'];

        if($data['zone_id'] == 10 && $data['dist_id'] != 15)
        {
            $data['staff_id'] = '';

            $result = $this->_save($this->table, $data);
        }
        else
        {
            $fetchRow = $this->model->fetchRow('zone_id', $this->tables['staff'], [ 'id' => $data['staff_id'] ], 'id desc');
            $zone_id = ( isset($fetchRow) ? $fetchRow->zone_id : 0 );

            if( !isset($data['is_bailiff_on_leave']) && ($zone_id == $data['zone_id']) )
            {
                $result = $this->_save($this->table, $data);
            }
            else
            {
                // fetchTable($select, $table, $where, $orderBy)
                $bailiffsAssignedHalqa = $this->model->fetchTable('id', $this->tables['staff'], array( 'zone_id' => $data['zone_id']), 'id asc');

                if( !empty($bailiffsAssignedHalqa) )
                {
                    // if($data['warrant_type_id'] == 36)
                    // {
                    //     $presentBaillifs = $bailiffsAssignedHalqa;
                    // }
                    // else
                    // {
                        $bailiffsOnLeave = $this->model->fetchTable('staff_id as id', $this->tables['staff_leaves'], '"'.$today.'" BETWEEN start_date AND end_date', 'id asc');
                        $presentBaillifs = array_diff($bailiffsAssignedHalqa, $bailiffsOnLeave);                        
                    // }

                    if( !in_array($data['staff_id'], $presentBaillifs) )
                    {
                        if($data['warrant_type_id'] == 36)
                        {
                            $staff_id   = $this->_assignBailiffPossession($presentBaillifs, $data['warrant_type_id']);
                        }
                        else
                        {
                            $staff_id   = $this->_assignBailiff($presentBaillifs);
                        }                     
                        
                        $data['staff_id'] = $staff_id;

                        if( $id > 0 )
                        {
                            if($this->model->save_history($this->tables['warrant_history'], $data) )
                            {
                                $result['save_history'] = "Warrant # CN-$id saved successfully";
                            }   
                        }
                    }

                    $result = $this->_save($this->table, $data);
                }
                else
                {
                    $result['message_type'] = 'error';
                    $result['message']      = 'Bailiff is not available in the selected zone.';
                }
            }
        }

        echo json_encode($result);
        die();
    }
    public function _save($table, $data)
    {
        if( $this->model->save($table, $data) > 0 )
        {
            $message['id']  = $data['id'];
            $message['message_type']    = 'success';
            $message['message']         = 'The warrant saved successfully.';
        }
        else
        {
            $message['message_type'] = 'warning';
            $message['message']      = 'No changes have been made to the record.';
        }

        return $message;
    }
    // **_assignBailiff
    public function _assignBailiff($presentBaillifs)
    {    
        $warrantsOf3Days = [];
        $warrantsOf7Days = [];
        $warrantsOf30Days = [];
        foreach($presentBaillifs as $key => $val)
        {
            // countAll($table, $where)
            $warrantsOf3Days[$val] = $this->model->countAll($this->table, [ "staff_id" => $val, 'cr_date >=' => $this->previous_days('00:00:00',3), 'cr_date <=' => $this->previous_days('23:59:59', 0) ]);
            $warrantsOf7Days[$val] = $this->model->countAll($this->table, [ "staff_id" => $val, 'cr_date >=' => $this->previous_days('00:00:00',7), 'cr_date <=' => $this->previous_days('23:59:59', 0) ]);
            $warrantsOf30Days[$val] = $this->model->countAll($this->table, [ "staff_id" => $val, 'cr_date >=' => $this->previous_days('00:00:00',30), 'cr_date <=' => $this->previous_days('23:59:59', 0) ]);
        }
        
        $minWarrantsOf3Days = array_keys($warrantsOf3Days, min($warrantsOf3Days));
        $minWarrantsOf7Days = array_keys($warrantsOf7Days, min($warrantsOf7Days));
        $minWarrantsOf30Days = array_keys($warrantsOf30Days, min($warrantsOf30Days));
                    
        if( count(array_unique($warrantsOf3Days)) > 1 )
        {
            $key = array_rand($minWarrantsOf3Days);
            return $staff_id = $minWarrantsOf3Days[$key];
        }
        
        elseif( count(array_unique($warrantsOf7Days)) > 1 )
        {
            $key = array_rand($minWarrantsOf7Days);    
            return $staff_id = $minWarrantsOf7Days[$key];                                 
        }
        else
        {
            $key = array_rand($minWarrantsOf30Days);
            return $staff_id = $minWarrantsOf30Days[$key];                
        }
    }

    public function _assignBailiffPossession($presentBaillifs, $warrant_type_id)
    {
        $warrantsOf7Days = [];
        $warrantsOf30Days = [];

        foreach($presentBaillifs as $key => $val)
        {
            $warrantsOf7Days[$val] = $this->model->countAll($this->table, [ "staff_id" => $val, 'warrant_type_id' => $warrant_type_id, 'cr_date >=' => $this->previous_days('00:00:00',7), 'cr_date <=' => $this->previous_days('23:59:59', 0) ]);
            $warrantsOf30Days[$val] = $this->model->countAll($this->table, [ "staff_id" => $val, 'warrant_type_id' => $warrant_type_id, 'cr_date >=' => $this->previous_days('00:00:00',30), 'cr_date <=' => $this->previous_days('23:59:59', 0) ]);
        }

        $minWarrantsOf7Days = array_keys($warrantsOf7Days, min($warrantsOf7Days));
        $minWarrantsOf30Days = array_keys($warrantsOf30Days, min($warrantsOf30Days));
            
        if( count(array_unique($warrantsOf7Days)) > 1 )
        {
            $key = array_rand($minWarrantsOf7Days);    
            return $staff_id = $minWarrantsOf7Days[$key];                                 
        }
        else
        {
            $key = array_rand($minWarrantsOf30Days);
            return $staff_id = $minWarrantsOf30Days[$key];                
        }
    }

    public function previous_days($formatTime, $days)
    {
        return date("Y-m-d $formatTime", strtotime("$days days ago", time()));
    }

    public function add_report()
    {
        $data = $this->input->post();

        if( $this->model->save_report($this->table, $data) > 0 )
        {
            $message['report_id']  = $data['report_id'];
            $message['message_type']    = 'success';
            $message['message']         = 'The warrant report saved successfully.';
        }
        else
        {
            $message['message_type'] = 'error';
            $message['message']      = 'Error in the database while saving case!';
        }

        echo json_encode($message);
        die();
    }
}