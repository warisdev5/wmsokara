<?php defined('BASEPATH') or die('Restricted acess');

class Cities extends WS_Controller {

    public function __construct()
    {
        parent::__construct();

    
        $this->table = $this->tables['cities'];        
        $this->data['table'] = $this->table;

        $fields = $this->db->list_fields($this->table);
        $this->fields = array_values( array_diff( $fields, ['id', 'sorting', 'dist_id', 'active_cms'] ) );        
        $this->where    = [ 'dist_id' => NULL ];

        //load urdu editor script and file
		$this->template->add_header_js('assets/js/urdu-webpad.js');
		$this->template->add_js_script('initUrduEditor()');
    }
    public function index()
    {
        // updated DataTables
		$this->template->add_css('assets/plugins/uDataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/uDataTables/datatables.min.js');

        // notify
        $this->template->add_js('assets/plugins/notifyjs/js/notify.js');
        $this->template->add_js('assets/plugins/notifications/notify-metro.js');
        
        // select2
        $this->template->add_css('assets/plugins/select2/css/select2.min.css');
        $this->template->add_js('assets/plugins/select2/js/select2.min.js');

		$this->template->set_title('Cities List');
		$this->template->loadview('templates/default_admin','admin/cities_index', $this->data);
        
    }

    public function fetchAllData()
    {
        $draw   = intval($this->input->post("draw"));
		$start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post('search');

        $query = $this->model->fetchAllData($this->table, $this->where, $this->sorting, $this->fields, $start, $length, $search);

        $fetchRecord = $query->result();

        foreach( $fetchRecord as $row)
        {
            if($row->dist_id === NULL)
            {
                // fetchTable($select, $table, $where, $orderBy)
                $row->tehsils = $this->model->fetchTable('*', $this->table, ['dist_id' => $row->id], "sorting asc, name asc");
            }
        }

        $data = [];

        foreach ($fetchRecord as $r)
        {
            $data[] = array(                
                $r->name ." <span class='urdu'>($r->name_ur)</span> ". ($r->active_cms == 1 ? '<i class="md md-done text-success"></i>' : ''),
                "<a href='#' id='editBtn' edit_id='".$r->id."' name='".$r->name."' name_ur='".$r->name_ur."' dist_id='".$r->dist_id."' active_cms='".$r->active_cms."' sorting='".$r->sorting."' data-toggle='modal' data-target='#addModal' data-rel='tooltip' data-container='body' title='Edit Record!' class='btn btn-sm btn-custom btn-primary waves-effect waves-light'><i class='fa fa-edit'></i></a>"
            );

            if(isset($r->tehsils))
            {
                foreach($r->tehsils as $teh)
                {
                    $data[] = array(                        
                        "<span class='p-l-80 fa fa-angle-right'> $teh->name <span class='urdu'>($teh->name_ur)</span> </span>",
                        "<a href='#' id='editBtn' edit_id='".$teh->id."' name='".$teh->name."' name_ur='".$teh->name_ur."' dist_id='".$teh->dist_id."' active_cms='".$teh->active_cms."' sorting='".$teh->sorting."' data-toggle='modal' data-target='#addModal' data-rel='tooltip' data-original-title='Edit Record!' class='btn btn-sm btn-custom btn-primary waves-effect waves-light'><i class='fa fa-edit'></i></a>"
                    );
                }
                $i = 0;
            }
            
        }

        $result = array(
            "draw" => $draw,
            "recordsTotal" => $this->model->countAll($this->table, $this->where),
            "recordsFiltered" => $this->model->countFiltered($this->table, $this->where, $this->sorting, $this->fields, $start, $length, $search),
            "data" => $data
        );
    
        echo json_encode($result);
        exit();

    }

    public function add()
    {
        $data = $this->input->post();

        // set validation
        // $this->form_validation->set_rules('name', 'city name', 'required|callback_validate_city_name_is_unique['.$id.']');
        $this->form_validation->set_rules('name', 'name', 'required|callback_validate_name_is_unique['.$data['id'].']');
        $this->form_validation->set_rules('active_cms', 'active cms', 'callback_validate_acitve_cms_on_unique_district['.$data['id'].']');
        // $this->form_validation->set_rules('name_ur', 'name in urdu', 'required');

        if( $this->form_validation->run() === TRUE )
        {
            if( $this->model->save($this->table, $data) > 0 )
            {
                $message['message_type'] = 'success';
                $message['message']      = 'This record has saved!';
                $message['id']           = $data['id'];
                // $message['name']  = $data['name'];
            }
            else
            {
                $message['message_type'] = 'error';
                $message['message']      = 'This record could not be saved!';
            }            
        }
        else
        {
            $message['message_type'] = 'error';
            // $message['message']   = 'Please filled required (*) fields!';
            $message['message']      = validation_errors();
        }

        echo json_encode($message);
        die();
    }

    public function validate_name_is_unique($name, $id)
    {
        $this->form_validation->set_message('validate_name_is_unique', "This $name is already exist!");

        // fetchTable($select, $table, $where, $orderBy)
        $fetchItems = $this->model->fetchTable('name', $this->table, [ 'id !=' => $id, 'dist_id' => $this->input->post('dist_id') ], 'id asc');

        foreach( $fetchItems as $item )
        {
            if( strtolower($item->name) == strtolower($name) )
            {
                return FALSE;
            }
        }

        return TRUE;
    }

    public function validate_acitve_cms_on_unique_district($active_cms, $id)
    {
        $this->form_validation->set_message('validate_acitve_cms_on_unique_district', 'CMS is already active on other district!');
		
        // fetchTable($select, $table, $where, $orderBy)
        $fetchItems = $this->model->fetchTable('active_cms', $this->table, ['id !=' => $id, 'dist_id' => $this->input->post('dist_id')], 'id asc');

        foreach( $fetchItems as $item )
        {
            if( $item->active_cms == $active_cms )
            {
                return FALSE;
            }
        }
        return TRUE; 
    }
}