<?php defined('BASEPATH') or die('Restricted acess');

class Zones extends WS_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->table = $this->tables['zones'];
        $this->data['table'] = $this->table;

        $fields = $this->db->list_fields($this->table);
        $this->fields = array_values( array_diff( $fields, ['id', 'sorting'] ) );;

        //load urdu editor script and file
		$this->template->add_header_js('assets/js/urdu-webpad.js');
		$this->template->add_js_script('initUrduEditor()');
    }
    // courts type
    public function index()
    {
        // updated DataTables
		$this->template->add_css('assets/plugins/uDataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/uDataTables/datatables.min.js');

        // notify
        $this->template->add_js('assets/plugins/notifyjs/js/notify.js');
        $this->template->add_js('assets/plugins/notifications/notify-metro.js');
        
        // select2
        $this->template->add_css('assets/plugins/select2/css/select2.min.css');
        $this->template->add_js('assets/plugins/select2/js/select2.min.js');

        $this->template->set_title('Zones List');
		$this->template->loadview('templates/default_admin','admin/zones_index', $this->data);
    }

    public function fetchAllData()
    {
        $draw   = intval($this->input->post("draw"));
		$start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post('search');

        $query = $this->model->fetchAllData($this->table, $this->where, $this->sorting, $this->fields, $start, $length, $search);

        $data = [];

        foreach ( $query->result() as $r )
        {
            $name_ur = $r->name_ur ? " <span class='urdu'>($r->name_ur)</span>" : "";

            $data[] = array(                                
                $r->name .$name_ur,
                $r->description,
                "<a href='#' id='editBtn' edit_id='".$r->id."' name='".$r->name."' name_ur='".$r->name_ur."' description='".$r->description."' sorting='".$r->sorting."' data-toggle='modal' data-target='#addModal' data-rel='tooltip' title='Edit Zone Name!' class='btn btn-sm btn-custom btn-primary waves-effect waves-light'><i class='fa fa-edit'></i></a>",
            );

        }

        $result = array(
            "draw" => $draw,
            "recordsTotal" => $this->model->countAll($this->table, $this->where),
            "recordsFiltered" => $this->model->countFiltered($this->table, $this->where, $this->sorting, $this->fields, $start, $length, $search),
            "data" => $data
        );
    
        echo json_encode($result);
        exit();
    }

    public function add()
    {
        $data = $this->input->post();

        $this->form_validation->set_rules('name', 'name', 'required|callback_validate_name_is_unique['.$data['id'].']');

        if( $this->form_validation->run() === TRUE )
        {
            if( $this->model->save($this->table, $data) > 0 )
            {
                $message['message_type'] = 'success';
                $message['message']      = 'This record has saved!';
                $message['id']           = $data['id'];
                // $message['name']         = $data['name'];
            }
            else
            {
                $message['message_type'] = 'error';
                $message['message']      = 'This record could not be saved!';
            }            
        }
        else
        {
            $message['message_type'] = 'error';
            // $message['message']   = 'Please filled required (*) fields!';
            $message['message']      = validation_errors(' ', ' ');
        }

        echo json_encode($message);
        die();
    }

    public function validate_name_is_unique($value, $id)
    {
        $this->form_validation->set_message('validate_name_is_unique', "This $value is already exist!");

        // fetchTable($select, $table, $where, $orderBy)
        $fetchItems = $this->model->fetchTable('name', $this->table, ['id !=' => $id], 'id asc');

        foreach( $fetchItems as $item )
        {
            if( strtolower($item->name) == strtolower($value) )
            {
                return FALSE;
            }
        }

        return TRUE;
    }
}