<?php defined('BASEPATH') or die('Restricted acess');

class Warrant_type extends WS_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->table = $this->tables['warrant_type'];
        $this->data['table'] = $this->table;

        $fields = $this->db->list_fields($this->table);
        $this->fields = array_values( array_diff( $fields, ['id', 'sorting', 'parent_id', 'progress'] ) );

        //load urdu editor script and file
		$this->template->add_header_js('assets/js/urdu-webpad.js');
		$this->template->add_js_script('initUrduEditor()');

        
    }
    public function index()
    {
        // updated DataTables
		$this->template->add_css('assets/plugins/uDataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/uDataTables/datatables.min.js');

        // notify
        $this->template->add_js('assets/plugins/notifyjs/js/notify.js');
        $this->template->add_js('assets/plugins/notifications/notify-metro.js');
        
        // select2
        $this->template->add_css('assets/plugins/select2/css/select2.min.css');
        $this->template->add_js('assets/plugins/select2/js/select2.min.js');
	
		$this->template->set_page_title('Warrants Type');
		$this->template->loadview('templates/default_admin','admin/warrant_type_index', $this->data);
        
    }

    public function fetchAllData()
    {
        $draw   = intval($this->input->post("draw"));
		$start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post('search');
        $where  = array( 'parent_id' => NULL );

        $query = $this->model->fetchAllData($this->table, $where, $this->sorting, $this->fields, $start, $length, $search);

        $fetchRecord = $query->result();

        foreach( $fetchRecord as $row)
        {
            if($row->parent_id === NULL)
            {
                // fetchRecord($select, $table, $where, $orderBy)
                $row->reports = $this->model->fetchTable('*', $this->table, array('parent_id' => $row->id), 'sorting asc, name asc');
            }
        }

        $data = [];
        $serialArray = array( 'i','ii','iii','iv','v','vi','vii', 'viii', 'ix', 'x', 'xi', 'xii', 'xiii', 'xiv', 'xv' );
        $i = 0;

        $tick = '<i class="md md-done text-success"></i>';

        foreach ($fetchRecord as $r)
        {
            $data[] = array(               
                $r->name . ' <br><span class="urdu">(' .$r->name_ur . ')</span> '.($r->progress == 1 ? $tick : ''),
                '-',
                $r->description,
                "<a href='#' id='editBtn' edit_id='".$r->id."' name='".$r->name."' name_ur='".$r->name_ur."' parent_id='".$r->parent_id."' description='".$r->description."'  progress='".$r->progress."' sorting='".$r->sorting."' data-toggle='modal' data-target='#addModal' data-rel='tooltip' data-container='body' title='Edit Warrent!' class='btn btn-sm btn-custom btn-primary waves-effect waves-light'><i class='fa fa-edit'></i></a>"
            );

            if(isset($r->reports))
            {
                foreach($r->reports as $rep)
                {
                    $data[] = array(                       
                        "<span class='text-lowercase pull-right'>".$serialArray[$i++]."</span>",
                        $rep->name . ' <span class="urdu">(' .$rep->name_ur . ')</span> ' .($rep->progress == 1 ? $tick : ''),
                        $rep->description,
                        "<a href='#' id='editBtn' edit_id='".$rep->id."' name='".$rep->name."' name_ur='".$rep->name_ur."' parent_id='".$rep->parent_id."' description='".$rep->description."' progress='".$rep->progress."' sorting='".$rep->sorting."' data-toggle='modal' data-target='#addModal' data-rel='tooltip' data-original-title='Edit Warrent Report!' class='btn btn-sm btn-custom btn-primary waves-effect waves-light'><i class='fa fa-edit'></i></a>"
                    );
                }
                $i = 0;
            }
            
        }

        $result = array(
            "draw" => $draw,
            "recordsTotal" => $this->model->countAll($this->table, $where),
            "recordsFiltered" => $this->model->countFiltered($this->table, $where, $this->sorting, $this->fields, $start, $length, $search),
            "data" => $data
        );
    
        echo json_encode($result);
        exit();

    }

    public function add()
    {
        $data = $this->input->post();

        $this->form_validation->set_rules('name', 'name', 'required|callback_validate_name_is_unique['.$data['id'].']');

        if( $this->form_validation->run() === TRUE )
        {
            if( $this->model->save($this->table, $data) > 0 )
            {
                $message['message_type'] = 'success';
                $message['message']      = 'This record has saved!';
                $message['id']           = $data['id'];
                // $message['name']         = $data['name'];
            }
            else
            {
                $message['message_type'] = 'error';
                $message['message']      = 'This record could not be saved!';
            }            
        }
        else
        {
            $message['message_type'] = 'error';
            // $message['message']   = 'Please filled required (*) fields!';
            $message['message']      = validation_errors(' ', ' ');
        }

        echo json_encode($message);
        die();
    }

    public function validate_name_is_unique($name, $id)
    {
        $this->form_validation->set_message('validate_name_is_unique', "This $name is already exist!");

        // fetchTable($select, $table, $where, $orderBy)
        $fetchItems = $this->model->fetchTable('name', $this->table, ['id !=' => $id, 'parent_id' => $this->input->post('parent_id')], 'id asc');

        foreach( $fetchItems as $item )
        {
            if( strtolower($item->name) == strtolower($name) )
            {
                return FALSE;
            }
        }

        return TRUE;
    }
}