<?php defined('BASEPATH') or die('Restricted acess');

class Staff extends WS_Controller {

    public function __construct()
    {
        parent::__construct();

        // load models
        $this->load->model('staff_model');
        $this->model = $this->staff_model;
        $this->table = $this->tables['staff'];
        $this->data['table'] = $this->table;

        $fields = $this->db->list_fields($this->table);
        $this->fields = array_values( array_diff( $fields, ['id', 'sorting'] ) );

        $this->sorting = "a.service_status_id asc, c.sorting asc, d.sorting asc";

        //load urdu editor script and file
		$this->template->add_header_js('assets/js/urdu-webpad.js');
		$this->template->add_js_script('initUrduEditor()');
    }
    public function index()
    {
        // updated DataTables
		$this->template->add_css('assets/plugins/uDataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/uDataTables/datatables.min.js');
        
        // datepicker
        $this->template->add_css('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');
        $this->template->add_js('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');
        // notify
        $this->template->add_js('assets/plugins/notifyjs/js/notify.js');
        $this->template->add_js('assets/plugins/notifications/notify-metro.js');
        // input-mask
        $this->template->add_js('assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js');
        // select2
        $this->template->add_css('assets/plugins/select2/css/select2.min.css');
        $this->template->add_js('assets/plugins/select2/js/select2.min.js');
        // load image in popup
		$this->template->add_css('assets/plugins/lightbox/ekko-lightbox.css');
		$this->template->add_js('assets/plugins/lightbox/ekko-lightbox.min.js');
	
		$this->template->set_title('Judicial Staff List');
		$this->template->loadview('templates/default_admin','admin/staff_index', $this->data);
    }

    public function fetchAllData()
    {
        $draw   = intval($this->input->post("draw"));
		$start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post('search');
        
        $desgn_id        = $this->input->post('desgn_id');
        $zone_id         = $this->input->post('zone_id');
        $bloodgroup_id   = $this->input->post('bloodgroup_id');

        if($desgn_id !== 'null')
        {
            $this->where['a.desgn_id'] = (int)$desgn_id;
        }
        if($zone_id !== 'null')
        {
            $this->where['a.zone_id'] = (int)$zone_id;
        }
        if($bloodgroup_id !== 'null')
        {
            $this->where['a.bloodgroup_id'] = (int)$bloodgroup_id;
        }

        $query = $this->model->fetchAllData($this->table, $this->where, $this->sorting, $this->fields, $start, $length, $search);

        $data = [];
        foreach ( $query->result() as $r )
        {
            $staff_id = $this->encrypt_decrypt($r->id, 'encrypt');

            // fetchRow($select, $table, $where, $orderBy)
            $staffLeave = $this->model->fetchRow('*, DATE_FORMAT(start_date,"%d-%m-%Y") as startDate, DATE_FORMAT(end_date,"%d-%m-%Y") as endDate', $this->tables['staff_leaves'], array('staff_id' => $r->id), 'end_date desc');

            $editLeavebutton = isset($staffLeave->start_date) ? "<a href='#' id='editLeaveBtn' staff_id='".$staff_id."' name='".$r->name."' leave_id='".$staffLeave->id."' leavetype_id='".$staffLeave->leavetype_id."' start_date='".$staffLeave->startDate."' end_date='".$staffLeave->endDate."' remarks='".$staffLeave->remarks."' cr_date='".$staffLeave->cr_date."' data-rel='tooltip' data-container='body' title='Edit Last Leave' class=''>".$staffLeave->endDate."</a>" : '-';
           
            $profile_photo_url = $r->profile_photo ? base_url("uploads/profile/$r->profile_photo") : ($r->gender == 'Male' ? base_url("uploads/male.jpg") : base_url("uploads/female.jpg"));

            $profile_photo = '<a class="image-popup" href="'.$profile_photo_url.'" data-toggle="lightbox" title="'.$r->name.'"><img class="img-rounded" src="'.$profile_photo_url.'" alt="profile photo" width="40" height=""></a>';

            $address = empty($r->postal_address) ? $r->permanent_address : $r->postal_address;

            $button = "<div class='input-group-btn'>";

            $button .= "<a href='#' id='editBtn' staff_id='".$staff_id."' name='".$r->name."' name_ur='".$r->name_ur."' father='".$r->father."' cnic_no='".$r->cnic_no."' date_of_birth='".$r->date_of_birth."' date_of_appointment='".$r->date_of_appointment."' appointed_as_id='".$r->appointed_as_id."' desgn_id='".$r->desgn_id."' payscale_id='".$r->payscale_id."' personnel_number='".$r->personnel_number."' domicile_id='".$r->domicile_id."' qualification='".$r->qualification."' gender_id='".$r->gender_id."' bloodgroup_id='".$r->bloodgroup_id."' religion_id='".$r->religion_id."' disability='".$r->disability."' contact_no='".$r->contact_no."' permanent_address='".$r->permanent_address."' postal_address='".$r->postal_address."' inquiries='".$r->inquiries."' remarks='".$r->remarks."' profile_photo='".$r->profile_photo."' official_bike_no='".$r->official_bike_no."' driving_license_type='".$r->driving_license_type."' zone_id='".$r->zone_id."' service_status_id='".$r->service_status_id."' sorting='".$r->sorting."' data-toggle='modal' data-target='#addModal' data-rel='tooltip' title='Edit Profile!' class='btn btn-sm btn-custom btn-primary waves-effect waves-light'><i class='fa fa-edit'></i></a>";

            $button .= "<a href='#' id='addLeaveBtn' staff_id='".$staff_id."' name='".$r->name."' father='".$r->father."' data-toggle='modal' data-target='#addLeaveModal' data-rel='tooltip' data-container='body' title='Add Leave' class='btn btn-sm btn-custom btn-primary waves-effect waves-light'><i class='fa fa-plus'></i></a>";            

            $button .= "<a href='#' id='printBtn' staff_id='".$staff_id."' name='".$r->name."' name_ur='".$r->name_ur."' father='".$r->father."' cnic_no='".$r->cnic_no."' date_of_birth='".$r->date_of_birth."' date_of_appointment='".$r->date_of_appointment."' appointed_as='".$r->appointed_as."' designation='".$r->designation."' payscale='".$r->payscale."' personnel_number='".$r->personnel_number."' domicile='".$r->domicile."' qualification='".$r->qualification."' gender='".$r->gender."' bloodgroup='".$r->bloodgroup."' religion='".$r->religion."' disability='".$r->disability."' contact_no='".$r->contact_no."' permanent_address='".$r->permanent_address."' postal_address='".$r->postal_address."' inquiries='".$r->inquiries."' remarks='".$r->remarks."' profile_photo='".$profile_photo_url."' official_bike_no='".$r->official_bike_no."' driving_license_type='".$r->driving_license_type."' zone='".$r->zone."' service_status='".$r->service_status."' sorting='".$r->sorting."' data-toggle='modal' data-target='#printModal' data-rel='tooltip' data-container='body' title='Print!' class='btn btn-sm btn-custom btn-primary waves-effect waves-light'><i class='fa fa-print'></i></a>";

            $button .= "</div>";

            $data[] = array(
                $profile_photo,
                $r->name,
                $r->father,
                $r->contact_no,
                $r->cnic_no,                
                $r->date_of_birth,
                $r->qualification,  
                $r->designation,
                $r->personnel_number,                        
                $r->date_of_appointment,
                $r->domicile,
                $r->gender,
                $r->bloodgroup,
                $r->disability,
                $r->religion,                           
                $address,
                $r->zone,
                $r->service_status,
                $editLeavebutton,
                $button
            );

        }

        $result = array(
            "draw" => $draw,
            "recordsTotal" => $this->model->countAll($this->table, []),
            "recordsFiltered" => $this->model->countFiltered($this->table, $this->where, $this->sorting, $this->fields, $start, $length, $search),
            "data" => $data
        );
    
        echo json_encode($result);
        exit();
    }

    public function add()
    {
        $data = $this->input->post();
        $data['id'] = $this->encrypt_decrypt($data['id'], 'decrypt');

        $this->form_validation->set_rules('cnic_no', 'CNIC No.', 'required|callback_validate_cnic_no_is_unique['.$data['id'].']');
        $this->form_validation->set_rules('personnel_number', 'Personnel No.', 'callback_validate_personnel_no_is_unique['.$data['id'].']');
        $this->form_validation->set_rules('contact_no', 'Contact No.', 'required|callback_validate_contact_no_is_unique['.$data['id'].']');

        if( $this->form_validation->run() === TRUE )
        {
            if ( !empty($_FILES['profile_photo']['name']) )
            {
                //set file upload
                $config['upload_path'] = UPLOADS_PATH."/profile/";
                $config['overwrite'] = TRUE;
                // $config['encrypt_name'] = TRUE;
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['max_size']      = '50';
                // $config['max_width']     = '200';
                // $config['max_height']    = '240';

                if($_FILES['profile_photo']['name'])
                {
                    // rename file name
                    $config['file_name'] = $data['cnic_no'];

                    //load the upload library
                    $this->load->library('upload', $config);

                    //if upload successful						
                    if ( ! $this->upload->do_upload('profile_photo'))
                    {
                        $message['message_type'] = 'error';
                        $message['message'] = $this->upload->display_errors("Photo size must be less than 50kb ");

                        echo json_encode($message);
                        die();
                    }
                    else
                    {
                        $uploadedProfilePhoto = $this->upload->data();
                        $data['profile_photo'] = $uploadedProfilePhoto['file_name'];
                    }
                }
            }

            
            if( $this->model->save($this->table, $data) > 0 )
            {
                $message['message_type'] = 'success';
                $message['message']      = 'This record has saved!';
                $message['id']           = $data['id'];
                $message['name']         = $data['name'];
            }
            else
            {
                $message['message_type'] = 'warning';
                $message['message']      = 'No changes have been made to the record.';
            }

            $zone_id = isset($data['zone_id']) ? $data['zone_id'] : 0;

            if( $data['id'] > 0 && $zone_id == 0 ||  $zone_id != $data['zone_id_old'] )
            {
                if($this->model->save_history($this->tables['zone_history'], $data) )
                {
                    $message['save_history'] = "Zone history saved successfully!";
                }   
            }
        }
        else
        {
            $message['message_type'] = 'error';
            // $message['message']   = 'Please filled required (*) fields!';
            $message['message']      = validation_errors(' ', ' ');
        }

        echo json_encode($message);
        die();
    }

    public function validate_cnic_no_is_unique($cnic_no, $id)
    {
        $this->form_validation->set_message('validate_cnic_no_is_unique', "This CNIC No. $cnic_no is already exist!");

        // fetchTable($select, $table, $where, $orderBy)
        $fetchItems = $this->model->fetchTable('cnic_no', $this->table, ['id !=' => $id ], 'id asc');

        foreach($fetchItems as $key => $value )
        {
            if($value->cnic_no == $cnic_no )
            {
                return FALSE;
            }
        }

        return TRUE;
    }

    public function validate_personnel_no_is_unique($personnel_number, $id)
    {
        $this->form_validation->set_message('validate_personnel_no_is_unique', "This Personal No. $personnel_number is already exist!");

        // fetchTable($select, $table, $where, $orderBy)
        $fetchItems = $this->model->fetchTable('personnel_number', $this->table, ['id !=' => $id], 'id asc');

        foreach($fetchItems as $key => $value )
        {
            if($value->personnel_number == $personnel_number && !empty($personnel_number))
            {
                return FALSE;
            }
        }

        return TRUE;
    }

    public function validate_contact_no_is_unique($contact_no, $id)
    {
        $this->form_validation->set_message('validate_contact_no_is_unique', "This Contact No. $contact_no is already exist!");

        // fetchTable($select, $table, $where, $orderBy)
        $fetchItems = $this->model->fetchTable('contact_no', $this->table, ['id !=' => $id], 'id asc');

        foreach($fetchItems as $key => $value )
        {
            if($value->contact_no == $contact_no )
            {
                return FALSE;
            }
        }

        return TRUE;
    }

    public function addleave()
    {
        $data = $this->input->post();
        $data['staff_id'] = $this->encrypt_decrypt($data['leave_staff_id'], 'decrypt');

        $parameters = $data['leave_id'] . '||' . $data['staff_id'];
        // set validation
        $this->form_validation->set_rules('start_date', 'start date', 'callback_validate_staff_leave_is_unique['.$parameters.']');

        if ($this->form_validation->run() == TRUE)
        {
            if( $this->model->save_leave($this->tables['staff_leaves'], $data) > 0 )
            {
                $message['message_type'] = 'success';
                $message['message']      = 'This record has saved!';
                $message['leave_id']     = $data['leave_id'];
            }
            else
            {
                $message['message_type'] = 'error';
                $message['message']      = 'This record could not be saved!';
            }
        }
        else
        {
            $message['message_type'] = 'error';
            $message['message']      = validation_errors(' ', ' ');
        }
        
        echo json_encode($message);
        die();
    }

    public function validate_staff_leave_is_unique($start_date, $parameters)
    {
        $this->form_validation->set_message('validate_staff_leave_is_unique', "This leave $start_date is already exist!");

        $paras = explode('||', $parameters);
        // $where = array( 'id !=' => $paras[0], 'staff_id' => $paras[1] );
        
        // fetchTable($select, $table, $where, $orderBy)
        $fetchItems = $this->model->fetchTable('start_date, end_date', $this->tables['staff_leaves'], ['id !=' => $paras[0], 'staff_id' => $paras[1]], 'id desc');

        foreach($fetchItems as $key => $value)
        {            
            if( $value->start_date == $this->dateFormat("Y-m-d",$start_date) || $value->end_date >= $this->dateFormat("Y-m-d",$start_date) )
            {
                return FALSE;
            }
        }

        return TRUE;
    }

    public function fetch_leave_record($staff_id)
    {
        $staff_id = $this->encrypt_decrypt($staff_id, 'decrypt');

        $result = $this->model->fetch_leave_record($this->tables['staff_leaves'], ['staff_id' => $staff_id]);

        echo json_encode($result);
        die();
    }

    public function print($id = NULL)
    {
        $staff_id = $this->encrypt_decrypt($id, 'decrypt');

        // var_dump($staff_id);
		// die();

        $this->data = $this->model->get_profile($this->table, $staff_id);
        $this->data->leaves = $this->model->fetch_leave_record($this->tables['staff_leaves'], ['staff_id' => $staff_id]);
        // $this->data['profile'] = $this->model->get_employee_profile($this->table, $staff_id);
        // $this->data['promotion'] = $this->model->fetchStaffPromotion('staff_promotion', $staff_id);
        // $this->data['postingArray'] = $this->model->fetchStaffPosting('staff_posting', $staff_id);

        // echo "<pre>";
        // var_dump($this->data);
        // die();

        $this->template->set_page_title('Staff Profile');
        $this->template->loadview('templates/default_admin','admin/staff_profile', $this->data);
    }

    /* public function profile($id=NULL)
    {
        $this->data = $this->model->get_profile($id);

        // echo "<pre>";
        // var_dump($this->data);
        // die();

        if(!$id || !$this->data )
        {
            $this->session->set_flashdata('message','Record not found!');
            $this->session->set_flashdata('message_type','danger');
            redirect( base_url('admin/satff') );
        }

        // notify
        $this->template->add_js('assets/plugins/notifyjs/js/notify.js');
        $this->template->add_js('assets/plugins/notifications/notify-metro.js');
        // input-mask
        $this->template->add_js('assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js');
        // datepicker
        $this->template->add_css('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');
        $this->template->add_js('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');

		$this->template->set_title('Staff Profile');
		$this->template->loadview('templates/default_admin','admin/staff_profile',$this->data);
    } */

    /* public function add_academic_record()
    {
        $data = $this->input->post();

        // set validation
        $this->form_validation->set_rules('degree', 'degree', 'required|callback_validate_degree_is_unique['.$data['id'].']');

        if ($this->form_validation->run() == TRUE)
        {
            $result = $this->employees_model->save_academic_record($data);

            echo json_encode($result);
            die();
        }
        else
        {
            echo json_encode(2);
            die;
        }
    } */
    /* public function validate_degree_is_unique($degree, $id)
    {
        $emp_id = $this->input->post('emp_id');

        $degrees = $this->employees_model->get_academic_record($id, $emp_id);

        foreach($degrees as $key => $val)
        {
            if( $val->degree == $degree )
            {
                return FALSE;
            }
        }
        return TRUE;
    } */
    /* public function get_academic_record()
    {

        $emp_id = $this->input->post('emp_id');

        $result = $this->employees_model->get_academic_record(0, $emp_id);

        echo json_encode($result);
        die();
    } */
    /* public function get_courts()
    {
        $result = $this->dropdown_model->get_courts($status = NULL);

        echo json_encode($result);
        die();
    } */
    /* public function add_posting_record()
    {
        $data = $this->input->post();

        // set validation
        $this->form_validation->set_rules('emp_id', 'posting', 'required|callback_validate_emp_posting_is_unique['.$data['id'].']');

        if ($this->form_validation->run() == TRUE)
        {
            $result = $this->employees_model->save_posting_record($data);

            echo json_encode($result);
            die();
        }
        else
        {
            echo json_encode(2);
            die;
        }
    } */
    /* public function validate_emp_posting_is_unique($emp_id, $id)
    {
        $court_id       = $this->input->post('court_id');
        $posting_date   = $this->changeDateFormatDB($this->input->post('posting_date'));
        $transfer_date  = ($this->input->post('transfer_date') ? $this->changeDateFormatDB($this->input->post('transfer_date')) : '');
        
        $postings = $this->employees_model->get_posting_record_by_id($id, $emp_id);

        foreach($postings as $key => $val)
        {
            if(empty($val->transfer_date))
            {
                return FALSE;
            }
      
        }
        return TRUE;
    } */
    /* public function get_posting_record()
    {

        $emp_id = $this->input->post('emp_id');

        $result = $this->employees_model->get_posting_record($emp_id);

        echo json_encode($result);
        die();
    } */
    
    
}