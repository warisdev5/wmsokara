<?php defined('BASEPATH') or die('Restricted acess');

class Reports extends WS_Controller {

    public function __construct()
    {
        parent::__construct();
        
        // load models
        $this->load->model('reports_model');
        $this->model = $this->reports_model;
        $this->table = $this->tables['staff'];

        $fields = $this->db->list_fields($this->table);
        $this->fields = array_values( array_diff( $fields, ['id', 'sorting'] ) );

        $this->sorting = "a.id asc";
    }

    public function index()
    {
       // updated DataTables
		$this->template->add_css('assets/plugins/uDataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/uDataTables/datatables.min.js');
        
        // datepicker
        $this->template->add_css('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');
        $this->template->add_js('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');
        // notify
        $this->template->add_js('assets/plugins/notifyjs/js/notify.js');
        $this->template->add_js('assets/plugins/notifications/notify-metro.js');
        // input-mask
        $this->template->add_js('assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js');
        // select2
        $this->template->add_css('assets/plugins/select2/css/select2.min.css');
        $this->template->add_js('assets/plugins/select2/js/select2.min.js');

        $this->template->set_title('Bailiff Report');
		$this->template->loadview('templates/default_admin','admin/reports_index', $this->data);
    }

    public function fetchAllData()
    {
        $draw   = intval($this->input->post("draw"));
		$start  = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $search = $this->input->post('search');

        $zone_id = $this->input->post('zone_id');
        $staff_id = $this->input->post('staff_id');

        $startDate = $this->dateFormat("Y-m-d 00:00:00", $this->input->post('startDate'));
        $endDate = $this->dateFormat("Y-m-d 23:59:59", $this->input->post('endDate'));

        if($zone_id !== 'null')
        {
            // $this->where['a.zone_id'] = (int)$zone_id;
            $this->where['a.zone_id'] = $zone_id;
        }
        if($staff_id !== 'null')
        {
            // $this->where['a.id'] = (int)$staff_id;
            $this->where['a.id'] = $staff_id;
        }
        
        $query = $this->model->fetchAllData($this->table, $this->where, $this->sorting, $this->fields, $start, $length, $search);
        
        $data = [];

        foreach ($query->result() as $r)
        {            
            // countAll($table, $where)
            $total_warrants = $this->model->countAll($this->tables['warrants'], ['staff_id' => $r->id, 'cr_date >=' => $startDate, 'cr_date <=' => $endDate] );

            $where = ['a.staff_id' => $r->id, 'a.cr_date >=' => $startDate, 'a.cr_date <=' => $endDate];

            // countWarrants($where, $where_in_array)
            $executed = $this->model->countWarrants( $where, ['Arrested', 'Executed', 'Recovery', 'Partially Executed'] );            
            
            $progress = ( $total_warrants ? $executed / $total_warrants*100 : 0);            
            
            $not_executed = $this->model->countWarrants( $where, ['Not Arrested', 'Not Executed', 'Not Recovery'] );            
            
            $abscondence = $this->model->countWarrants( $where, ['Abscondence'] );            
            
            $accommodationChanged = $this->model->countWarrants( $where, ['Accommodation changed'] );            
            
            $incompleteAddress = $this->model->countWarrants( $where, ['Incorrect Address', 'Incomplete Address'] );            
            
            $died = $this->model->countWarrants( $where, ['Died'] );            
                        
            $others = $this->model->countWarrants( $where, ['Issued To SHO', 'Goods to be attached not found.', 'Issued to Tehsildar', 'Attachement Not Executed', 'Robkar Received To Revenue Officer'] );            
            
            $cancels = $this->model->countWarrants( $where, ['Warrant Cancel', 'Robkar Cancel'] );

            $pending = $this->model->countAll($this->tables['warrants'], ['staff_id' => $r->id, 'cr_date >=' => $startDate, 'cr_date <=' => $endDate, 'warrant_report_id' => NULL] );            

            // $total = $executed + $not_executed + $abscondence + $accommodationChanged + $incompleteAddress + $died + $others + $cancels + $pending;

            $data[] = array(                
                $r->bailiff_name,
                sprintf("%.2f", $progress),
                $total_warrants,
                $executed,
                $not_executed,
                $abscondence,
                $accommodationChanged,
                $incompleteAddress,
                $died,
                $others,                
                $cancels,                
                $pending
            );
        }

        $result = array(
            "draw" => $draw,
            "recordsTotal" => $this->model->countAll($this->table, []),
            "recordsFiltered" => $this->model->countFiltered($this->table, $this->where, $this->sorting, $this->fields, $start, $length, $search),
            "data" => $data
        );
    
        echo json_encode($result);
        exit();
    }
}