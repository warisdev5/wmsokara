<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends WS_Controller {

    public function __construct()
    {
        parent::__construct();

        // load models
        $this->load->model('dashboard_model');
        $this->model = $this->dashboard_model;
        $this->table = $this->tables['warrants'];

        // $arr = array( 'name' => 'tahira');
        // $arr = array( 'a', 'b', 'c');
        // $arr = array();
        // $newArr = !empty($arr) ?? 'good work';
        // var_dump($newArr);
        // die();
    }

    public function index()
    {
        // jQuery
        $this->template->add_js('assets/plugins/waypoints/lib/jquery.waypoints.js');
        $this->template->add_js('assets/plugins/counterup/jquery.counterup.min.js');
        // Morris Chart
        $this->template->add_css('assets/plugins/morris/morris.css');
		$this->template->add_js('assets/plugins/morris/morris.min.js');
		$this->template->add_js('assets/plugins/raphael/raphael-min.js');
		// $this->template->add_js('assets/pages/morris.init.js');

        // notify
		// $this->template->add_js('assets/plugins/notifyjs/js/notify.js');
		// $this->template->add_js('assets/plugins/notifications/notify-metro.js');

        $this->data['total_warrants'] = $this->model->countAll($this->table, array() );

        // countWarrants($where, $where_in_array)
        $this->data['total_success_warrants'] = $this->model->countWarrants(array(), array('Arrested', 'Executed', 'Recovery', 'Partially Executed'));
        $this->data['total_pending_warrants'] = $this->model->countAll($this->table, array('warrant_report_id' => NULL));
        // $this->data['total_success_warrants'] = $this->model->countWarrants(array( 'b.progress' => 1 ), array());

        // $this->data['total_warrants'] = $this->dashboard_model->total_warrants();
        // $this->data['total_success_warrants'] = $this->dashboard_model->total_success_warrants();
        // $this->data['total_pending_warrants'] = $this->dashboard_model->total_pending_warrants();

        // get & set chart data
        $chart_data = $this->model->chart_data();

        // echo "<pre>";
        // var_dump($chart_data); die();
        $chartData = '';
        foreach( $chart_data as $key => $val )
        {
            $chartData .= "{ month: '".$key."', total: ".$val['total'].", success: ".$val['success'].", others: ".$val['others'].", pending: ".$val['pending']." },";            
        }
        $this->data['chart_data'] = substr($chartData, 0, -1);

        $this->template->set_title('Welcome to Dashboard');
        $this->template->loadview('templates/default_admin','admin/dashboard_index', $this->data);
    }
    

}