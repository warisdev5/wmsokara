<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Login extends CI_Controller
{
    public $data = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');

		if($this->ion_auth->logged_in())
		return redirect('admin', 'refresh');
    }
    /**
	 * Log the user in
	 */
    public function index()
    {
		$this->data['title'] = $this->lang->line('login_heading');

		// validate form input
		$this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
		$this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');

		if ($this->form_validation->run() === TRUE)
		{
			
			// check to see if the user is logging in
			// check for "remember me"
			$remember = (bool)$this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				$this->session->set_flashdata('message_type','success');
				redirect('admin');
			}
			else
			{
				// if the login was un-successful
				// redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				$this->session->set_flashdata('message_type','danger');

				// var_dump($this->session->flashdata('message_type'));
				// var_dump($this->session->flashdata('message'));
				// die();

				redirect('login'); // use redirects instead of loading views for compatibility with MY_Controller libraries
			}
			
		}
		else
		{
			// the user is not logging in so display the login page
			// set the flash data error message if there is one
			$this->session->set_flashdata('message', (validation_errors()) ? validation_errors(' ', ' ') : $this->session->flashdata('message') );
			$this->session->set_flashdata('message_type', (validation_errors() ? 'danger' : $this->session->flashdata('message_type')));
			
			// var_dump($this->session->flashdata('message_type'));
			// var_dump($this->session->flashdata('message'));
			
			$this->data['identity'] = [
				'name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'class' => 'form-control',
				'placeholder' => 'Username',
				'required' => 'required',
				'data-parsley-trigger' => 'change',
				'data-parsley-error-message' => 'Username is required',
				'autofocus' => '',
				'value' => $this->form_validation->set_value('identity'),
			];

			$this->data['password'] = [
				'name' => 'password',
				'id' => 'password',
				'type' => 'password',
				'class' => 'form-control',
				'placeholder' => 'Password',
				'required' => 'required',
				'data-parsley-trigger' => 'change',
				'data-parsley-error-message' => 'Password is required',
			];

			// $this->_render_page('auth' . DIRECTORY_SEPARATOR . 'login', $this->data);
			$this->template->set_page_title('Login');
			$this->template->loadview('templates/default_login','admin/users/login', $this->data);
		}
    }
    
}