<?php  

function makeResponse($code = 0, $msg = 'success', $error = '', $record = '') 
{
	$obj = [
		'code'   => $code,
		'status' => $msg,
		'record' => $record,
		'errors' => $error
	];
	return $obj;
}

function makeErrorResponse($errors = []) 
{
	$err = [];
	foreach ($errors as $error) 
	{
		if (is_array($error)) 
		{
			foreach ($error as $er) 
			{
				$err[] = $er;
			}
		}
	}
	return $this->makeResponse(0, 'failed', $err, []);
}


?>