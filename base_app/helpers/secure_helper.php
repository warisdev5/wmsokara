<?php
// Decoder by Ikram Khizer  Sept 05, 2019.

function token_encoder($data)
{
    $data = base64_encode(json_encode($data));
    return $data;
}

function token_decoder($data)
{
    $data = base64_decode($data);
    $data = json_decode($data);
    return $data;
}

?>