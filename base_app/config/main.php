<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Name: DB Tables
 * Discription: All Database Tables
 */

 /*
| -------------------------------------------------------------------------
| Tables.
| -------------------------------------------------------------------------
| Database table names.
*/

$config['tables']['users']           = 'users';
$config['tables']['groups']          = 'groups';
$config['tables']['users_groups']    = 'users_groups';
$config['tables']['login_attempts']  = 'login_attempts';

$config['tables']['opbloodgroup']    = 'opbloodgroup';
$config['tables']['opgender']        = 'opgender';
$config['tables']['opmaritalstatus'] = 'opmaritalstatus';
$config['tables']['oppayscale']      = 'oppayscale';
$config['tables']['opreligion']      = 'opreligion';
$config['tables']['opservicestatus'] = 'opservicestatus';
$config['tables']['opleavetypes']    = 'opleavetypes';   

$config['tables']['courts']          = 'courts';
$config['tables']['designations']    = 'designations';

$config['tables']['staff']           = 'staffdata';
$config['tables']['staff_leaves']    = 'staff_leaves';

$config['tables']['cities']          = 'districts';
$config['tables']['zones']           = 'zones';
$config['tables']['zone_history']    = 'zone_history';
$config['tables']['warrant_type']    = 'warrant_type';
$config['tables']['warrants']        = 'warrants';
$config['tables']['warrant_history'] = 'warrant_history';

/* $config['default']['religionOptions']   = array(
                                            'Islam' => 'Islam',
                                            'Christianity' => 'Christianity',
                                            'Sikhism' => 'Sikhism',
                                            'Hinduism' => 'Hinduism',
                                            'Other' => 'Other'
                                        ); */


/* $config['default']['bloodGroupOptions'] = array(
                                            'A+' => 'A+', 
                                            'B+' => 'B+',
                                            'AB+' => 'AB+',
                                            'O+' => 'O+',
                                            'A-' => 'A-',
                                            'B-' => 'B-',
                                            'AB-' => 'AB-',
                                            'O-' => 'O-'
                                        ); */

/* $config['default']['genderOptions']     = array( 
                                            'Male' => 'Male',
                                            'Female' => 'Female',
                                            'Transgender' => 'Transgender'
                                        ); */

/* $config['default']['payScaleOptions']   = array(
                                            'BPS-1' => 'BPS-1',
                                            'BPS-2' => 'BPS-2',
                                            'BPS-3' => 'BPS-3',
                                            'BPS-4' => 'BPS-4',
                                            'BPS-5' => 'BPS-5',
                                            'BPS-6' => 'BPS-6',
                                            'BPS-7' => 'BPS-7',
                                            'BPS-8' => 'BPS-8',
                                            'BPS-9' => 'BPS-9',
                                            'BPS-10' => 'BPS-10',
                                            'BPS-11' => 'BPS-11',
                                            'BPS-12' => 'BPS-12',
                                            'BPS-13' => 'BPS-13',
                                            'BPS-14' => 'BPS-14',
                                            'BPS-15' => 'BPS-15',
                                            'BPS-16' => 'BPS-16',
                                            'BPS-17' => 'BPS-17'
                                        ); */

/* $config['default']['serviceStatusOptions']  = array(
                                                'In-Service' => 'In-Service',
                                                'Retired' => 'Retired',
                                                'Dismissed' => 'Dismissed',
                                                'Transferred' => 'Transferred'
                                            ); */

/* $config['default']['maritalStatusOptions']  = array(
                                                'Single' => 'Single',
                                                'Married' => 'Married'
                                            ); */

$config['default']['maritalStatusOptions']['Single'] = 'Single';
$config['default']['maritalStatusOptions']['Married'] = 'Married';


