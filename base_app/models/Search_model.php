<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Search_model extends WS_Model
{
    public function _fetchAllData($table, $where, $sorting, $fields, $start = NULL, $length = NULL, $search = NULL)
    {
        // $date_of_hearing = $this->input->post('date_of_hearing');
        // $court_id        = $this->input->post('court_id');
        $case_title      = $this->input->post('case_title');
        $defendant      = $this->input->post('defendant');

        $this->db->select('a.id, a.case_title, a.defendant, a.plaintiff, a.address, DATE_FORMAT(a.date_of_hearing,"%d-%m-%Y") as date_of_hearing, w.name as warrant, CONCAT(c.name," ",cd.short_name,"<br>",cc.name) as courtname, CONCAT(s.name,"<br>",s.contact_no) as bailiff');
        $this->db->from($table.' a');
        $this->db->join($this->tables['warrant_type'].' w', 'a.warrant_type_id = w.id');
        $this->db->join($this->tables['courts'].' c', 'a.court_id = c.id');
        $this->db->join($this->tables['designations'].' cd', 'c.desgn_id = cd.id');
        $this->db->join($this->tables['cities'].' cc', 'c.teh_id = cc.id');
        $this->db->join($this->tables['staff'].' s', 'a.staff_id = s.id');
        $this->db->join($this->tables['designations'].' sd', 's.desgn_id = sd.id');

        $this->db->where($where);

        /* if($court_id !== 'null')
        {
            // $this->where['a.court_id'] = (int)$court_id;
            $this->db->where( 'a.court_id', (int)$court_id );
        } */

        /* if(!empty($date_of_hearing))
        {
            // $this->where['a.date_of_hearing'] = $this->dateFormat('Y-m-d', $date_of_hearing);
            $this->db->where( 'a.date_of_hearing', $this->dateFormat('Y-m-d', $date_of_hearing) );
        } */

        /* if(empty($case_title) && $court_id == 'null' && empty($date_of_hearing))
        {
            // $this->where['a.case_title'] = $case_title;
            $this->db->where('a.case_title', $case_title);
        } */

        if(!empty($case_title) || !empty($defendant))
        {            
            $this->db->like('a.case_title', $case_title);            
            $this->db->like('a.defendant', $defendant);
        }


        /* if( $court_id !== 'null' )
        {
            $this->db->where('a.court_id', $court_id);
        } */

        // var_dump($where);die();

        // $this->db->where($where);

        // var_dump(array('a.court_id' => 238, 'a.date_of_hearing' => '2023-10-09'));
        // die();
        
        // $this->db->where(['a.court_id' => 238, 'a.date_of_hearing' => '2023-10-05']);
        
       
        
        // $this->db->where('a.date_of_hearing', $this->dateFormat('Y-m-d', $date_of_hearing) );

        /* if( $court_id !== 'null' )
        {
            $this->db->where('a.court_id', $court_id);
        }

        // if( $this->input->post('date_of_hearing') )
        if(!empty($date_of_hearing))
        {
            // var_dump($this->dateFormat('Y-m-d', $date_of_hearing));
            // die();
            // echo "date is not empty";
            // echo "$this->dateFormat('Y-m-d', $date_of_hearing)"; die();
            $this->db->where('a.date_of_hearing', $this->dateFormat('Y-m-d', $date_of_hearing) );
        }
        else
        {
            // echo "date is empty"; die();
        } */

        // $this->dateFormat("Y-m-d", $data['date_of_hearing'])
        /* if( ($court_id == 'null') && empty($date_of_hearing) && empty($case_title) ) {
            // echo "all fields empty!"; die();
            $this->db->where('a.case_title', $case_title);
        }
        else
        {
            // echo "all fields not empty!"; die();
            $this->db->like('a.case_title', $case_title);
            $this->db->or_like('a.defendant', $case_title);
            $this->db->or_like('a.address', $case_title);
        } */

        if(isset($search))
		{
			if($search['value'] != '')
			{
                $this->db->like('a.case_title', $search['value']);
                
			}
        }

        if(isset($_POST["order"]))
        {
            $this->db->order_by($_POST['order']['0']['column'], $_POST['order']['0']['dir']);
        }
        else
        {
            $this->db->order_by($sorting);
        }
        
    }

}