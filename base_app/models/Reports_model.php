<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_model extends WS_Model {

    public function get_bailiff_report($start = NULL, $length = NULL, $search = NULL)
    {
        $this->db->select('s.id, s.name as bailiff_name');

        $this->db->from($this->tables['staff'].' s');
        $this->db->join($this->tables['designations'].' d', 's.desgn_id = d.id');
        $this->db->join($this->tables['warrants'].' w', 's.id = w.staff_id', 'left');

        $this->db->where('d.name', 'Bailiff');
        $this->db->where('s.zone_id !=', 10);

        if( !empty($zone_id) )
        {
            $this->db->where('w.zone_id', $zone_id);
        }

        if( isset($search) && $search['value'] != '')
        {
            $this->db->like('s.name', $search['value']);
        }

        $this->db->order_by('s.desgn_id asc, s.zone_id asc, s.name asc');

        if(isset($start) && isset($length))
        {
            if($length != '-1')
            {
                $this->db->limit($length, $start);
            }
        }

        $query = $this->db->get();
		return $query;
    }

    /* public function fetchAllData($table, $where, $sorting, $fields, $start, $length, $search)
	{
		$this->_fetchAllData($table, $where, $sorting, $fields, $start, $length, $search);
		if( isset($start) && $length != '-1' )
		{
			$this->db->limit($length, $start);
		}

		$query = $this->db->get();
		return $query;  
	} */

	// **** fetchAllData for table
	public function _fetchAllData($table, $where, $sorting, $fields, $start = NULL, $length = NULL, $search = NULL)
	{        
        // $zone_id = $this->input->post('zone_id');

        $this->db->select('a.id, a.name as bailiff_name');

        $this->db->from($table.' a'); // staff table
        $this->db->join($this->tables['designations'].' b', 'a.desgn_id = b.id');

        /* if( $zone_id == 'null' )
        {
            $this->db->where(array('b.name' => 'Bailiff', 'a.zone_id !=' => 10));
        }
        else
        {
            $this->db->where(array('b.name' => 'Bailiff', 'a.zone_id' => $zone_id));
        } */

        if(isset($search['value']) && $search['value'] != '')
        {
            $this->db->like('a.name', $search['value']);
            // $this->db->or_like('b.name', $search['value']);
        }
        else
        {
            $this->db->where($where);
        }

        $this->db->group_by('a.id');

        if(isset($_POST["order"]))
        {
            $this->db->order_by($_POST['order']['0']['column'], $_POST['order']['0']['dir']);
        }
        else
        {
            $this->db->order_by($sorting);
        }			
	}

}