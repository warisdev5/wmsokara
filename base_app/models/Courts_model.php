<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Courts_model extends WS_Model {

    public function save($table, $data)
	{
		// preg_replace('!\s+!', ' ', trim($data['name']))
		//set value by name
		$this->db->set('name', preg_replace('!\s+!', ' ', trim($data['name'])));
		$this->db->set('name_ur', $data['name_ur']);
		$this->db->set('desgn_id', $data['desgn_id']);
		$this->db->set('teh_id', $data['teh_id']);
		$this->db->set('status', ( isset($data['status']) ? $data['status'] : 0 ));
		$this->db->set('sorting', $data['sorting']);
		
		if ($data['id'] > 0 )
		{
			$this->db->set('md_user_id', $this->user_id);
			
			$this->db->where('id',$data['id']);
			$this->db->update($table);
		}
		else 
		{
			$this->db->set('cr_user_id', $this->user_id);
			
			$this->db->insert($table);
		}

		return $this->db->affected_rows();
	}

	// **** fetchAllData for table
	public function _fetchAllData($table, $where, $sorting, $fields, $start = NULL, $length = NULL, $search = NULL)
	{
		$this->db->select('a.*, CONCAT(a.name," ",d.short_name," ",c.name) as judge_name');

		$this->db->from($table.' a');
        $this->db->join($this->tables['designations'].' d', 'a.desgn_id = d.id');
        $this->db->join($this->tables['cities'].' c', 'a.teh_id = c.id');

		if( isset($search['value']) && $search['value'] != '' ) {
				
			$this->db->like('a.name', $search['value']);
			$this->db->or_like('a.name_ur', $search['value']);
			$this->db->or_like('d.name', $search['value']);
			$this->db->or_like('c.name', $search['value']);
			
		} else {
			$this->db->where( $where );
		}

		if(isset($_POST["order"]))
		{
			$this->db->order_by($_POST['order']['0']['column'], $_POST['order']['0']['dir']);
		}
		else
		{
			$this->db->order_by( $sorting );
		}			
	}
}