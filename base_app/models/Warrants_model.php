<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Warrants_model extends WS_Model
{
    // ** fetchTable($select, $table, $where, $orderBy)
    public function fetchTable($select, $table, $where, $orderBy)
	{
		$this->db->select($select);
		$this->db->from($table);
		$this->db->where($where);
		$this->db->order_by($orderBy);
		$query = $this->db->get();

        $result = array();
        foreach($query->result() as $r)
        {
            $result[]= $r->id;
        }

		return $result;
	}

    public function save($table, $data)
    {
        // preg_replace('!\s+!', ' ', trim($data['case_title']))
        
        //set value by name
        $this->db->set('warrant_type_id', $data['warrant_type_id']);
        $this->db->set('court_id', $data['court_id']);
        $this->db->set('date_of_hearing', ($data['date_of_hearing'] ? $this->dateFormat("Y-m-d", $data['date_of_hearing']) : NULL) );
        $this->db->set('case_title', preg_replace('!\s+!', ' ', trim($data['case_title'])));

        $this->db->set('plaintiff', preg_replace('!\s+!', ' ', trim($data['plaintiff'])));
        $this->db->set('defendant', preg_replace('!\s+!', ' ', trim($data['defendant'])));
        $this->db->set('address', preg_replace('!\s+!', ' ', trim($data['address'])));
        $this->db->set('staff_id', ($data['staff_id'] ? $data['staff_id'] : NULL) );
        $this->db->set('dist_id', $data['dist_id']);

        $this->db->set('remarks', $data['remarks']);
        
        if( $data['id'] > 0 )
        {
            $this->db->set('md_user_id', $this->user_id);
            
            $this->db->where('id', $data['id']);
            $this->db->update($table);
        }
        else
        {
            $this->db->set('cr_user_id', $this->user_id);
			// $this->db->set('md_user_id', $this->user_id);
            
            $this->db->insert($table);
        }

        $status = $this->db->affected_rows();        
		return $status;
    }

    public function save_report($table, $data)
    {
        //set value by name
        $this->db->set('warrant_report_id', $data['warrant_report_id']);
        // $this->db->set('report_date', $this->dateFormat("Y-m-d", $data['report_date']) );
        $this->db->set('report_date', date('Y-m-d') );
        $this->db->set('remarks', $data['report_remarks']);
        $this->db->set('md_user_id', $this->user_id);
        
        $this->db->where('id', $data['report_id']);
        $this->db->update($table);

        $status = $this->db->affected_rows();
		return $status;

    }

    public function save_history($table, $data)
    {
        $this->db->set('warrant_id', $data['id']);
        $this->db->set('zone_id', $data['zone_id']);
        $this->db->set('staff_id', $data['staff_id']);
        $this->db->set('cr_user_id', $this->user_id);

        $this->db->insert($table);

        return $this->db->affected_rows();
    }
    public function fetch_warrant($id)
    {
        $this->db->select('wt.name as warrant_name, CONCAT(courts.name," ",d.name," ",c.name) as court, a.case_title, a.plaintiff, a.defendant, a.address, a.remarks, DATE_FORMAT(a.date_of_hearing,"%d-%m-%Y") as date_of_hearing, s.name as bailiff, s.contact_no as bailiff_mobile, wtt.name as warrant_report, DATE_FORMAT(a.report_date,"%d-%m-%Y") as report_date, z.name as zone, cc.name as district, u.username as cr_user, uu.username as md_user, DATE_FORMAT(a.cr_date,"%d-%m-%Y %H:%i:%s") as cr_date, DATE_FORMAT(a.md_date,"%d-%m-%Y %H:%i:%s") as md_date,');

        $this->db->from($this->tables['warrants'].' a');
        $this->db->join($this->tables['warrant_type'].' wt', 'a.warrant_type_id = wt.id');
        $this->db->join($this->tables['warrant_type'].' wtt', 'a.warrant_report_id = wtt.id', 'left');
        $this->db->join($this->tables['staff'].' s', 'a.staff_id = s.id', 'left');
        $this->db->join($this->tables['zones'].' z', 's.zone_id = z.id', 'left');
        $this->db->join($this->tables['courts'].' courts', 'a.court_id = courts.id');
        $this->db->join($this->tables['designations'].' d', 'courts.desgn_id = d.id');
        $this->db->join($this->tables['cities'].' c', 'courts.teh_id = c.id');
        $this->db->join($this->tables['cities'].' cc', 'a.dist_id = cc.id');
        $this->db->join($this->tables['users'].' u', 'a.cr_user_id = u.id');
        $this->db->join($this->tables['users'].' uu', 'a.md_user_id = uu.id', 'left');
        
        $this->db->where('a.id', $id);
		$query = $this->db->get();
		$result = $query->row();
		return $result;

    }
    public function fetchUpdateHistory($id)
    {
        $this->db->select('wt.name as warrant, s.name as bailiff, z.name as zone_area, u.username, DATE_FORMAT(a.cr_date, "%d-%m-%Y %h:%i:%s") as cr_date');
        // $this->db->select('a.*');

        $this->db->from($this->tables['warrant_history'].' a');
        $this->db->join($this->tables['warrants'].' w', 'a.warrant_id = w.id');
        $this->db->join($this->tables['warrant_type'].' wt', 'w.warrant_type_id = wt.id');
        $this->db->join($this->tables['staff'].' s', 'a.staff_id = s.id');
        $this->db->join($this->tables['zones'].' z', 's.zone_id = z.id');
        $this->db->join($this->tables['users'].' u', 'a.cr_user_id = u.id');

        $this->db->where('a.warrant_id', $id);
        $this->db->order_by('a.id asc');

        $query = $this->db->get();
    	$result = $query->result();
		return $result;
    }

    // **** fetchAllData for table
	public function _fetchAllData($table, $where, $sorting, $fields, $start = NULL, $length = NULL, $search = NULL)
	{
        $case_title      = $this->input->post('case_title');
        $defendant      = $this->input->post('defendant');

        $this->db->select('a.*, DATE_FORMAT(a.date_of_hearing,"%d-%m-%Y") as dateOfHearing, DATE_FORMAT(a.report_date,"%d-%m-%Y") as reportDate, DATE_FORMAT(a.cr_date,"%d-%m-%Y") as created_date, wt.name as warrant_name,  wt.name_ur as warrant_name_urdu, CONCAT(courts.name,"<br>",d.short_name," ",c.name) as court, CONCAT(s.name,"<br>",s.contact_no) as baillif, wtt.name as warrant_report, z.id as zone_id, z.name as zone, cc.name as district');

        $this->db->from($this->tables['warrants'].' a');
        $this->db->join($this->tables['warrant_type'].' wt', 'a.warrant_type_id = wt.id');
        $this->db->join($this->tables['warrant_type'].' wtt', 'a.warrant_report_id = wtt.id', 'left');
        $this->db->join($this->tables['staff'].' s', 'a.staff_id = s.id', 'left');
        $this->db->join($this->tables['zones'].' z', 's.zone_id = z.id', 'left');
        $this->db->join($this->tables['courts'].' courts', 'a.court_id = courts.id');
        $this->db->join($this->tables['designations'].' d', 'courts.desgn_id = d.id');
        $this->db->join($this->tables['cities'].' c', 'courts.teh_id = c.id');
        $this->db->join($this->tables['cities'].' cc', 'a.dist_id = cc.id');

        // $this->db->where($where);

        if(!empty($case_title) || !empty($defendant))
        {            
            $this->db->like('a.case_title', $case_title);            
            $this->db->like('a.defendant', $defendant);            
        }

        if(isset($search['value']) && $search['value'] != '')
		{
            $this->db->like('a.id', $search['value']);
            $this->db->or_like('a.case_title', $search['value']);
            $this->db->or_like('courts.name', $search['value']);
            $this->db->or_like('a.address', $search['value']);
            $this->db->or_like('cc.name', $search['value']);
            $this->db->or_like('s.name', $search['value']);
            $this->db->or_like('s.contact_no', $search['value']);
            $this->db->or_like('a.remarks', $search['value']);
            // $this->db->or_like('z.zone_name', $search['value']);
            $this->db->or_like('a.plaintiff', $search['value']);
            $this->db->or_like('a.defendant', $search['value']);
            $this->db->or_like('wt.name', $search['value']);

            $this->db->or_like('DATE_FORMAT(a.date_of_hearing,"%d-%m-%Y")',$search['value']);
            // $this->db->or_like('DATE_FORMAT(a.cr_date,"%d-%m-%Y")',$search['value']);
            // $this->db->or_like('DATE_FORMAT(a.report_date,"%d-%m-%Y")',$search['value']);    
        }
        else
        {            
            $this->db->where( $where );
        }

        if(isset($_POST["order"]))
        {
            $this->db->order_by($_POST['order']['0']['column'], $_POST['order']['0']['dir']);
        }
        else
        {
            $this->db->order_by('a.id desc');
        }			
	}
}