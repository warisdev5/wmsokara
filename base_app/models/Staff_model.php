<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Staff_model extends WS_Model {

    public function save($table, $data)
	{
		// preg_replace('!\s+!', ' ', trim($data['name']))
		//set value by name
		$this->db->set('name', preg_replace('!\s+!', ' ', trim($data['name'])));
		$this->db->set('name_ur', $data['name_ur']);
		$this->db->set('father', $data['father']);
		$this->db->set('cnic_no', $data['cnic_no']);
		$this->db->set('date_of_birth', ($data['date_of_birth'] ? $this->dateFormat("Y-m-d",$data['date_of_birth']) : NULL ));
		$this->db->set('date_of_appointment', ($data['date_of_appointment'] ? $this->dateFormat("Y-m-d",$data['date_of_appointment']) : NULL ));
		$this->db->set('personnel_number', $data['personnel_number']);
		
		$this->db->set('appointed_as_id', $data['appointed_as_id']);
		$this->db->set('desgn_id', $data['desgn_id']);
		$this->db->set('payscale_id', $data['payscale_id']);
		$this->db->set('service_status_id', $data['service_status_id']);
		$this->db->set('domicile_id', $data['domicile_id']);
		$this->db->set('gender_id', $data['gender_id']);
		
		$this->db->set('bloodgroup_id', isset($data['bloodgroup_id']) ? $data['bloodgroup_id'] : NULL);
		$this->db->set('religion_id', $data['religion_id']);
		$this->db->set('zone_id', isset($data['zone_id']) ? $data['zone_id'] : NULL);
		
		$this->db->set('qualification', preg_replace('!\s+!', ' ', trim($data['qualification'])));
		$this->db->set('profile_photo', isset($data['profile_photo']) ? $data['profile_photo'] : $data['hidden_profile_photo'] );
		$this->db->set('disability', $data['disability']);
		// $this->db->set('skills', $data['skills']);
		
		$this->db->set('contact_no', $data['contact_no']);
		// $this->db->set('whatsapp_no', $data['whatsapp_no']);
		// $this->db->set('email', $data['email']);
		$this->db->set('permanent_address', preg_replace('!\s+!', ' ', trim($data['permanent_address'])));
		$this->db->set('postal_address', preg_replace('!\s+!', ' ', trim($data['postal_address'])));
		
		$this->db->set('official_bike_no', $data['official_bike_no']);
		$this->db->set('driving_license_type', isset($data['driving_license_type']) ? $data['driving_license_type'] : '' );

		$this->db->set('inquiries', preg_replace('!\s+!', ' ', trim($data['inquiries'])));
		$this->db->set('remarks', $data['remarks']);
		$this->db->set('sorting', 100);
		
		if ($data['id'] > 0 )
		{
			$this->db->set('md_user_id', $this->user_id);

			$this->db->where('id',$data['id']);
			$this->db->update($table);
		}
		else 
		{
			$this->db->set('cr_user_id', $this->user_id);

			$status = $this->db->insert($table);
		}
	
		$status = $this->db->affected_rows();
		return $status;
	}

	public function save_history($table, $data)
    {
        $this->db->set('staff_id', $data['id']);
        $this->db->set('zone_id', $data['zone_id_old'] != 0 ? $data['zone_id_old'] : NULL);        
        $this->db->set('cr_user_id', $this->user_id);

        $this->db->insert($table);

        return $this->db->affected_rows();
    }

	public function save_leave($table, $data)
	{
		//set value by name
		$this->db->set('start_date', $this->dateFormat("Y-m-d", $data['start_date']));
		$this->db->set('end_date', $this->dateFormat("Y-m-d", $data['end_date']));
		$this->db->set('leavetype_id', $data['leavetype_id']);
		$this->db->set('remarks', $data['remarks']);
		$this->db->set('staff_id', $data['staff_id']);

		if ($data['leave_id'] > 0 )
		{
			$this->db->where('id',$data['leave_id']);
			$this->db->update($table);
		}
		else 
		{
			$this->db->insert($table);
		}

		$status = $this->db->affected_rows();
		return $status;
	}
	// fetch_leave_record
	public function fetch_leave_record($table, $where)
	{
		$this->db->select('a.*, b.name as leavetype, DATE_FORMAT(a.start_date,"%d-%m-%Y") as startdate, DATE_FORMAT(a.end_date,"%d-%m-%Y") as enddate');
		$this->db->from($table.' a');
        $this->db->join($this->tables['opleavetypes'].' b', 'a.leavetype_id = b.id');

		$this->db->where($where);
		$this->db->order_by('a.start_date desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_profile($table, $id)
	{
		$this->db->select('a.*, DATE_FORMAT(a.date_of_birth,"%d-%m-%Y") as date_of_birth, DATE_FORMAT(a.date_of_appointment,"%d-%m-%Y") as date_of_appointment, d.name as designation, dd.name as appointed_as, c.name as domicile, z.name as zone, p.name as payscale, g.name as gender, b.name as bloodgroup, r.name as religion, s.name as service_status');

		$this->db->from($table.' a');
		$this->db->join($this->tables['designations'].' d', 'a.desgn_id = d.id');
        $this->db->join($this->tables['designations'].' dd', 'a.appointed_as_id = dd.id');
        $this->db->join($this->tables['cities'].' c', 'a.domicile_id = c.id');
        $this->db->join($this->tables['zones'].' z', 'a.zone_id = z.id', 'left');
		$this->db->join($this->tables['opservicestatus'].' s', 'a.service_status_id = s.id');
        $this->db->join($this->tables['opgender'].' g', 'a.gender_id = g.id', 'left');
		$this->db->join($this->tables['oppayscale'].' p', 'a.payscale_id = p.id');
		$this->db->join($this->tables['opbloodgroup'].' b', 'a.bloodgroup_id = b.id', 'left');
		$this->db->join($this->tables['opreligion'].' r', 'a.religion_id = r.id', 'left');

		$this->db->where('a.id', $id);
		$query = $this->db->get();
		$result = $query->row();
		return $result;
	}

	// **** fetchAllData for table
	public function _fetchAllData($table, $where, $sorting, $fields, $start = NULL, $length = NULL, $search = NULL)
	{
		$this->db->select('a.*, DATE_FORMAT(a.date_of_birth,"%d-%m-%Y") as date_of_birth, DATE_FORMAT(a.date_of_appointment,"%d-%m-%Y") as date_of_appointment, d.name as designation, dd.name as appointed_as, c.name as domicile, z.name as zone, p.name as payscale, g.name as gender, b.name as bloodgroup, r.name as religion, s.name as service_status');

		$this->db->from($table.' a');
        $this->db->join($this->tables['designations'].' d', 'a.desgn_id = d.id');
        $this->db->join($this->tables['designations'].' dd', 'a.appointed_as_id = dd.id');
        $this->db->join($this->tables['cities'].' c', 'a.domicile_id = c.id');
        $this->db->join($this->tables['zones'].' z', 'a.zone_id = z.id', 'left');
		$this->db->join($this->tables['opservicestatus'].' s', 'a.service_status_id = s.id');
        $this->db->join($this->tables['opgender'].' g', 'a.gender_id = g.id', 'left');
		$this->db->join($this->tables['oppayscale'].' p', 'a.payscale_id = p.id');
		$this->db->join($this->tables['opbloodgroup'].' b', 'a.bloodgroup_id = b.id', 'left');
		$this->db->join($this->tables['opreligion'].' r', 'a.religion_id = r.id', 'left');

		$this->db->where($where);
        
		if( isset($search['value']) && $search['value'] != '' ) {				
			$this->db->like('a.name', $search['value']);
			$this->db->or_like('a.name_ur', $search['value']);
			$this->db->or_like('a.cnic_no', $search['value']);
			$this->db->or_like('a.personnel_number', $search['value']);
			$this->db->or_like('a.contact_no', $search['value']);
			$this->db->or_like('a.permanent_address', $search['value']);
			$this->db->or_like('a.postal_address', $search['value']);
			// $this->db->or_like('d.name', $search['value']);
			$this->db->or_like('c.name', $search['value']);
			// $this->db->or_like('z.name', $search['value']);
		}

		if(isset($_POST["order"]))
		{
			$this->db->order_by($_POST['order']['0']['column'], $_POST['order']['0']['dir']);
		}
		else
		{
			$this->db->order_by( $sorting );
		}			
	}	
}