<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends WS_Model
{
    /* public function total_warrants()
    {
        return $this->db->from($this->tables['warrants'])->count_all_results();
    } */
    
    /* public function total_success_warrants()
    {
        $this->db->select('a.*');
        $this->db->from($this->tables['warrants'].' a');
        $this->db->join($this->tables['warrant_type'].' b', 'a.warrant_report_id = b.id');
        $this->db->where('b.progress', 1);
        return $this->db->count_all_results();

    } */

    /* public function total_pending_warrants()
    {
        return $this->db->from($this->tables['warrants'])->where('warrant_report_id', NULL)->count_all_results();
        // return $this->db->where('warrant_report_id', NULL)->from($this->tables['warrants'])->count_all_results();
    } */
    public function chart_data()
    {
        //  back to privious -11 month from today, just like startDate = 2019-02
        $startMonth = date("Y-m", strtotime("-11 months", strtotime(Date('Y-m'))));

        // run loop on year and month and fetch data

        $result = array();

        for ($x = 0; $x < 12; $x++) {
            $newMonth = date("Y-m", strtotime("+".$x." months", strtotime($startMonth)));

            $result[$newMonth]['total'] = $this->db->from($this->tables['warrants'])->like('cr_date', $newMonth)->count_all_results();
            $result[$newMonth]['pending'] = $this->db->from($this->tables['warrants'])->where('warrant_report_id', NULL)->like('cr_date', $newMonth)->count_all_results();
            $result[$newMonth]['success'] = $this->countWarrant(1, $newMonth);
            $result[$newMonth]['others'] = $this->countWarrant(0, $newMonth);
            /* $this->db->from($this->tables['warrants'].' a');
            $this->db->join($this->tables['warrant_type'].' b', 'a.warrant_report_id = b.id');
            $this->db->where('b.progress', 1);
            $this->db->like('cr_date', $newMonth);
            $result[$newMonth]['success'] = $this->db->count_all_results(); */

            /* $this->db->from($this->tables['warrants'].' a');
            $this->db->join($this->tables['warrant_type'].' b', 'a.warrant_report_id = b.id');
            $this->db->where('b.progress', 0);
            $this->db->like('cr_date', $newMonth);
            $result[$newMonth]['others'] = $this->db->count_all_results(); */
        }

        return $result;

    }

    public function countWarrant($progress, $month)
    {
        $this->db->from($this->tables['warrants'].' a');
        $this->db->join($this->tables['warrant_type'].' b', 'a.warrant_report_id = b.id', 'left');
        $this->db->where('b.progress', $progress);
        $this->db->like('cr_date', $month);        
        return $this->db->count_all_results();
    } 

    /* public function countWarrant($table, $where, $like)
    {
        $this->db->from($table.' a');
        $this->db->join($this->tables['warrant_type'].' b', 'a.warrant_report_id = b.id', 'left');
        
        if(!empty($where)) {
            $this->db->where($where);
        }
        if(!empty($like)) {
            $this->db->like($like);
        }
        
        return $this->db->count_all_results();
    } */
}
