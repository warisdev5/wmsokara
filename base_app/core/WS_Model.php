<?php
/**
 * WS_Model
 * A smallest abstraction class for CI Model.
 *
 * @version   1.0.0
 * @author Malik Waris
 */
class WS_Model extends CI_Model 
{
	/**
	 * Holds an array of tables used
	 *
	 * @var array
	*/
	public $tables = [];
	public $user_id;
	
	public function __construct() {
		parent::__construct();
		//load config file
		$this->config->load('main',TRUE);
		$this->tables = $this->config->item('tables','main');

		if( $this->ion_auth->logged_in() )
		{
			$this->user_id = $this->ion_auth->user()->row()->id;
		}
	}

	// *save table record
	public function save($table, $data)
	{
		//set value by name	
		$this->db->set('name', ucwords(strtolower(trim(preg_replace('/\s+/', ' ', $data['name'])))));
		
		if(isset($data['name_ur'])) {
			$this->db->set('name_ur', $data['name_ur']);
		}
		
		// for zone/warrant_type field
		if(isset($data['description'])) {
			$this->db->set('description', $data['description']);
		}

		// for warrant_type field
		if($table == 'warrant_type') {
			$this->db->set('parent_id', isset($data['parent_id']) ? $data['parent_id'] : NULL );
			$this->db->set('progress', isset($data['progress']) ? $data['progress'] : 0 );
		}

		// for district field
		if($table == 'districts') {
			$this->db->set('dist_id', isset($data['dist_id']) ? $data['dist_id'] : NULL );
			$this->db->set('active_cms', isset($data['active_cms']) ? $data['active_cms'] : 0 );
		}
		
		// for designations field
		if($table == 'designations') {
			$this->db->set('short_name', $data['short_name']);
			$this->db->set('designation_type', isset($data['designation_type']) ? $data['designation_type'] : 0 );
		}

		$this->db->set('sorting', $data['sorting']);
		
		if( $data['id'] > 0 )
		{
			$this->db->where('id', $data['id']);
			$this->db->update($table);
		}
		else
		{
			$this->db->insert($table);
		}

		return $this->db->affected_rows();
  	}

	// dateFormat("Y-m-d", "28-09-2023")
	public function dateFormat($format, $date)
	{
		return date($format, strtotime($date));
	}

	// for dropdown options
	public function get_options($table, $where, $id)
	{
		$this->db->select('*');
		$this->db->from($table);
		
		if($id != 0)
		{
			$this->db->where('id', $id);
		}
		else
		{
			$this->db->where( $where );
		}

		if(isset($_POST['searchTerm']))
		{
			$this->db->like('name', $_POST['searchTerm']);
		}
		else
		{
			$this->db->limit(20);
		}

		$this->db->order_by('sorting asc, name asc');
		$query = $this->db->get();
		
		$result = [];

		foreach($query->result() as $row)
		{
			$text = isset($row->name_ur) ? $row->name.','. $row->name_ur : $row->name;

			if($id == 0)
			{
				$result[] = [ "id" => $row->id,  "text" => $text ];
			}
			else
			{
				$result = [ "id" => $row->id, "text" => $text ];
			}			
		}

		return $result;
	}

	// for dropdown options for courts
	public function courtsOptions($table, $id)
	{
		$this->db->select('a.id, CONCAT(a.name," ",d.short_name," ",c.name) as name');
		$this->db->from($table .' a');
		$this->db->join($this->tables['designations'].' d', 'a.desgn_id = d.id');
		$this->db->join($this->tables['cities'].' c', 'a.teh_id = c.id');

		$this->db->where('a.status', 0);

		if($id != 0)
		{
			$this->db->where('a.id', $id);
		}
		else
		{
			$this->db->limit(120);
		}

		if(isset($_POST['searchTerm']))
		{
			$this->db->like('a.name', $_POST['searchTerm']);
		}

		$this->db->order_by('a.status asc, d.sorting ASC, a.sorting ASC');

		$query = $this->db->get();

		$result = [];

		foreach($query->result() as $row)
		{
			if($id == 0)
			{
				$result[] = [ "id" => $row->id,  "text" => $row->name ];
			}
			else
			{
				$result = [ "id" => $row->id, "text" => $row->name ];
			}
		}
		return $result;
	}

	// fetch Row
	public function fetchRow($select, $table, $where, $orderBy)
	{
		$this->db->select($select);
		$this->db->order_by($orderBy);
		$this->db->where($where);
		$query = $this->db->get( $table );
		return $query->row();
	}
	// fetch Table Data
	public function fetchTable($select, $table, $where, $orderBy)
	{
		$this->db->select($select);
		$this->db->from($table);
		$this->db->where($where);
		$this->db->order_by($orderBy);
		$query = $this->db->get();
		return $query->result();
	}

	/**
	 * fetch search
	 * @ form input fields
	 */
	public function fetchSearch($table, $column_name, $value)
    {
        $this->db->select($column_name);
		$this->db->from($table);
        $this->db->like($column_name, $value);
        $this->db->limit(5);
        $this->db->distinct();
		$query = $this->db->get();
    	$result = $query->result();
		return $result;
    }

	// * fetchAllData for table
	public function fetchAllData($table, $where, $sorting, $fields, $start, $length, $search)
	{
		$this->_fetchAllData($table, $where, $sorting, $fields, $start, $length, $search);

		if( isset($start) && $length != '-1' )
		{
			$this->db->limit($length, $start);
		}

		$query = $this->db->get();
		return $query;  
	}

	// ** fetchAllData for table
	public function countFiltered($table, $where, $sorting, $fields, $start, $length, $search)
	{
		$this->_fetchAllData($table, $where, $sorting, $fields, $start, $length, $search);
		$query = $this->db->get();
		return $query->num_rows();
	}

	// *** fetchAllData for table
	public function countAll($table, $where)
	{
		return $this->db->where($where)->count_all_results($table);
	}

	// **** fetchAllData for table
	public function _fetchAllData($table, $where, $sorting, $fields, $start = NULL, $length = NULL, $search = NULL)
	{
		$this->db->select('*');

		$this->db->from($table);
		// $this->db->where($where);

		if(isset($search['value']) && $search['value'] != '')
		{			
			$this->db->like($fields[0], $search['value']);

			for ($x = 1; $x < count($fields); $x++) {
				$this->db->or_like($fields[$x], $search['value']);
			}
			
		}
		else
        {            
            $this->db->where($where);
        }

		if(isset($_POST["order"]))
		{
			$this->db->order_by($_POST['order']['0']['column'], $_POST['order']['0']['dir']);
		}
		else
		{
			$this->db->order_by( $sorting );
		}			
	}

	/**
	 * counts warrants with join table
	 * @ reports and dashboard route
	 */
    public function countWarrants($where, $where_in_array)
    {
        $this->db->from($this->tables['warrants'] . ' a');
        $this->db->join($this->tables['warrant_type'] . ' b', 'a.warrant_report_id = b.id');
		if(!empty($where)) {
			$this->db->where($where);
		}
		if(!empty($where_in_array)) {
			$this->db->where_in('b.name', $where_in_array);
		}
        return $this->db->count_all_results();
    }
	
}