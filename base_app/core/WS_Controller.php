<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Controller for superadmin, admin
 */
class WS_Controller extends CI_Controller {

	/**
	 * Holds an array of tables used
	 *
	 * @var array
	 */
	public $tables = [];
	public $data = [];
	public $config_vars = [];
	public $model;
	public $user;
	public $sorting = 'sorting asc, id asc';
	public $fields;
	public $where = [];

	function __construct()
	{
		parent::__construct();
		// define time zone asia/pakistan
		date_default_timezone_set('Asia/Karachi');

		$this->session->set_userdata('url',uri_string());

		//load config file
		$this->config->load('main',TRUE);
    	$this->tables = $this->config->item('tables','main');
		$this->data = $this->config->item('default', 'main');

		// Pehlwan Vs Sabhrahi  Naseem

		// ALL QUERY RUN ON LIVE SERVER MYSQL DATABASE
		// UPDATE warrants SET case_title = REPLACE(REPLACE(TRIM(case_title), '  ', ' '), '  ', ' ');
		// UPDATE warrants SET address = REPLACE(REPLACE(TRIM(address), '  ', ' '), '  ', ' ');
		// UPDATE warrants SET plaintiff = REPLACE(REPLACE(TRIM(plaintiff), '  ', ' '), '  ', ' ');
		// UPDATE warrants SET defendant = REPLACE(REPLACE(TRIM(defendant), '  ', ' '), '  ', ' ');

		// UPDATE cases SET is_sync=2, is_sync2=2, plaintiff_name = REPLACE(REPLACE(TRIM(plaintiff_name), '  ', ' '), '  ', ' ');
		// UPDATE cases SET is_sync=2, is_sync2=2, defendant_name = REPLACE(REPLACE(TRIM(defendant_name), '  ', ' '), '  ', ' ');
		// UPDATE cases SET is_sync=2, is_sync2=2, plaintiff_address = REPLACE(REPLACE(TRIM(plaintiff_address), '  ', ' '), '  ', ' ');
		// UPDATE cases SET is_sync=2, is_sync2=2, plaintiff_lawyer = REPLACE(REPLACE(TRIM(plaintiff_lawyer), '  ', ' '), '  ', ' ');

		// load models		
		$this->load->model( 'dashboard_model' );		
		$this->model = $this->dashboard_model;

		// enable profiler
		// $this->output->enable_profiler(TRUE);

		$active_district = $this->db->select('id')->where('active_cms', 1)->get( $this->tables['cities'] )->row();
		if(!empty($active_district)) {
			$this->template->set_active_district($active_district->id);
		}

		$this->user = $this->ion_auth->user()->row();
		
		$active_controller = $this->router->fetch_class();
		$active_method 	   = $this->router->fetch_method();

		$membersAccess = in_array($active_controller, ['dashboard','warrants','reports','users'] );

		$publicAccess = in_array($active_controller, ['search'] );

		if( !$this->ion_auth->logged_in() && $publicAccess == false )
		{
			$this->session->set_flashdata('message', 'Please login to access this page!');
			$this->session->set_flashdata('message_type', 'warning');
			redirect('login');
		}
		else
		{
			if ( !$this->ion_auth->is_admin() )
			{
				if ( $this->ion_auth->in_group('members') && $membersAccess == false )
				{
					$this->session->set_flashdata('message','You must be an administrator to view this page!');
					$this->session->set_flashdata('message_type','warning');
					redirect('admin');
				}

				if( $this->ion_auth->in_group('members') && $active_method == 'delete' && $membersAccess == false )
				{
					$this->session->set_flashdata('message','You must be an administrator to delete any value!');
					$this->session->set_flashdata('message_type','warning');
					redirect('admin');
				}

			}
		}
		
	}

	/**
	 * create custom function
	 * @string - value
	 * @action - encrypt/ decrypt
	 */
	function encrypt_decrypt($string, $action = 'encrypt')
	{
		$encrypt_method = "AES-256-CBC";
		$secret_key = 'AA74CDCC2XT4D5S9J4F1136HH7B63C27'; // user define private key
		$secret_iv = '5fgf5HJ5d4s1'; // user define secret key
		$key = hash('sha256', $secret_key);
		$iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
		if ($action == 'encrypt') {
			$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
			$output = base64_encode($output);
		} else if ($action == 'decrypt') {
			$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
		}
		return $output;
	}

	public function dateFormat($format, $date)
	{
		return date($format, strtotime($date));
	}
	public function countDayes($firstDate, $secondDate)
	{
		// Calculating the difference in timestamps
		$diff = strtotime($secondDate) - strtotime($firstDate);
  
		// 1 day = 24 hours
		// 24 * 60 * 60 = 86400 seconds
		return abs(round($diff / 86400)) +1;
	}
	// for dropdown options
	public function getOptions($id)
	{
		$table = $this->tables[$_POST["table"]];

		$field_value = $_POST["field_value"] === "NULL" ? NULL : $_POST["field_value"];

		$where = !empty($_POST['field_name']) ? [ $_POST["field_name"] => $field_value ] : [];

		$result = $this->model->get_options($table, $where, $id);

		echo json_encode($result);
		die;
	}
	// for dropdown options
	public function courtsOptions($id)
    {
		$table = $this->tables[$_POST["table"]];
		
		$result = $this->model->courtsOptions($table, $id);

		echo json_encode($result);
		die;
    }


	/**
	 * fetch search
	 * @ form input fields
	 */
	public function fetchSearch()
    {
        if(isset($_POST['search']))
        {
			$table = $_POST['table'];
            $name = $_POST['search'];
            $field_name = $_POST['field_name'];

            $fetchRecord = $this->model->fetchSearch($table, $field_name, $name);

            foreach( $fetchRecord as $val)
            {
                $input_value = ucwords(strtolower($val->$field_name));
                echo "<option value='$input_value'>$input_value</option>";
            }
        }
    }
}