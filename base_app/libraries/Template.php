<?php 
defined('BASEPATH') or die('Restricted Access');

class Template
{
	private $ci ;
	private $title = 'WMS of Senior Civil Judge Okara';
	private $description = 'Complete solution of Civil Nazar Branch of District Judiciary Punjab, Pakistan';
	private $key_words = 'DSJ, District and Sessions Judge, Sessions Court, Senior Civil Judge, SCJ';
	private $author = 'Malik Waris';
	private $user = '';
	private $page_title ='';
	private $active_district_id = '';
	private $css_files = [];
	private $header_js_files = [];
	private $js_script = [];
	private $js_files = [];

	public function __construct()
	{
		$this->ci =& get_instance();
	}
	public function loadview($template_name,$view_name,$view_data=array(),$template_data=array())
	{
		$template_data['user'] = $this->ci->ion_auth->logged_in() ? $this->ci->ion_auth->user()->row() : $this->user;

		$template_data['active_controller'] = $this->ci->uri->segment(2);
		$template_data['active_method'] = $this->ci->uri->segment(3);
		$template_data['title'] = $this->title;
		$template_data['description'] = $this->description;
		$template_data['key_words'] = $this->key_words;
		$template_data['author'] = $this->author;
		$template_data['main_content'] = $this->ci->load->view($view_name,$view_data,true);
		$template_data['page_title'] = $this->page_title;
		$template_data['active_district_id'] = $this->active_district_id;
		$template_data['css_files'] = $this->css_files;
		$template_data['header_js_files'] = $this->header_js_files;
		$template_data['js_script'] = $this->js_script;
		$template_data['js_files'] = $this->js_files;
		$this->ci->load->view($template_name, $template_data);
	}

	public function add_css($css)
	{
		$this->css_files[]=$css;
	}
	public function add_header_js($js)
	{
		$this->header_js_files[]=$js;
	}
	public function add_js_script($js_script)
	{
		$this->js_script[]=$js_script;
	}
	public function add_js($js)
	{
		$this->js_files[]=$js;
	}
	public function set_active_district($id)
	{
		$this->active_district_id = $id; 
	}
	public function set_description($description)
	{
		$this->description = $description;
	}
	public function set_key_words($key_words)
	{
		$this->key_words = $key_words;
	}
	public function set_title($title)
	{
		$this->title = $title ; 
	}
	public function set_page_title($page_title)
	{
		$this->page_title = $page_title;
	}
}