-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 02, 2024 at 05:34 AM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wmsokara`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `courts`
--

CREATE TABLE `courts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(99) NOT NULL,
  `name_ur` varchar(99) NOT NULL,
  `desgn_id` smallint(6) UNSIGNED DEFAULT NULL,
  `teh_id` smallint(6) UNSIGNED DEFAULT NULL,
  `sorting` tinyint(4) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0=active, 1=Transfferred ',
  `cr_user_id` int(10) UNSIGNED NOT NULL,
  `md_user_id` int(10) UNSIGNED NOT NULL,
  `cr_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `md_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `name` varchar(99) NOT NULL,
  `name_ur` varchar(99) NOT NULL,
  `short_name` varchar(20) NOT NULL,
  `designation_type` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `sorting` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `name`, `name_ur`, `short_name`, `designation_type`, `sorting`) VALUES
(30, 'District & Sessions Judge', 'ڈسٹرکٹ اینڈ سیشن جج', 'D&SJ', 1, 1),
(31, 'Additional District & Sessions Judge', 'ایڈیشنل ڈسٹرکٹ اینڈ سیشن جج ', 'ASJ', 1, 2),
(32, 'Senior Civil Judge (civil Division)', 'سینئر سول جج (سول ڈویژن)', 'SCJ (Admin)', 1, 3),
(33, 'Senior Civil Judge (criminal Division)', 'سینئر سول جج (کریمینل ڈویژن)', 'SCJ (Cr.Div)', 1, 4),
(34, 'Senior Civil Judge (Family Division)', 'سینئر سول جج (فیملی ڈویژن)', 'SCJ (F.D)', 1, 6),
(35, 'Civil Judge Class-I', 'سول جج درجہ اول', 'CJ-I', 1, 9),
(36, 'Civil Judge Class-II', 'سول جج درجہ دوئم ', 'CJ-II', 1, 10),
(37, 'Civil Judge Class-III', 'سول جج درجہ سوئم', 'CJ-III', 1, 11),
(38, 'Civil Judge Cum Judicial Magistrate', 'سول جج جوڈیشل مجسٹریٹ', 'CJ', 1, 12),
(39, 'Bailiff', 'بیلف', 'BF', 0, 49),
(40, 'Process Server', 'پراسیس سرور', 'PS', 0, 50),
(41, 'Acting Nazar', 'قائمقام ناظر', 'AN', 0, 50);

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `name_ur` varchar(100) NOT NULL,
  `dist_id` smallint(6) UNSIGNED DEFAULT NULL,
  `active_cms` tinyint(1) NOT NULL,
  `sorting` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `name`, `name_ur`, `dist_id`, `active_cms`, `sorting`) VALUES
(15, 'Faisalabad', 'فیصل آباد', NULL, 0, 10),
(20, 'Chiniot', 'چنیوٹ', NULL, 0, 10),
(21, 'Toba Tek Singh', 'ٹوبہ ٹیک سنگھ', NULL, 0, 10),
(22, 'Jhang', 'جھنگ', NULL, 0, 10),
(23, 'Multan', 'ملتان', NULL, 0, 10),
(24, 'Lahore', 'لاہور', NULL, 0, 10),
(25, 'Sahiwal', 'ساہیوال', NULL, 0, 10),
(26, 'Sheikhupura', 'شیخوپورہ', NULL, 0, 10),
(27, 'Gujranwala', 'گجرانوالہ', NULL, 0, 10),
(28, 'Muzaffargarh', 'مظفرگڑھ', NULL, 0, 10),
(29, 'Okara', 'اوکاڑہ', NULL, 1, 1),
(30, 'Layyah', 'لیہ', NULL, 0, 10),
(31, 'Sargodha', 'سرگودھا', NULL, 0, 10),
(32, 'Attock', 'اٹک', NULL, 0, 10),
(33, 'Bahawalpur', 'بہاولپور', NULL, 0, 10),
(34, 'Bahawalnagar', 'بہاولنگر', NULL, 0, 10),
(35, 'Bhakkar', 'بھکر', NULL, 0, 10),
(36, 'Chakwal', 'چکوال', NULL, 0, 10),
(37, 'D.G. Khan', 'ڈیرہ غازی خان', NULL, 0, 10),
(38, 'Gujrat', 'گجرات', NULL, 0, 10),
(39, 'Hafizabad', 'حافظ آباد', NULL, 0, 10),
(40, 'Jhelum', 'جہلم', NULL, 0, 10),
(41, 'Kasur', 'قصور', NULL, 0, 10),
(42, 'Khanewal', 'خانیوال', NULL, 0, 10),
(43, 'Khushab', 'خوشاب', NULL, 0, 10),
(44, 'Lodhran', 'لودھراں', NULL, 0, 10),
(45, 'M.B. Din', 'منڈی بہاولدین', NULL, 0, 10),
(46, 'Mianwali', 'میانوالی', NULL, 0, 10),
(47, 'Narowal', 'نارووال', NULL, 0, 10),
(48, 'Nankana Sahib', 'ننکانہ صاحب', NULL, 0, 10),
(49, 'Pak Pattan', 'پاکپتن', NULL, 0, 10),
(50, 'Rahim Yar Khan', 'رحیم یار خان', NULL, 0, 10),
(51, 'Rajanpur', 'راجن پور', NULL, 0, 10),
(52, 'Rawalpindi', 'راولپنڈی', NULL, 0, 10),
(53, 'Sialkot', 'سیالکوٹ', NULL, 0, 10),
(54, 'Vehari', 'وہاڑی', NULL, 0, 10),
(55, 'Karachi', 'کراچی', NULL, 0, 10),
(56, 'Islamabad', 'اسلام آباد', NULL, 0, 10),
(57, 'Bunir  (KPK)', 'بونیر   ( کے پی کے)', NULL, 0, 39);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'superadmin', 'Super Admin'),
(4, 'adminview', 'only administrative task watch');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `opbloodgroup`
--

CREATE TABLE `opbloodgroup` (
  `id` tinyint(2) UNSIGNED NOT NULL,
  `name` varchar(10) NOT NULL,
  `sorting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `opbloodgroup`
--

INSERT INTO `opbloodgroup` (`id`, `name`, `sorting`) VALUES
(1, 'A+', 1),
(2, 'B+', 2),
(3, 'AB+', 3),
(4, 'O+', 4),
(5, 'A-', 5),
(6, 'B-', 6),
(7, 'AB-', 7),
(8, 'O-', 8);

-- --------------------------------------------------------

--
-- Table structure for table `opgender`
--

CREATE TABLE `opgender` (
  `id` tinyint(2) UNSIGNED NOT NULL,
  `name` varchar(15) NOT NULL,
  `sorting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `opgender`
--

INSERT INTO `opgender` (`id`, `name`, `sorting`) VALUES
(1, 'Male', 1),
(2, 'Female', 2),
(3, 'Transgender', 3);

-- --------------------------------------------------------

--
-- Table structure for table `opleavetypes`
--

CREATE TABLE `opleavetypes` (
  `id` tinyint(2) UNSIGNED NOT NULL,
  `name` varchar(25) NOT NULL,
  `sorting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `opleavetypes`
--

INSERT INTO `opleavetypes` (`id`, `name`, `sorting`) VALUES
(1, 'Casual Leave', 1),
(2, 'Medical Leave', 2),
(3, 'Earned Leave', 3),
(4, 'Study Leave', 4),
(5, 'Extraordinary Leave', 5),
(6, 'Bereavement Leave', 6),
(7, 'Maternity/Paternity Leave', 7),
(8, 'Others Leave', 8);

-- --------------------------------------------------------

--
-- Table structure for table `opmaritalstatus`
--

CREATE TABLE `opmaritalstatus` (
  `id` tinyint(2) UNSIGNED NOT NULL,
  `name` varchar(10) NOT NULL,
  `sorting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `opmaritalstatus`
--

INSERT INTO `opmaritalstatus` (`id`, `name`, `sorting`) VALUES
(1, 'Single', 1),
(2, 'Married', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oppayscale`
--

CREATE TABLE `oppayscale` (
  `id` tinyint(2) UNSIGNED NOT NULL,
  `name` varchar(10) NOT NULL,
  `sorting` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oppayscale`
--

INSERT INTO `oppayscale` (`id`, `name`, `sorting`) VALUES
(1, 'BPS-01', 1),
(2, 'BPS-02', 1),
(3, 'BPS-03', 1),
(4, 'BPS-04', 1),
(5, 'BPS-05', 1),
(6, 'BPS-06', 1),
(7, 'BPS-07', 1),
(8, 'BPS-08', 1),
(9, 'BPS-09', 1),
(10, 'BPS-10', 1),
(11, 'BPS-11', 1),
(12, 'BPS-12', 1),
(13, 'BPS-13', 1),
(14, 'BPS-14', 1),
(15, 'BPS-15', 1),
(16, 'BPS-16', 1),
(17, 'BPS-17', 1),
(18, 'BPS-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `opreligion`
--

CREATE TABLE `opreligion` (
  `id` tinyint(2) UNSIGNED NOT NULL,
  `name` varchar(15) NOT NULL,
  `sorting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `opreligion`
--

INSERT INTO `opreligion` (`id`, `name`, `sorting`) VALUES
(1, 'Islam', 1),
(2, 'Christianity', 2),
(3, 'Sikhism', 3),
(4, 'Hinduism', 4),
(5, 'Other', 5);

-- --------------------------------------------------------

--
-- Table structure for table `opservicestatus`
--

CREATE TABLE `opservicestatus` (
  `id` tinyint(2) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `sorting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `opservicestatus`
--

INSERT INTO `opservicestatus` (`id`, `name`, `sorting`) VALUES
(1, 'In-Service', 1),
(2, 'Retired', 2),
(3, 'Suspend', 3),
(4, 'Dismissed', 4),
(5, 'Transferred', 5);

-- --------------------------------------------------------

--
-- Table structure for table `staffdata`
--

CREATE TABLE `staffdata` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(99) NOT NULL,
  `name_ur` varchar(99) NOT NULL,
  `father` varchar(99) NOT NULL,
  `appointed_as_id` smallint(6) UNSIGNED NOT NULL COMMENT 'appointed as designation',
  `desgn_id` smallint(6) UNSIGNED NOT NULL COMMENT 'Current Designation',
  `payscale_id` tinyint(2) UNSIGNED DEFAULT NULL,
  `service_status_id` tinyint(2) UNSIGNED DEFAULT NULL,
  `gender_id` tinyint(2) UNSIGNED DEFAULT NULL,
  `bloodgroup_id` tinyint(2) UNSIGNED DEFAULT NULL,
  `maritalstatus_id` tinyint(2) UNSIGNED DEFAULT NULL,
  `religion_id` tinyint(2) UNSIGNED DEFAULT NULL,
  `personnel_number` varchar(10) NOT NULL,
  `qualification` varchar(50) NOT NULL,
  `date_of_appointment` date DEFAULT NULL,
  `domicile_id` smallint(6) UNSIGNED NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `disability` varchar(50) NOT NULL,
  `skills` varchar(255) NOT NULL,
  `official_bike_no` varchar(10) NOT NULL,
  `driving_license_type` varchar(25) NOT NULL,
  `cnic_no` varchar(15) NOT NULL,
  `contact_no` varchar(12) NOT NULL,
  `whatsapp_no` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `permanent_address` varchar(255) NOT NULL,
  `postal_address` varchar(255) NOT NULL,
  `zone_id` smallint(6) UNSIGNED DEFAULT NULL,
  `profile_photo` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `inquiries` varchar(255) NOT NULL,
  `remarks` text NOT NULL,
  `sorting` int(11) NOT NULL,
  `cr_user_id` int(10) UNSIGNED NOT NULL,
  `md_user_id` int(10) UNSIGNED NOT NULL,
  `cr_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `md_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `staff_leaves`
--

CREATE TABLE `staff_leaves` (
  `id` int(10) UNSIGNED NOT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `leavetype_id` tinyint(2) UNSIGNED DEFAULT NULL,
  `remarks` text NOT NULL,
  `cr_date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'warisdev5', '$2y$10$RV/fV6hYOD7gX5mDU3hGkejgQAzW5bIf/8DtbH2YMt7gUU/1RJ.S.', 'warisdev5@gmail.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1711084670, 1, 'Malik', 'Waris', 'IT-Section', '0300-7201629');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(23, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `warrants`
--

CREATE TABLE `warrants` (
  `id` int(10) UNSIGNED NOT NULL,
  `warrant_type_id` smallint(6) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `court_id` int(10) UNSIGNED DEFAULT NULL,
  `dist_id` smallint(6) UNSIGNED DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `date_of_hearing` date DEFAULT NULL,
  `case_title` varchar(255) NOT NULL,
  `plaintiff` varchar(100) NOT NULL,
  `defendant` varchar(100) NOT NULL,
  `warrant_report_id` smallint(6) UNSIGNED DEFAULT NULL COMMENT 'come from warrant_type table id whose parent_id is not null',
  `report_date` date DEFAULT NULL,
  `remarks` text NOT NULL,
  `cr_user_id` int(10) UNSIGNED DEFAULT NULL,
  `md_user_id` int(10) UNSIGNED DEFAULT NULL,
  `cr_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `md_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `warrant_history`
--

CREATE TABLE `warrant_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `warrant_id` int(10) UNSIGNED DEFAULT NULL,
  `zone_id` smallint(6) UNSIGNED DEFAULT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `cr_user_id` int(10) UNSIGNED NOT NULL,
  `cr_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `warrant_type`
--

CREATE TABLE `warrant_type` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `name_ur` varchar(55) NOT NULL,
  `parent_id` smallint(6) UNSIGNED DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `progress` tinyint(1) NOT NULL DEFAULT '0',
  `sorting` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `warrant_type`
--

INSERT INTO `warrant_type` (`id`, `name`, `name_ur`, `parent_id`, `description`, `progress`, `sorting`) VALUES
(30, 'Bailable Warrant of Arrest', 'وارنٹ گرفتاری ضمانتی   ', NULL, '', 0, 1),
(31, 'Non-bailable Warrant of Arrest', 'وارنٹ گرفتاری بلاضمانتی', NULL, '', 0, 2),
(33, 'Not arrested', 'عدم گرفتاری', 30, '', 0, 2),
(34, 'Arrested', 'گرفتاری ہوچکی', 30, '', 1, 1),
(35, 'Warrant of Attachment', 'وارنٹ قرقی', NULL, '', 0, 3),
(36, 'Warrant of Possession', 'وارنٹ دخل', NULL, '', 0, 4),
(37, 'Warrant Section 100', 'وارنٹ دفعہ 100', NULL, '', 0, 5),
(38, 'Perpetual Warrant of Arrest', 'وارنٹ گرفتاری دائمی', NULL, '', 0, 6),
(40, 'Robkar Saman Jaheez', 'روبکار سامان جہیز', NULL, '', 0, 8),
(41, 'Abscondence', 'روپوشی', 30, '', 0, 3),
(42, 'Incomplete Address ', 'پتہ نامکمل', 30, '', 0, 6),
(43, 'Died', 'فوت شد', 30, '', 0, 7),
(44, 'Incorrect Address', 'عدم رہائش', 30, '', 0, 5),
(45, 'Accommodation changed', 'ترک سکونت', 30, '', 0, 4),
(46, 'Arrested', 'گرفتاری ہوچکی', 31, '', 1, 1),
(47, 'Not arrested', 'عدم گرفتاری', 31, '', 0, 2),
(48, 'Accommodation changed', 'ترک سکونت', 31, '', 0, 4),
(49, 'Incomplete Address', 'پتہ نامکمل', 31, '', 0, 6),
(50, 'Died', 'فوت شد', 31, '', 0, 7),
(51, 'Abscondence', 'روپوشی', 31, '', 0, 3),
(52, 'Incorrect Address', 'عدم رہائش', 31, '', 0, 5),
(53, 'Executed', 'تعمیل ہوگئی', 35, '', 1, 1),
(54, 'Incomplete Address', 'پتہ نامکمل', 35, '', 0, 2),
(55, 'Goods to be attached not found.', 'سامان کا نہ ملنا', 35, '', 0, 3),
(56, 'Issued To Tehsildar', 'تحصیلدار کو جاری ہوا', 35, '', 0, 5),
(57, 'Executed', 'مکمل', 36, '', 1, 1),
(58, 'Not Executed', 'نامکمل', 36, '', 0, 2),
(59, 'Executed', 'مکمل', 37, '', 1, 1),
(60, 'Not Executed', 'نامکمل', 37, '', 0, 2),
(61, 'Executed', 'مکمل', 38, '', 1, 1),
(62, 'Not Executed', 'نامکمل', 38, '', 0, 2),
(65, 'Executed', 'مکمل', 40, '', 1, 1),
(66, 'Not Executed', 'نامکمل', 40, '', 0, 2),
(70, 'Partially Executed', 'جزوی تعمیل', 40, '', 1, 10),
(72, 'Robkar Mulaqat', 'روبکار ملاقات', NULL, '', 1, 10),
(73, 'Executed', 'ملاقات ہو چکی ', 72, '', 1, 10),
(74, 'Not Executed', 'ملاقات نہیں ہوئی', 72, '', 0, 10),
(75, 'Issued To SHO', 'ایس- ایچ -او کو جاری ہوا', 30, '', 0, 10),
(76, 'Issued to SHO', 'ایس- ایچ -او کو جاری ہوا', 31, '', 0, 10),
(77, 'Robkar Commission', 'روبکار کمیشن', NULL, '', 1, 10),
(78, 'Executed', 'مکمل', 77, '', 1, 10),
(79, 'Not Executed', 'نا مکمل', 77, '', 0, 10),
(80, 'Show Cause Notice', 'شوکاز نوٹس', NULL, '', 1, 10),
(81, 'Executed', 'مکمل', 80, '', 1, 10),
(82, 'Not Executed', 'نامکمل', 80, '', 0, 10),
(83, 'Warrant Cancel', 'وارنٹ منسوخ', 30, '', 0, 10),
(84, 'Warrant Cancel', 'وارنٹ منسوخ', 31, '', 0, 10),
(85, 'Warrant Cancel', 'وارنٹ منسوخ', 35, '', 0, 10),
(86, 'Warrant Cancel', 'وارنٹ منسوخ', 36, '', 0, 10),
(87, 'Warrant Cancel', 'وارنٹ منسوخ', 37, '', 0, 10),
(88, 'Warrant Cancel', 'وارنٹ منسوخ', 38, '', 0, 10),
(89, 'Robkar Cancel', 'روبکار منسوخ', 40, '', 0, 10),
(90, 'Robkar Cancel', 'روبکار منسوخ', 77, '', 0, 10),
(91, 'Robkar Cancel', 'روبکار منسوخ', 72, '', 0, 10),
(92, 'Not Executed', 'نامکمل', 35, '', 0, 10),
(93, 'Attachement Not executed', 'قرقی نہ ہوئی۔', 35, '', 0, 10),
(94, 'Robkar Takmeel-e-Hukam', 'روبکار تکمیل حکم', NULL, '', 1, 10),
(95, 'Executed', 'مکمل', 94, '', 1, 10),
(96, 'Not Executed', 'نا مکمل', 94, '', 0, 10),
(97, 'Robkar Hawalgi Immature', 'روبکار حوالگی نابالغ', NULL, '', 1, 10),
(98, 'Recovery', 'بازیابی', 97, '', 1, 10),
(99, 'Not Recovery', 'بازیابی نہ ہوئی', 97, '', 0, 10),
(100, 'Property Distribution', 'تقسیم جائیداد', NULL, '', 1, 10),
(101, 'Executed', 'تقسیم ہو گئی', 100, '', 1, 10),
(102, 'Not Executed', 'تقسیم نہ ہوئی', 100, '', 0, 10),
(103, 'Robkar Summons', 'روبکار طلبی', NULL, '', 1, 10),
(104, 'Excuted', 'مکمل', 103, '', 1, 10),
(105, 'Not Executed', 'نامکمل', 103, '', 0, 10),
(106, 'Court Auction', 'روبکار رپورٹ نیلامی', NULL, '', 1, 10),
(107, 'Executed', 'مکمل', 106, '', 1, 10),
(108, 'Not Executed', 'نامکمل', 106, '', 0, 10),
(109, 'Robkar of Attachment', 'روبکار قرقی جائیداد', NULL, '', 1, 10),
(110, 'Executed', 'عملدرآمد ہو چکا ہے۔', 109, '', 1, 10),
(111, 'Not Executed', 'قرقی نہ ہوئی', 109, '', 0, 10),
(112, 'Robkar Received To Revenue Officer', 'روبکار قرقی متعلقہ ریونیو آفیسر کو وصول کروائی', 109, '', 0, 10),
(113, 'Warrant of Witness', 'وارنٹ بنام گواہ', NULL, '', 1, 10),
(114, 'Executed', 'عملدرآمد ہو چکا ہے', 113, '', 1, 10),
(115, 'Not Executed', 'عملدرآمد نہ ہو سکا', 113, '', 0, 10),
(116, 'Robkar Attachment', 'روبکار قرقی', NULL, '', 1, 10),
(117, 'Executed', 'عمل درآمد ہو چکاہے', 116, '', 1, 10),
(118, 'Not Executed', 'عمل درآمد نہ ہوا', 116, '', 0, 10);

-- --------------------------------------------------------

--
-- Table structure for table `zones`
--

CREATE TABLE `zones` (
  `id` smallint(6) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `name_ur` varchar(55) NOT NULL,
  `description` varchar(255) NOT NULL,
  `sorting` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zones`
--

INSERT INTO `zones` (`id`, `name`, `name_ur`, `description`, `sorting`) VALUES
(1, 'City I', '', '', 1),
(2, 'City II', '', '', 2),
(3, 'City III', '', '', 3),
(4, 'City IV', '', '', 4),
(10, 'No Zone', '', 'No zone defined.', 10);

-- --------------------------------------------------------

--
-- Table structure for table `zone_history`
--

CREATE TABLE `zone_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `zone_id` smallint(6) UNSIGNED DEFAULT NULL,
  `cr_user_id` int(10) UNSIGNED NOT NULL,
  `cr_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `courts`
--
ALTER TABLE `courts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_desgn_id_courts` (`desgn_id`),
  ADD KEY `fk_teh_id_courts` (`teh_id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_districts_parent_id` (`dist_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opbloodgroup`
--
ALTER TABLE `opbloodgroup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opgender`
--
ALTER TABLE `opgender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opleavetypes`
--
ALTER TABLE `opleavetypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opmaritalstatus`
--
ALTER TABLE `opmaritalstatus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oppayscale`
--
ALTER TABLE `oppayscale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opreligion`
--
ALTER TABLE `opreligion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opservicestatus`
--
ALTER TABLE `opservicestatus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staffdata`
--
ALTER TABLE `staffdata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_desgn_staff` (`appointed_as_id`),
  ADD KEY `fk_bloodgroup_staff` (`bloodgroup_id`),
  ADD KEY `fk_crrnt_desgn_staff` (`desgn_id`),
  ADD KEY `fk_domicile_staff` (`domicile_id`),
  ADD KEY `fk_gender_staff` (`gender_id`),
  ADD KEY `fk_payscale_staff` (`payscale_id`),
  ADD KEY `fk_religion_staff` (`religion_id`),
  ADD KEY `fk_servicestatus_staff` (`service_status_id`),
  ADD KEY `fk_zone_staff` (`zone_id`),
  ADD KEY `fk_martitalstatus_staff` (`maritalstatus_id`);

--
-- Indexes for table `staff_leaves`
--
ALTER TABLE `staff_leaves`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_staff_id_leaves` (`staff_id`),
  ADD KEY `fk_leavetype_leaves` (`leavetype_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `warrants`
--
ALTER TABLE `warrants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_court_id_warrants` (`court_id`),
  ADD KEY `fk_dist_id_warrants` (`dist_id`),
  ADD KEY `fk_wreport_warrants` (`warrant_report_id`),
  ADD KEY `fk_staff_id_warrants` (`staff_id`),
  ADD KEY `fk_wtype_warrants` (`warrant_type_id`);

--
-- Indexes for table `warrant_history`
--
ALTER TABLE `warrant_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_warranth_staff_id` (`staff_id`),
  ADD KEY `fk_warranth_warrant_id` (`warrant_id`),
  ADD KEY `fk_warranth_zone_id` (`zone_id`),
  ADD KEY `fk_warranth_user_id` (`cr_user_id`);

--
-- Indexes for table `warrant_type`
--
ALTER TABLE `warrant_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_parent_id_warrant_type` (`parent_id`);

--
-- Indexes for table `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zone_history`
--
ALTER TABLE `zone_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_zoneh_staff_id` (`staff_id`),
  ADD KEY `fk_zoneh_zone_id` (`zone_id`),
  ADD KEY `fk_zoneh_user_id` (`cr_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courts`
--
ALTER TABLE `courts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `opbloodgroup`
--
ALTER TABLE `opbloodgroup`
  MODIFY `id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `opgender`
--
ALTER TABLE `opgender`
  MODIFY `id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `opleavetypes`
--
ALTER TABLE `opleavetypes`
  MODIFY `id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `opmaritalstatus`
--
ALTER TABLE `opmaritalstatus`
  MODIFY `id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oppayscale`
--
ALTER TABLE `oppayscale`
  MODIFY `id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `opreligion`
--
ALTER TABLE `opreligion`
  MODIFY `id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `opservicestatus`
--
ALTER TABLE `opservicestatus`
  MODIFY `id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `staffdata`
--
ALTER TABLE `staffdata`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff_leaves`
--
ALTER TABLE `staff_leaves`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `warrants`
--
ALTER TABLE `warrants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `warrant_history`
--
ALTER TABLE `warrant_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `warrant_type`
--
ALTER TABLE `warrant_type`
  MODIFY `id` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `zones`
--
ALTER TABLE `zones`
  MODIFY `id` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `zone_history`
--
ALTER TABLE `zone_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `courts`
--
ALTER TABLE `courts`
  ADD CONSTRAINT `fk_desgn_id_courts` FOREIGN KEY (`desgn_id`) REFERENCES `designations` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_teh_id_courts` FOREIGN KEY (`teh_id`) REFERENCES `districts` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `districts`
--
ALTER TABLE `districts`
  ADD CONSTRAINT `fk_districts_parent_id` FOREIGN KEY (`dist_id`) REFERENCES `districts` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `staffdata`
--
ALTER TABLE `staffdata`
  ADD CONSTRAINT `fk_bloodgroup_staff` FOREIGN KEY (`bloodgroup_id`) REFERENCES `opbloodgroup` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_crrnt_desgn_staff` FOREIGN KEY (`desgn_id`) REFERENCES `designations` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_desgn_staff` FOREIGN KEY (`appointed_as_id`) REFERENCES `designations` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_domicile_staff` FOREIGN KEY (`domicile_id`) REFERENCES `districts` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_gender_staff` FOREIGN KEY (`gender_id`) REFERENCES `opgender` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_martitalstatus_staff` FOREIGN KEY (`maritalstatus_id`) REFERENCES `opmaritalstatus` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_payscale_staff` FOREIGN KEY (`payscale_id`) REFERENCES `oppayscale` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_religion_staff` FOREIGN KEY (`religion_id`) REFERENCES `opreligion` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_servicestatus_staff` FOREIGN KEY (`service_status_id`) REFERENCES `opservicestatus` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_zone_staff` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `staff_leaves`
--
ALTER TABLE `staff_leaves`
  ADD CONSTRAINT `fk_leavetype_leaves` FOREIGN KEY (`leavetype_id`) REFERENCES `opleavetypes` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_staff_id_leaves` FOREIGN KEY (`staff_id`) REFERENCES `staffdata` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `warrants`
--
ALTER TABLE `warrants`
  ADD CONSTRAINT `fk_court_id_warrants` FOREIGN KEY (`court_id`) REFERENCES `courts` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_dist_id_warrants` FOREIGN KEY (`dist_id`) REFERENCES `districts` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_staff_id_warrants` FOREIGN KEY (`staff_id`) REFERENCES `staffdata` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_wreport_warrants` FOREIGN KEY (`warrant_report_id`) REFERENCES `warrant_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_wtype_warrants` FOREIGN KEY (`warrant_type_id`) REFERENCES `warrant_type` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `warrant_history`
--
ALTER TABLE `warrant_history`
  ADD CONSTRAINT `fk_warranth_staff_id` FOREIGN KEY (`staff_id`) REFERENCES `staffdata` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_warranth_user_id` FOREIGN KEY (`cr_user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_warranth_warrant_id` FOREIGN KEY (`warrant_id`) REFERENCES `warrants` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_warranth_zone_id` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `warrant_type`
--
ALTER TABLE `warrant_type`
  ADD CONSTRAINT `fk_parent_id_warrant_type` FOREIGN KEY (`parent_id`) REFERENCES `warrant_type` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
